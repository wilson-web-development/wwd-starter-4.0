<?php
  global $post;
  block('header');
  if ( post_password_required(  $post->ID ) ) {
    block('standard-content', ['content' => '<h1>Password Required</h1>' . get_the_password_form(), 'classes' =>'m-t-100 m-b-100']);
  } else {
    echo "<div class='wrapper'>";
      the_content();
    echo "</div>";
  }
  block('footer');
?>