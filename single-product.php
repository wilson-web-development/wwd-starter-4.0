<?php
  //this overrides the woocommerce default template for products
  block('header');
  block('breadcrumbs');
  block('woocommerce-product');
  block('footer');
?>