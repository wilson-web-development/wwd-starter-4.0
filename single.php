<?php
  global $post;
  $category = get_primary_category($post);
  $related_posts = get_posts([ 
    'numberposts' => 3, 
    'post__not_in'  => [ $post->ID ],
    'tax_query' => [[
      'taxonomy' => 'category',
      'terms'    => $category->term_id,
      'field'    => 'term_id'
     ]]
  ]);

  block('header');
  block('standard-content', [ 
    'content' => '<h1>'. get_the_title() . '</h1><p>'.get_the_date('F j, Y', $post).'<p>'  . apply_filters('the_content', $post->post_content) 
  ]);
  block('share');
  block('post-tags');
  block('featured-posts', [
    'content' => '<h2>Related Posts</h2>',
    'posts'   => $related_posts
  ]);
  block('footer');
?>