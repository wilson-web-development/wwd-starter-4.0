const gulp = require('gulp')
const concat = require('gulp-concat')
const errorHandler = require('gulp-error-handle')
const prefix = require('gulp-autoprefixer')
const sass = require('gulp-sass')
const imagemin = require('gulp-imagemin')
const webp = require('gulp-webp')


function frontSass(done){
  gulp.src(['sass/style.sass', '../blocks/**/*.sass'])
    .pipe(errorHandler())
    .pipe(concat('style.sass'))
    .pipe(sass({outputStyle: process.env.NODE_ENV == 'development' ? 'expanded' : 'compressed'})) 
    .pipe(prefix())
    .pipe(gulp.dest('../public/css/'))
  done()
}

function editorSass(done){
  gulp.src('sass/editor-style.sass')
    .pipe(errorHandler())  
    .pipe(sass({outputStyle: process.env.NODE_ENV == 'development' ? 'expanded' : 'compressed'})) 
    .pipe(prefix())
    .pipe(gulp.dest('../public/css/'))
  done()
}

function adminSass(done){
  gulp.src('sass/admin.sass')
    .pipe(errorHandler())  
    .pipe(sass({outputStyle: process.env.NODE_ENV == 'development' ? 'expanded' : 'compressed'})) 
    .pipe(prefix())
    .pipe(gulp.dest('../public/css/'))
  done()
}

function woocommerceSass(done){
  gulp.src('sass/woocommerce.sass')
    .pipe(errorHandler())  
    .pipe(sass({outputStyle: process.env.NODE_ENV == 'development' ? 'expanded' : 'compressed'})) 
    .pipe(prefix())
    .pipe(gulp.dest('../public/css/'))
  done()
}

function minifyImages(done){
  gulp.src('images/**/*')
    .pipe(errorHandler())
    .pipe(imagemin([
      imagemin.gifsicle({ interlaced: true }),
      imagemin.jpegtran({ progressive: true }),
      imagemin.optipng({ optimizationLevel: 5 }),
      imagemin.svgo({ plugins: [{ removeViewBox: false, collapseGroups: true }] })
    ]))
    // .pipe(gulp.dest('../public/images'))
    .pipe(webp())
    .pipe(gulp.dest('../public/images'))
  done()
}

function copyImages(done){
  gulp.src('images/**/*')
    .pipe(errorHandler())
    // .pipe(gulp.dest('../public/images'))
    .pipe(webp())
    .pipe(gulp.dest('../public/images'))
  done()
}

function watch(done){
  gulp.watch(['../blocks/**/*.sass', 'sass/**/*.sass', '!sass/editor-style.sass', '!sass/admin.sass', '!sass/woocommerce.sass'], frontSass)
  gulp.watch(['sass/**/*.sass', 'sass/editor-style.sass', '!sass/admin.sass', '!sass/woocommerce.sass'], editorSass)
  gulp.watch('sass/admin.sass', adminSass)
  gulp.watch('sass/woocommerce.sass', woocommerceSass)
  gulp.watch('images/**/*', copyImages)
  done()
}

exports.prod = gulp.parallel(frontSass, editorSass, adminSass, woocommerceSass, minifyImages)
exports.watch = watch
exports.default = watch