import imageCompare from './imageCompare'
import animateOnScroll from './animateOnScroll'
import adaModal from './adaModal'
import { adaSlider, adaSlide, adaSliderPage, adaSliderPrev, adaSliderNext } from './adaSlider'
import adaToggler from './adaToggler'
import { adaForm, adaInput, adaOption } from './adaForms'
import { countdownTimer, countdownTimerDays, countdownTimerHours, countdownTimerMinutes, countdownTimerSeconds } from './countdownTimer'
import forceAspectRatio from './forceAspectRatio'
import countOnScroll from './countOnScroll'
import adaVideo from './adaVideo'
import parallax from './parallax'
import matchSize from './matchSize'
import scrollProgress from './scrollProgress'
import stickyDiv from './stickyDiv'

function initCustomElements(){
  customElements.define('animate-on-scroll', animateOnScroll)

  customElements.define('ada-modal', adaModal)

  customElements.define('ada-slider', adaSlider)
  customElements.define('ada-slide', adaSlide)
  customElements.define('ada-slider-page', adaSliderPage)
  customElements.define('ada-slider-prev', adaSliderPrev)
  customElements.define('ada-slider-next', adaSliderNext)

  customElements.define('ada-toggler', adaToggler)
 
  // customElements.define('ada-form', adaForm)
  // customElements.define('ada-input', adaInput)
  // customElements.define('ada-option', adaOption)

  customElements.define('countdown-timer', countdownTimer)
  customElements.define('countdown-timer-days', countdownTimerDays)
  customElements.define('countdown-timer-hours', countdownTimerHours)
  customElements.define('countdown-timer-minutes', countdownTimerMinutes)
  customElements.define('countdown-timer-seconds', countdownTimerSeconds)

  customElements.define('force-aspect-ratio', forceAspectRatio)

  customElements.define('count-on-scroll', countOnScroll)
  
  customElements.define('ada-video', adaVideo)

  customElements.define('para-lax', parallax)

  customElements.define('match-size', matchSize)

  customElements.define('scroll-progress', scrollProgress)

  customElements.define('sticky-div', stickyDiv)
  
  customElements.define('image-compare', imageCompare)
}

export default initCustomElements