import { setStyle } from '../plugins/helperFunctions'

class scrollProgress extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
    
    setStyle(this, {
      display: 'block',
      position: getComputedStyle(this).position == 'static' ? 'relative' : '',
      backgroundColor: this.getAttribute('background-color') ? this.getAttribute('background-color') : ''
    })


    const type = this.getAttribute('type')
    const progressbar = document.createElement('div')
    const target = document.querySelector(this.getAttribute('target'))

    setStyle(progressbar, {
      position: 'absolute',
      top: 0,
      left: 0,
      width: type == 'vertical' ? '100%' : 0,
      height: type == 'vertical' ? 0 : '100%',
      backgroundColor: this.getAttribute('foreground-color') ? this.getAttribute('foreground-color') : '',
      transition: '250ms'
    })

    this.append(progressbar)

    window.addEventListener('scroll', e => {
      let parentTop = target.getBoundingClientRect().top + document.documentElement.scrollTop - (window.innerHeight / 2)
      let parentBottom = parentTop + target.offsetHeight
      const scroll = window.scrollY
      const scrolledInEl = scroll - parentTop
      let percentScrolled = (scrolledInEl / target.offsetHeight) * 100

      if(scroll > parentTop && scroll < parentBottom){
        if(type == 'vertical'){
          progressbar.style.height = percentScrolled + '%'
        } else {
          progressbar.style.width = percentScrolled + '%'
        }
      }

      if(scroll < parentTop){
        if(type == 'vertical'){
          progressbar.style.height = '0%'
        } else {
          progressbar.style.width = '0%'
        }
      }

      if(scroll > parentBottom){
        if(type == 'vertical'){
          progressbar.style.height = '100%'
        } else {
          progressbar.style.width = '100%'
        }
      }
    })
  }
}

export default scrollProgress