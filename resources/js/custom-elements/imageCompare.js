import { setStyle, wrap, generateID } from '../plugins/helperFunctions'

class imageCompare extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    //bail if wrong
    let isValid = true  
    //must have 2 children
    isValid = this.children.length == 2

    //each child must be an img or picture
    if(isValid){
      isValid = this.children.length ? [...this.children].filter(child => child.tagName == 'PICTURE' || child.tagName == 'IMG').length == 2 : true
    }

    if(!isValid){
      console.error('image-compare must have 2 images as children');
      this.remove()
      return false
    }

    const firstImage = this.children[0]
    const compareContainer = wrap(firstImage)

    //set styles and create elements
    const pictures = this.querySelectorAll('picture')
    if(pictures){
      pictures.forEach(el => setStyle(el, { display: 'inline-flex', height: '100%' }))
    }

    setStyle(this, {
      display: 'inline-flex',
      position: 'relative',
      'user-select': 'none'
    })

    setStyle(compareContainer, {
      top: 0,
      left: 0,
      width: '50%',
      height: '100%',
      position: 'absolute',
      overflow: 'hidden',
      'will-change': 'width',
    })

    const imageEl = firstImage.tagName == 'PICTURE' ? firstImage.firstElementChild : firstImage
    setStyle(imageEl, {
      height: '100%',
      width: 'auto',
      'max-width': 'none'
    })

    const compareHandle = document.createElement('div')
    compareHandle.classList.add('image-compare--handle')

    const button = document.createElement('button')
    button.addEventListener('keydown', e => {
      if(e.key == 'ArrowDown' || e.key == 'ArrowLeft'){
        hiddenInput.value = parseInt(hiddenInput.value) - 1
        hiddenInput.dispatchEvent(new Event('change'))
      } else if(e.key == 'ArrowRight' || e.key == 'ArrowUp'){
        hiddenInput.value = parseInt(hiddenInput.value) + 1
        hiddenInput.dispatchEvent(new Event('change'))        
      }
    })
    button.setAttribute('aria-label', 'Compare images by moving this button left or right')
    compareHandle.append(button)

    this.append(compareHandle)
    setStyle(compareHandle, {
      top: '50%',
      left: '50%',
      transform: 'translate(-50%, -50%)',
      position: 'absolute',
      cursor: 'grab',
    })

    //create hidden input for ADA requirements
    const hiddenInputContainer = document.createElement('div')
    const hiddenInputLabel = document.createElement('label')
    const hiddenInput = document.createElement('input')
    hiddenInput.setAttribute('type', 'range')
    hiddenInput.setAttribute('min', 0)
    hiddenInput.setAttribute('max', 100)
    hiddenInput.value = 50
    hiddenInput.id = 'image-compare-' + generateID()
    hiddenInputLabel.innerText = 'Compare Images'
    hiddenInputLabel.setAttribute('for', hiddenInput.id)
    hiddenInputContainer.append(hiddenInputLabel)
    hiddenInputContainer.append(hiddenInput)
    this.append(hiddenInputContainer)
    hiddenInput.addEventListener('change', e => {
      setStyle(compareHandle, { left: hiddenInput.value + '%' })
      setStyle(compareContainer, { width: hiddenInput.value + '%' })
    })
    setStyle(hiddenInputContainer, {
      position: 'absolute',
      left: '-9999px',
      width: '1px',
      height: '1px',
      overflow: 'hidden'
    })

    //create mouse/touch events
    let shouldMove = false
    const touchdowns = [ 'mousedown', 'touchstart' ]
    const touchups = [ 'mouseup', 'touchend' ]
    const touchmoves = [ 'mousemove', 'touchmove' ]

    touchdowns.forEach(type => {
      compareHandle.addEventListener(type, e => {
        setStyle(compareHandle, { 'pointer-events': 'none' })
        shouldMove = true
      })
    })

    touchups.forEach(type => {
      window.addEventListener(type, e => {
        shouldMove = false
        setStyle(compareHandle, { 'pointer-events': '' })
      })
    })

    touchmoves.forEach(type => {
      this.addEventListener(type, e => {
        e.preventDefault()
        if(shouldMove){
          const left = this._getRelativeCursorPosition(e)
          setStyle(compareHandle, { left: left })
          setStyle(compareContainer, { width: left })
          hiddenInput.value = left.replace('%', '')
        }
      })
    })



    //create labels
    const labels = [ this.getAttribute('label-1'), this.getAttribute('label-2') ]
    labels.forEach((text, index) => {
      if(text){
        const label = document.createElement('div')
        label.innerText = text
        label.classList.add('image-compare--label')
        this.append(label)
        setStyle(label, {
          position: 'absolute',
          left: index == 0 ? 0 : '',
          right: index == 1 ? 0 : '',
        })
      }
    })    
  }

  _getRelativeCursorPosition(e){
    //touches calculate a bit differently
    let percent = 0
    let x = 0
    if(e.touches && e.touches.length){
      x = e.touches[0].clientX
      percent = ((x / e.target.parentNode.offsetWidth) * 100)
    } else {
      const rect = e.target.getBoundingClientRect()      
      x = e.clientX - rect.left
      percent = ((x / e.target.offsetWidth) * 100)      
    }

    if(percent < 0){
      percent = 0
    } else if(percent > 100){
      percent = 100
    }

    return percent + '%'    
  }
}

export default imageCompare