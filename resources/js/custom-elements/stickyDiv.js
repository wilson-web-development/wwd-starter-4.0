import { setStyle } from '../plugins/helperFunctions'

class stickyDiv extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    setStyle(this, {
      display: 'block',
      position: 'absolute',
      top: 0
    })

    window.addEventListener('scroll', e => {
      const parentTop = this.parentNode.getBoundingClientRect().top + document.documentElement.scrollTop
      const parentBottom = parentTop + this.parentNode.offsetHeight
      const scroll = window.scrollY

      // we are below scroll params
      if(this.offsetHeight + scroll >= parentBottom){
        this.style.position = 'absolute'
        this.style.top = ''
        this.style.bottom = 0
        return false
      }

      //we are within scroll params
      if(scroll > parentTop && scroll < parentBottom){
        this.style.position = 'fixed'
        this.style.top = 0
        this.style.bottom = ''
        return false
      }

      //we are above scroll params
      if(scroll < parentTop){
        this.style.position = 'absolute'
        this.style.top = 0  
        this.style.bottom = ''
        return false          
      }
    })
  }
}

export default stickyDiv