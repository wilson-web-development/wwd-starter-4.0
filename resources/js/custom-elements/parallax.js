import { isVisible } from '../plugins/helperFunctions'

function throttle(fn, wait) {
  let time = Date.now()
  return function() {
    if ((time + wait - Date.now()) < 0) {
      fn()
      time = Date.now()
    }
  }
}

class parallax extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    this.style.display = 'block'
    this._maxDistance = parseFloat(this.getAttribute('max-distance')) || 99999
    this._speed =  parseFloat(this.getAttribute("speed")) || 0.25

    setTimeout(() => {
      this._initialTopDistance = this.getBoundingClientRect().top + document.documentElement.scrollTop
      window.addEventListener('scroll', throttle(() => this._move_and_slide(false), 14))
      this._move_and_slide(true)
    }, 50)
  }

  _move_and_slide(firstTime){
    // if(!firstTime && !isVisible(this)){      
    //   return
    // }
    const scrolled = window.pageYOffset
    const distance = this._initialTopDistance - (window.innerHeight / 2) + (this.offsetHeight / 2)
    const coords = ((distance - scrolled) * this._speed)

    if(Math.abs(coords) < Math.abs(this._maxDistance)){
      this.style.transform = 'translateY(' + coords + 'px)'
    } else {
      const max = coords < 0 ? -this._maxDistance : this._maxDistance
      this.style.transform = 'translateY(' + max + 'px)'
    }
  }
}

export default parallax