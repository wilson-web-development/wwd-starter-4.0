class countdownTimer extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    let timestamp = this.getAttribute('target')
    let now = this.getAttribute('now')

    if(!timestamp){
      console.warn(this, ' has no target timestamp.')
      return
    }

    if(parseInt(timestamp) != timestamp){
      console.warn(this, ' timestamp should be a unix timestamp')
    }

    if(!now){
      console.warn(this, ' has no now timestamp.')
      return
    }

    if(parseInt(now) != now){
      console.warn(this, ' now should be a unix timestamp')
    }


    timestamp = parseInt(timestamp) * 1000
    now = parseInt(now) * 1000

    setInterval(() => {
        let date_future = new Date(timestamp) //new Date(new Date().getFullYear() +1, 0, 1)
        let date_now = new Date(now)
        let seconds = Math.floor((date_future - (date_now))/1000)
        let minutes = Math.floor(seconds/60)
        let hours = Math.floor(minutes/60)
        let days = Math.floor(hours/24)
        hours = hours-(days*24)
        minutes = minutes-(days*24*60)-(hours*60)
        seconds = seconds-(days*24*60*60)-(hours*60*60)-(minutes*60)
        now += 1000

        if(date_future < date_now){
          days = 0
          hours = 0
          minutes = 0
          seconds = 0
        }

        let daysEls = this.querySelectorAll('countdown-timer-days')
        let hoursEls = this.querySelectorAll('countdown-timer-hours')
        let minutesEls = this.querySelectorAll('countdown-timer-minutes')
        let secondsEls = this.querySelectorAll('countdown-timer-seconds')

        if(daysEls.length){
          daysEls.forEach(el => el.innerHTML = days)
        }
        if(hoursEls.length){
          hoursEls.forEach(el => el.innerHTML = hours)
        }
        if(minutesEls.length){
          minutesEls.forEach(el => el.innerHTML = minutes)
        }
        if(secondsEls.length){
          secondsEls.forEach(el => el.innerHTML = seconds)
        }
    }, 1000);
  }
}

/** 
 * These down have any functionality, they just make it easy to find children of the parent
 */
class countdownTimerDays extends HTMLElement {
  constructor(){
    super()
  }
}
class countdownTimerHours extends HTMLElement {
  constructor(){
    super()
  }
}
class countdownTimerMinutes extends HTMLElement {
  constructor(){
    super()
  }
}
class countdownTimerSeconds extends HTMLElement {
  constructor(){
    super()
  }
}

export {
  countdownTimer,
  countdownTimerDays,
  countdownTimerHours,
  countdownTimerMinutes,
  countdownTimerSeconds,
}