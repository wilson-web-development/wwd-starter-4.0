import { isVisible, setStyle, wrap } from '../plugins/helperFunctions'

const debounce = (func, delay) => {
  let inDebounce
  return function() {
    const context = this
    const args = arguments
    clearTimeout(inDebounce)
    inDebounce = setTimeout(() => func.apply(context, args), delay)
  }
}

class adaSlider extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
        
    if(!this.id){
      console.warn('ADA slider must have an id')
      this.remove()
      return
    }
    
    if([...this.children].filter(child => child.tagName !== 'ADA-SLIDE').length){
      console.warn('ADA slider can only have ada-slide as children')
    }
   
    if(document.querySelectorAll(`ada-slider-page[for="${this.id}"]`).length != this.children.length){
      console.warn(this.id, 'ADA slider slides/pagination mismatch')
    }
    if(!document.querySelector(`ada-slider-prev[for="${this.id}"]`)){
      console.warn(this.id, 'ADA slider must have prev button')
    }
    if(!document.querySelector(`ada-slider-next[for="${this.id}"]`)){
      console.warn(this.id, 'ADA slider must have next button')
    }

    if(!this.getAttribute('slides-per-page')){
      this.setAttribute('slides-per-page', 1)
    }

    if(!this.getAttribute('slides-per-page-large')){
      this.setAttribute('slides-per-page-large', this.getAttribute('slides-per-page'))
    }

    if(!this.getAttribute('slides-per-page-medium')){
      this.setAttribute('slides-per-page-medium', this.getAttribute('slides-per-page-large'))
    }

    if(!this.getAttribute('slides-per-page-small')){
      this.setAttribute('slides-per-page-small', this.getAttribute('slides-per-page-medium'))
    }
    
    if(!this.getAttribute('active-page')){
      this.setAttribute('active-page', 0)
    }
    
    if(!this.getAttribute('speed')){
      this.setAttribute('speed', 350)
    }

    if(!this.getAttribute('pagination-style')){
      this.setAttribute('pagination-style', 'single') //or multi
    }

    if(!this.getAttribute('transition-type')){
      this.setAttribute('transition-type', 'all') // or fade
    }

    if(this.getAttribute('autoplay')){
      this._startAutoplay()
    }

    //create live region for aria announcements
    this._createLiveRegion()

    //set styles
    this._createInitialStyles()
    
    //add touch/motion events
    this._createTouchEvents()

    //
    this._handleResponsivePerPage()
    window.addEventListener('resize', debounce(() => this._handleResponsivePerPage(), 500))
  }

  _handleResponsivePerPage(){
    const large = this.getAttribute('slides-per-page-large')
    const medium = this.getAttribute('slides-per-page-medium')
    const small = this.getAttribute('slides-per-page-small')

    if(window.innerWidth <= 768){
      this.setAttribute('slides-per-page', small)
    } else if(window.innerWidth <= 1024) {
      this.setAttribute('slides-per-page', medium)
    } else {
      this.setAttribute('slides-per-page', large)
    }
    
    setTimeout(() => {
      //check if the currently active page is a valid target, if not find an active target for it
      const current = parseInt(this.getAttribute('active-page'))
      const pages = document.querySelectorAll(`ada-slider-page[for="${this.id}"]`)

      if(pages && pages[current].style.display == 'none'){
        for (let i = 0; i < pages.length; i++) {
          if(pages[i].style.display != 'none'){
            this.setAttribute('active-page', i)
            break
          }
        }
      }

      //if there are less or an equal amount of slides as "slides-per-page" hide the navigation
      const relatedNavigation = document.querySelectorAll(`[for="${this.id}"]`)
      if(relatedNavigation){
        relatedNavigation.forEach(el => {
          el.style.display =  parseInt(this.getAttribute('slides-per-page')) >= this.children.length ? 'none' : ''
        })
      }
    }, parseInt(this.getAttribute('speed')) + 100)
  }

  _startAutoplay(){
    const speed = this.getAttribute('autoplay-speed') || 6000
    const interval = setInterval(() => {
      const current = parseInt(this.getAttribute('active-page'))
      let target = current + 1
      
      if(this.getAttribute('pagination-style') == 'multi'){
        //then we show/hide certain pages only....
        const slidesPerPage = this.getAttribute('slides-per-page') 
        target += parseInt(slidesPerPage) - 1
      }

      if(target > this.children.length - 1){
        target = 0
      }
      
      this.setAttribute('active-page', target)
    }, speed)

    const touchdowns = [ 'mousedown', 'touchstart' ]
    touchdowns.forEach(event => this.addEventListener(event, e => clearInterval(interval)))
  }

  _createTouchEvents(){
    const touchdowns = [ 'mousedown', 'touchstart' ]
    const touchups = [ 'mouseup', 'touchend' ]
    const touchmoves = [ 'mousemove', 'touchmove' ]
    let isTouching = false
    let firstX = 0
    let lastX = 0    
    let distance = 0
    
    touchdowns.forEach(event => this.addEventListener(event, e => {
      if(e.touches){
        firstX = e.touches[0].clientX
        lastX = firstX
        isTouching = true
      } else {
        firstX = e.clientX
        lastX = firstX
        isTouching = true
      }
    }))
    touchmoves.forEach(event => this.addEventListener(event, e => {
      if(!isTouching){
        return false
      }

      if(e.touches){
        lastX = e.touches[0].clientX
      } else {
        lastX = e.clientX
      }
    }))

    touchups.forEach(event => {
      this.addEventListener(event, e => {
        if(Math.abs(firstX - lastX) > 40){
          if(lastX > firstX){
            const prev = document.querySelector(`ada-slider-prev[for="${this.id}"]`)
            if(prev){
              e.preventDefault()
              e.stopPropagation()
              prev.querySelector('button').click()
            }
          }

          if(lastX < firstX){
            const next = document.querySelector(`ada-slider-next[for="${this.id}"]`)
            if(next){
              e.preventDefault()
              e.stopPropagation()
              next.querySelector('button').click()
            }
          }
        }

        isTouching = false
        firstX = 0
        lastX = 0
      })
    })
  }

  _createInitialStyles(){
    const speed = this.getAttribute('speed')
    setStyle(this, {
      display: 'flex',
      overflow: 'hidden',
      position: 'relative',
      transition: `height ${speed}ms linear, opacity ${speed}ms linear`
    })
  }

  _createLiveRegion(){
    const liveregion = document.createElement('div')
    liveregion.setAttribute('aria-live', 'polite')
    liveregion.setAttribute('aria-atomic', 'true')
    liveregion.setAttribute('for', this.id)
    liveregion.setAttribute('class', `ada-slider--liveregion`)
    setStyle(liveregion, {
      'clip': 'rect(0 0 0 0)',
      'clip-path': 'inset(50%)',
      'height': '1px',
      'overflow': 'hidden',
      'position': 'absolute',
      'white-space': 'nowrap',
      'width': '1px'
    })
    this.after(liveregion)
  }
}

class adaSlide extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
      
    //listen for changes on the parent and reinit
    const parentObserver = new MutationObserver((mutation, observer) => this._init())
    parentObserver.observe(this.parentNode, { attributes: true }) 

    this.lastActivePage = -1
    this.lastPerPage = 1    
    this._init(true)
  }
  

  _animateInLeft(speed){
    //set block style (cant animate)
    setStyle(this, { transition: 'none', display: 'block', position: 'static' })
    //transition in
    setTimeout(() => setStyle(this, { opacity: 1, transform: 'translate3d(-100%,0,0)', transition: `opacity ${speed}ms linear, transform ${speed}ms linear` }), 1) }

  //start everything... do this any time this needs an update
  _init(firstTime){
    const activePage = parseInt(this.parentNode.getAttribute('active-page'))   
    const perPage = parseInt(this.parentNode.getAttribute('slides-per-page'))
    const transitionType = this.parentNode.getAttribute('transition-type')
    const minActiveIndex = activePage
    const maxActiveIndex = activePage + perPage - 1
    const index = this._getIndexInParent()
    const perpage = this.parentNode.getAttribute('slides-per-page')
    const percentage = 100/(perpage ? perpage : 1)
    const speed = parseInt(this.parentNode.getAttribute('speed'))
    const wasActive = this.classList.contains('active')

    if(this.lastPerPage != perpage){
      this.lastPerPage = perpage
      firstTime = true
    }

    //if this is the first time animating, add some basic styles and turn off the transition temporarily
    if(firstTime){
      setStyle(this, {
        display: 'none',
        flex: `1 0 ${percentage}%`,
        'max-width': `${percentage}%`,
        transition: 'none'
      })
      setTimeout(() => this.style.transition = `opacity ${speed}ms linear, transform ${speed}ms linear`, speed + 1)
    }

    //find out if this slide is prev, active, or next
    let slideType = ''
    if(index >= minActiveIndex && index <= maxActiveIndex){
      this.classList.add('active')
      slideType = 'active'
    } else if(index < minActiveIndex){
      this.classList.remove('active')
      slideType = 'previous'
    } else if(index > maxActiveIndex){
      this.classList.remove('active')
      slideType = 'next'
    }

    if(slideType == 'active'){
      this.style.display = 'block'
      requestAnimationFrame(() => {
        setStyle(this, {
          opacity: 1,
          transform: 'none',
          position: 'static',
          width: '',
        })
      })
    }

    if(slideType == 'previous'){
      setStyle(this, {
        opacity: 0,
        transform: transitionType == 'fade' ? '' : 'translate3d(-100%,0,0)',
        position: 'absolute',
        left: 0,
        width: this.offsetWidth + 'px'
      })
      setTimeout(() => this.style.display = 'none', speed + 1)
    }

    if(slideType == 'next'){
      setStyle(this, {
        opacity: 0,
        transform: transitionType == 'fade' ? '' : 'translate3d(100%,0,0)',
        position: 'absolute',
        left: (this.offsetWidth * (perPage - 1)) + 'px',
        width: this.offsetWidth + 'px'
      })
      setTimeout(() => this.style.display = 'none', speed + 1)
    }


    // This stuff handles animations if there is more than 1 slide per page
    if(slideType == 'active' && wasActive && activePage > this.lastActivePage){
      //if this slide was active and we are going to the NEXT page
      //quick move this slide right then transition left
      this.style.transition = 'none'
      this.style.transform = transitionType == 'fade' ? '' : 'translate3d(100%, 0, 0)'
      requestAnimationFrame(() => {
        this.style.transition = `opacity ${speed}ms linear, transform ${speed}ms linear`
        this.style.transform = 'none'
      })
    }

    if(slideType == 'active' && wasActive && activePage < this.lastActivePage){
      //if this slide was active and we are going to the PREV page
      //quick move this slide left then transition right
      this.style.transition = 'none'
      this.style.transform = transitionType == 'fade' ? '' : 'translate3d(-100%,0,0)'
      requestAnimationFrame(() => {
        this.style.transition = `opacity ${speed}ms linear, transform ${speed}ms linear`
        this.style.transform = 'none'
      })
    }

    this.lastActivePage = activePage

    this._setAria(speed)
  }
  
  //set aria tags
  _setAria(speed){
    const isActive = this._shouldBeActive()
    this.setAttribute('tabindex', -1)
    this.setAttribute('aria-hidden', !isActive)
    if(!isActive){
      setTimeout(() => this.style.display = 'none', speed + 1)
    } else {
      document.querySelector(`.ada-slider--liveregion[for="${this.parentNode.id}"]`).textContent = 'Item ' + (parseInt(this.parentNode.getAttribute('active-page')) + 1) + ' of ' + this.parentNode.children.length
    }
  }
  
  //checks if the slide should be active by seeing if the paren't active-slide is set to this index 
  _shouldBeActive(){
    const index = this._getIndexInParent()
    const activePage = parseInt(this.parentNode.getAttribute('active-page'))
    const perPage = parseInt(this.parentNode.getAttribute('slides-per-page'))
    return index >= activePage && index <= activePage + (perPage - 1)
  }
  
  _getIndexInParent(){
    return Array.prototype.indexOf.call(this.parentNode.children, this)
  }
}

class adaSliderPage extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
    
    if(this.parentNode.tagName != 'UL'){
      console.warn('ADA slider page should have a <ul> for a parent')
    }

    const button = document.createElement('button')
    if(this.children.length){
      Array.from(this.children).forEach(child => button.append(child))
    } else {
      button.innerHTML = this.innerHTML
      this.innerHTML = ''
    }
    this.append(button)
    
    // const button = wrap(this, 'button')
    // button.classList.add('ada-slider-page')
    const li = wrap(this, 'li')
    
    button.addEventListener('click', e => {
      const slider = document.querySelector('#' + this.getAttribute('for'))
      slider.setAttribute('active-page', this.getAttribute('index'))
      setTimeout(() => {
        slider.querySelector('ada-slide[aria-hidden="false"]').focus()
      }, parseInt(slider.getAttribute('speed')) + 1)      
    })

    if(!isVisible(button)){
      button.setAttribute('focusable', 'false')
    }

    window.addEventListener('resize', e => {
      if(!isVisible(button)){
        button.setAttribute('focusable', 'false')
      } else {
        button.removeAttribute('focusable')
      }
    })
    
    const observer = new MutationObserver((mutation, observer) => this._init())
    observer.observe(document.querySelector('#' + this.getAttribute('for')), { attributes: true })
    
    this._init()
  }
  
  _init(){
    const slider = document.querySelector('#' + this.getAttribute('for'))
    const button = this.querySelector('button') //aria stuff should be set on buttons

    if(slider.getAttribute('pagination-style') == 'multi'){
      //then we show/hide certain pages only....
      const slidesPerPage = slider.getAttribute('slides-per-page') 

      if(parseInt(this.getAttribute('index')) % slidesPerPage !== 0){
        this.style.display = 'none'
      } else {
        this.style.display = ''
        let realIndex = parseInt(this.getAttribute('index'))
        let groupIndex = realIndex > 0 ? Math.ceil(realIndex / slidesPerPage+1) : 1
        button.setAttribute('aria-label', 'Go to Slide Group ' + groupIndex)
      }
    } else {
      let pageIndex = parseInt(this.getAttribute('index'))+1
      button.setAttribute('aria-label', 'Go to Slide ' + pageIndex)
    }

    if(slider.getAttribute('active-page') == this.getAttribute('index')){
      button.setAttribute('aria-current', true)
      this.classList.add('active')
    } else {
      button.setAttribute('aria-current', false)
      this.classList.remove('active')
    }
  }
}

class adaSliderPrev extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    const button = document.createElement('button')
    if(this.children.length){
      Array.from(this.children).forEach(child => button.append(child))
    } else {
      button.innerHTML = this.innerHTML
      this.innerHTML = ''
    }
    this.append(button)

    const ariaLabel = this.getAttribute('aria-label')
    if(ariaLabel){
      button.setAttribute('aria-label', ariaLabel)
      this.removeAttribute('aria-label')
    }

    // wrap(this, 'button')
    button.addEventListener('click', e => {
      const slider = document.querySelector('#' + this.getAttribute('for'))
      const current = parseInt(slider.getAttribute('active-page'))
      const paginationStyle = slider.getAttribute('pagination-style')
      const slidesPerPage = parseInt(slider.getAttribute('slides-per-page'))
      let target = current - 1

      if(paginationStyle == 'multi'){
        //then we show/hide certain pages only....
        target -= slidesPerPage - 1
      }

      if(target < 0){
        if(paginationStyle == 'multi'){
          var allValidIndexs = []
          for(let i = 0; i < slider.children.length - 1; i++){            
            if(i % slidesPerPage == 0){
              allValidIndexs.push(i)
            }
          }
          target = allValidIndexs[allValidIndexs.length - 1]
        } else {
          target = slider.children.length - 1
        }
      }
      
      if(!isVisible(button)){
        button.setAttribute('focusable', 'false')
      }
  
      window.addEventListener('resize', e => {
        if(!isVisible(button)){
          button.setAttribute('focusable', 'false')
        } else {
          button.removeAttribute('focusable')
        }
      })

      slider.setAttribute('active-page', target)
      setTimeout(() => {
        slider.querySelector('ada-slide[aria-hidden="false"]').focus()
      }, parseInt(slider.getAttribute('speed')) + 1)       
    })
  }
}

class adaSliderNext extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    const button = document.createElement('button')
    if(this.children.length){
      Array.from(this.children).forEach(child => button.append(child))
    } else {
      button.innerHTML = this.innerHTML
      this.innerHTML = ''
    }
    this.append(button)

    const ariaLabel = this.getAttribute('aria-label')
    if(ariaLabel){
      button.setAttribute('aria-label', ariaLabel)
      this.removeAttribute('aria-label')
    }

    button.addEventListener('click', e => {
      const slider = document.querySelector('#' + this.getAttribute('for'))
      const current = parseInt(slider.getAttribute('active-page'))
      let target = current + 1
      
      if(slider.getAttribute('pagination-style') == 'multi'){
        //then we show/hide certain pages only....
        const slidesPerPage = slider.getAttribute('slides-per-page') 
        target += parseInt(slidesPerPage) - 1
      }

      if(target > slider.children.length - 1){
        target = 0
      }
      
      slider.setAttribute('active-page', target)
      setTimeout(() => {
        slider.querySelector('ada-slide[aria-hidden="false"]').focus()
      }, parseInt(slider.getAttribute('speed')) + 1)      
    })


    if(!isVisible(button)){
      button.setAttribute('focusable', 'false')
    }

    window.addEventListener('resize', e => {
      if(!isVisible(button)){
        button.setAttribute('focusable', 'false')
      } else {
        button.removeAttribute('focusable')
      }
    })
  }
}

export {
  adaSlider,
  adaSlide,
  adaSliderPage,
  adaSliderPrev,
  adaSliderNext
}