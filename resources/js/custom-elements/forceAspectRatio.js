import { wrap, setStyle } from '../plugins/helperFunctions'

class forceAspectRatio extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    setTimeout(() => {
      const ratio = this.getAttribute('aspect-ratio')

      if(this.children.length != 1){
        console.warn(this, 'Force aspect ratio should only have 1 child as an element')
      }
            
      if(!ratio || !ratio.match(/^[1-9]+:[1-9]+$/)){
        console.warn(this, 'Missing or incorrect aspect-ratio attribute')
        return
      }
    }, 1000)
    
    const ratio = this.getAttribute('aspect-ratio')
    const top = ratio.split(':')[1]
    const bottom = ratio.split(':')[0]
    
    setStyle(this, {
      position: 'relative',
      width: '100%',
      'padding-top': (top/bottom) * 100 + '%',
      display: 'block'
    })
    
    setStyle(this.firstElementChild, {
      position: 'absolute',
      top: 0,
      left: 0,
      width: '100%',
      height: '100%'
    })
  }
}

export default forceAspectRatio