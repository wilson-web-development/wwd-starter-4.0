import { generateID } from '../plugins/helperFunctions'

class adaForm extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    //wrap all children in a form element
    const form = document.createElement('form')
    Array.from(this.children).forEach(child => form.append(child))
    this.append(form)
    const action = this.getAttribute('action') || ''
    const method = this.getAttribute('method') || ''
    if(action){
      form.setAttribute('action', action)
    }
    if(method){
      form.setAttribute('method', method)
    }

    //listen for a submit
    form.addEventListener('submit', e => {
      e.preventDefault()
      e.stopPropagation()
      
      let formIsValid = true //prove me wrong at least once      
      
      form.querySelectorAll('ada-input').forEach(input => {
        if(input.getAttribute('type') != 'submit' && !input._isValid()){
          formIsValid = false
        }
      })
      
      if(!formIsValid){
        let inputTarget = this.querySelector('ada-input.error input') || this.querySelector('ada-input.error textarea') || this.querySelector('ada-input.error select')
        inputTarget.focus()
      } else {
        const formData = new FormData(this.querySelector('form'))
        let obj = {}
        for (let [key, value] of formData) {
          if(key == null || value == null){
            continue
          }
          
          if (obj[key] !== undefined) {
            if (!Array.isArray(obj[key])) {
              obj[key] = [obj[key]]
            }
            obj[key].push(value)
          } else {
            obj[key] = value
          }
        }
        this.dispatchEvent(new CustomEvent('submit', { detail: obj }))
      }
    })
  }
}

class adaInput extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  static get observedAttributes() { 
    return [ 'value' ]
  }

  attributeChangedCallback (name, oldValue, newValue) {
    if(name == 'value'){
      let target = this.querySelector('textarea') || this.querySelector('select') || this.querySelector('input')
      if(target){
        target.value = newValue
      }
    }
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
    
    if(!this.id){
      this.id = generateID()
    }
    
    switch(this.getAttribute('type')){
      case 'checkbox':
      case 'radio':
        this._createCheckboxInputs()
        break
      case 'textarea':
        this._createTextarea()
        break
      case 'select':
        this._createSelect()
        break
      default:
        this._createDefaultInput()
    }
    
    requestAnimationFrame(() => {
      this.querySelectorAll('input, textarea, select').forEach(input => {
        input.addEventListener('input', e => this._isValid())
        input.addEventListener('input', e => this.dispatchEvent(new CustomEvent('interact', { detail: e.target.value })))
      })
    })
  }
  
  _createTextarea(){
    const label = document.createElement('label')
    label.innerText = this.getAttribute('label')
    label.setAttribute('for', `${this.id}-input`)
    this.append(label)
    
    const input = document.createElement('textarea')
    input.placeholder = this.getAttribute('placeholder') || ''
    input.value = this.getAttribute('value')    
    input.name = this.getAttribute('name')    
    input.id = `${this.id}-input`
    this.append(input)

    const rules = this.getAttribute('rules')
    if(rules && rules.indexOf('required') != -1){
      input.setAttribute('aria-required', true)
      label.innerHTML = label.innerText + '<span aria-hidden="true">*</span>'
    }
  }
  
  _createCheckboxInputs(){
    //wrap all contents in a fieldset
    const fieldset = document.createElement('fieldset')
    Array.from(this.children).forEach(child => fieldset.append(child))
    this.append(fieldset)
    
    const legend = document.createElement('legend')
    legend.innerHTML = this.getAttribute('label')
    fieldset.prepend(legend)

    const rules = this.getAttribute('rules')
    if(rules && rules.indexOf('required') != -1){
      fieldset.setAttribute('aria-required', true)
      legend.innerHTML = legend.innerText + '<span aria-hidden="true">*</span>'
    }
  }
  
  _createDefaultInput(){
    const label = document.createElement('label')
    label.innerText = this.getAttribute('label')
    label.setAttribute('for', `${this.id}-input`)
    this.append(label)
    
    const input = document.createElement('input')
    input.type = this.getAttribute('type')
    input.name = this.getAttribute('name')
    input.placeholder = this.getAttribute('placeholder') || ''
    input.value = this.getAttribute('value')    
    input.id = `${this.id}-input`
    this.append(input)

    const rules = this.getAttribute('rules')
    if(rules && rules.indexOf('required') != -1){
      input.setAttribute('aria-required', true)
      label.innerHTML = label.innerText + '<span aria-hidden="true">*</span>'
    }    
  }
  
  _createSelect(){
    const label = document.createElement('label')
    label.innerText = this.getAttribute('label')
    label.setAttribute('for', `${this.id}-input`)
    this.append(label)
    
    const input = document.createElement('select')
    input.name = this.getAttribute('name')
    input.id = `${this.id}-input`    
    this.append(input)    
    setTimeout(() => input.value = this.getAttribute('value'))

    const rules = this.getAttribute('rules')
    if(rules && rules.indexOf('required') != -1){
      input.setAttribute('aria-required', true)
      label.innerHTML = label.innerText + '<span aria-hidden="true">*</span>'
    }    
  }
  
  _isValid(){    
    const rules = this.getAttribute('rules')
    let isValid = true //prove me wrong once
    let errors = []

    if(this.getAttribute('type') == 'submit'){
      return false
    }

    if(rules){
      rules.split('|').forEach(rule => {
        let target = this.querySelector('textarea') || this.querySelector('select') || this.querySelector('input')
        const value = target.value
        const split = rule.split(':')
        const name = split[0]
        const modifier = split[1]
        switch(name){
          case 'required':
            if(!this._validateRequired(value)){
              errors.push('This field is required')
              isValid = false
            }
            break
          case 'email':
            if(!this._validateEmail(value)){
              errors.push('This field must be a valid email')
              isValid = false
            }
            break
          case 'min':
            if(!this._validateMin(value, modifier)){
              errors.push('This field must be greater or equal to ' + modifier)
              isValid = false
            }
            break
          case 'max':
            if(!this._validateMax(value, modifier)){
              errors.push('This field must be less than or equal to ' + modifier)
              isValid = false
            }
            break
          case 'maxLength':
            if(!this._validateMaxLength(value, modifier)){
              errors.push('This field must be less than or equal to ' + modifier + ' characters')
              isValid = false
            }
            break
          case 'minLength':
            if(!this._validateMinLength(value, modifier)){
              errors.push('This field must be greater than or equal to ' + modifier + ' characters')
              isValid = false
            }
            break
          case 'sameAs':
            if(!this._validateSameAs(value, modifier)){
              errors.push('This field must match')
              isValid = false
            }
            break
          case 'mime':
            if(!this._validateMime(value, modifier)){
              errors.push('This field must match the file type: ' + modifier)
              isValid = false
            }
            break
          case 'dateBefore':
            if(!this._validateDateBefore(value, modifier)){
              errors.push('This field must be before ' + modifier)
              isValid = false
            }
            break
          case 'dateAfter':
            if(!this._validateDateAfter(value, modifier)){
              errors.push('This field must be after ' + modifier)
              isValid = false
            }
            break
        }
      })
    }

    this._setErrors(errors)
    return isValid
  }
  
  _validateRequired(value){
    const inputType = this.getAttribute('type')
    if(inputType == 'checkbox' || inputType == 'radio'){
      let isValid = false
      this.querySelectorAll(`[name="${this.getAttribute('name')}"]`).forEach(input => {
        if(input.checked || this.selected){
          isValid = true
        }
      })
      return isValid
    } else {
      return value != undefined && value.length
    }
  }
  
  _validateEmail(value){
    return !value ? true : /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(value)
  }
  
  _validateDateBefore(value, checkAgainst){
    return !value ? true : parseInt(value.replaceAll('-', '')) < parseInt(checkAgainst.replaceAll('-', '')) 
  }
  
  _validateDateAfter(value, checkAgainst){
    return !value ? true : parseInt(value.replaceAll('-', '')) > parseInt(checkAgainst.replaceAll('-', '')) 
  }
  
  _validateMime(value, checkAgainst){
    const file = this.querySelector('input').files[0]
    let isValid = true //prove me true
    if(file){
      if(checkAgainst.indexOf('*') != -1){
        isValid = file.type.indexOf(checkAgainst.replaceAll('*', '')) !== -1
      } else {
        isValid = file.type != checkAgainst
      }     
    }
    return isValid
  }
  
  _validateSameAs(value, checkAgainst){
    const targetValue = this.closest('ada-form').querySelector(`input[name="${checkAgainst}"]`).value
    return !value ? true : value == targetValue
  }
  
  _validateMinLength(value, checkAgainst){
    return !value ? true : value.length >= checkAgainst
  }
  
  _validateMaxLength(value, checkAgainst){
    return !value ? true : value.length <= checkAgainst
  }
  
  _validateMin(value, checkAgainst){
    const inputType = this.getAttribute('type')

    if(inputType == 'checkbox'){
      const checkedChildren = this.querySelectorAll('input:checked')
      return !checkedChildren.length ? true : parseFloat(checkAgainst) <= checkedChildren.length
    } else {
      return !value ? true : parseFloat(checkAgainst) <= parseFloat(value)
    }
  }
  
  _validateMax(value, checkAgainst){
    const inputType = this.getAttribute('type')

    if(inputType == 'checkbox'){
      const checkedChildren = this.querySelectorAll('input:checked')
      return !checkedChildren.length ? true : parseFloat(checkAgainst) >= checkedChildren.length
    } else {
      return !value ? true : parseFloat(checkAgainst) >= parseFloat(value)
    }
 }
  
  _setErrors(errors){
    const prevErrors = this.querySelectorAll('.ada-input-error') || []
    prevErrors.forEach(err => err.remove())
    
    if(errors.length){
      this.classList.add('error')
      this.classList.remove('valid')
    } else {
      this.classList.remove('error')
      this.classList.add('valid')
    }
    
    if(errors.length){
      const errorContainer = document.createElement('ul')
      errorContainer.id = `${this.id}-errors`
      errorContainer.classList.add('ada-input-error')
      errors.forEach(error => {
        const el = document.createElement('li')
        el.innerText = error
        errorContainer.append(el)
      })
      //maybe this should be `fieldset` for checkboxes and radios?
      const targetInput = this.querySelector('textarea') || this.querySelector('input') || this.querySelector('select')
      targetInput.setAttribute('aria-describedby', `${this.id}-errors`)
      this.append(errorContainer)
    } else {
      const targetInput = this.querySelector('textarea') || this.querySelector('input') || this.querySelector('select')
      targetInput.removeAttribute('aria-describedby')
      const errorContainer = this.querySelector(`#${this.id}-errors`)
      if(errorContainer){
        errorContainer.remove()
      }
    }
  }
}

class adaOption extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  static get observedAttributes() { 
    return [ 'checked' ]
  }

  attributeChangedCallback (name, oldValue, newValue) {
    if(name == 'checked'){
      let target = this.querySelector('input')
      if(target){
        if(newValue == 'true'){
          target.setAttribute('checked', true)
        } else {
          target.removeAttribute('checked')
        }
      }
    }
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
    
    if(this.parentNode.getAttribute('type') == 'select'){
      const id = generateID()      
      const select = this.parentNode.querySelector('select')
      const option = document.createElement('option')
      option.value = this.getAttribute('value')
      option.textContent = this.getAttribute('label')
      select.append(option)
      this.remove()
    } else {
      const id = generateID()
      const input = document.createElement('input')
      input.id = id
      input.type = this.parentNode.parentNode.getAttribute('type')
      input.name = this.parentNode.parentNode.getAttribute('name')
      input.value = this.getAttribute('value')
      this.append(input)

      if(this.getAttribute('checked') == 'true'){
        input.setAttribute('checked', true)
      }

      const label = document.createElement('label')
      label.innerHTML = this.getAttribute('label')
      label.setAttribute('for', id)
      this.append(label)

      input.addEventListener('change', e => {
        this.dispatchEvent(new CustomEvent('interact', { detail: input.checked }))
      })
    }
  }
}

export { adaForm, adaInput, adaOption }