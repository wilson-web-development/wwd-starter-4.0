import { setStyle, wrap, generateID, slideUp, slideDown } from '../plugins/helperFunctions'

/**
 * Attributes:
 *   group | what group this tab belongs to? | example: `tabs`
 *   id | html ID, required for ADA/aria labels | example: `tab-title-1`
 *   for | html ID of the toggler's target | example: `tab-content-1`
 *   initially-active | true or false, a group should only have 1 ada-toggler set to true
 *   can-close-all | true or false, should the user be able to close all tabs in this group or should 1 always stay open?
 *   slide | true or false, should the target slide open or just appear/disappear mmediately 
 */

class adaToggler extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
    
    //wrap the inside in a button
    this.innerHTML = '<button>' + this.innerHTML + '</button>'
    
    if(!this.id){
      console.warn('ADA toggler should have an ID')
    }
    
    //set the parent ID on this one
    this.firstChild.id = this.id
    this.removeAttribute('id')
    
    if(!this.getAttribute('group')){
      this.setAttribute('group', generateID())
    }

    if(!this.querySelectorAll('h2,h3,h4,h5,h6').length){
      console.warn('ADA Toggler should have a header as a child', this)
    }
    
    this.firstChild.setAttribute('aria-controls', this.getAttribute('for'))
    this.firstChild.setAttribute('aria-expanded', 'false')
    
    let targetEl = document.querySelector('#' + this.getAttribute('for'))
      
    if(targetEl){
      targetEl.setAttribute('aria-labelledby', this.firstChild.id)
      targetEl.setAttribute('role', 'region')
      targetEl.style.display = 'none'
    } else {
      console.warn('No target for ada toggler ', this)
    }

    this.firstChild.addEventListener('keydown', e => {
      if(e.key == 'ArrowRight'){
        e.preventDefault()
        const allTabsInGroup = document.querySelectorAll(`ada-toggler[group="${this.getAttribute('group')}"]`)
        let nextTogglerIndex

        allTabsInGroup.forEach((toggler, index) => {
          if(toggler == this){
            nextTogglerIndex = index + 1
          }
        })

        if(nextTogglerIndex > allTabsInGroup.length - 1){
          nextTogglerIndex = 0
        }

        allTabsInGroup[nextTogglerIndex].firstChild.focus()
      }

      if(e.key == 'ArrowLeft'){
        e.preventDefault()
        const allTabsInGroup = document.querySelectorAll(`ada-toggler[group="${this.getAttribute('group')}"]`)
        let nextTogglerIndex
        
        allTabsInGroup.forEach((toggler, index) => {
          if(toggler == this){
            nextTogglerIndex = index - 1
          }
        })

        if(nextTogglerIndex < 0){
          nextTogglerIndex = allTabsInGroup.length - 1
        }

        allTabsInGroup[nextTogglerIndex].firstChild.focus()
      }

      if(e.key == 'Home'){
        e.preventDefault()
        const allTabsInGroup = document.querySelectorAll(`ada-toggler[group="${this.getAttribute('group')}"]`)
        allTabsInGroup[0].firstChild.focus()
      }
      if(e.key == 'End'){
        e.preventDefault()
        const allTabsInGroup = document.querySelectorAll(`ada-toggler[group="${this.getAttribute('group')}"]`)
        allTabsInGroup[allTabsInGroup.length - 1].firstChild.focus()
      }
    })
    
    this.firstChild.addEventListener('click', e => {
      document.querySelectorAll(`ada-toggler[group="${this.getAttribute('group')}"]`).forEach(el => {
        let target = document.querySelector('#' + el.getAttribute('for'))
        
        if(!target){
          return
        }
                
        let style = window.getComputedStyle(target)

        if(el == this){
          //if the current element is `this`, do some extra processing to see if we are showing it or hiding it
          if(style.display === 'none'){
            this.firstChild.setAttribute('aria-expanded', 'true')
            if(this.getAttribute('slide') == 'false'){
              target.style.display = ''
            } else {
              slideDown(target) 
            }
          } else {
            const canCloseAll = this.getAttribute('can-close-all')

            if(canCloseAll == 'false' || canCloseAll == 0){
              return
            }

            this.firstChild.setAttribute('aria-expanded', 'false')
            if(this.getAttribute('slide') == 'false'){
              target.style.display = 'none'
            } else {
              slideUp(target) 
            }
          }
        } else {
          //close any element that isn't `this`
          el.firstChild.setAttribute('aria-expanded', 'false')
          if(this.getAttribute('slide') == 'false'){
            target.style.display = 'none'
          } else {
            slideUp(target) 
          }
        }
      })
    })


    let initialActive = this.hasAttribute('initially-active') ? this.getAttribute('initially-active') : false
    if(initialActive == 'true' || initialActive == 1){
      setTimeout(() => {
        this.firstChild.click()
      }, 1)
    }
  }
}

export default adaToggler