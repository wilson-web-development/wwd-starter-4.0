function numberWithCommas(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function animateValue(obj, start, end, duration, decimalPlaces = 0) {
  let startTimestamp = null;
  const step = (timestamp) => {
    if (!startTimestamp) {
      startTimestamp = timestamp;
    }

    const progress = Math.min((timestamp - startTimestamp) / duration, 1)
    const rawNumber = (progress * (end - start) + start).toFixed(decimalPlaces)
    const formattedNumber = numberWithCommas(rawNumber)

    obj.innerHTML = formattedNumber

    if (progress < 1) {
      window.requestAnimationFrame(step);
    }
  };
  window.requestAnimationFrame(step);
}

function decimalPlaces(value) {
  if(Math.floor(value) === value || value.indexOf('.') == -1){ 
    return 0
  } else {
    return value.toString().split(".")[1].length || 0
  }
}

class countOnScroll extends HTMLElement {
  constructor() {
    super()
    
    this.innerHTML = 0    

    const observer = new IntersectionObserver(entries => {
      const entry = entries[0]
      const target = entry.target
      
      if(entry.isIntersecting === true && !target.getAttribute('animated')){
        this.setAttribute('animated', true)
        const to = this.getAttribute('to')
        const speed = this.getAttribute('speed') || 1000
        animateValue(this, 0, to, speed, decimalPlaces(to))
      }
    }, { threshold: [ 0.5 ] })

    setTimeout(() => {
      observer.observe(this)
    }, 1000)
  }
}

export default countOnScroll