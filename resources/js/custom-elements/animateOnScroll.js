import { setStyle } from '../plugins/helperFunctions'

class animateOnScroll extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }
    
    if(this.getAttribute('animation') == 'none'){
      setStyle(this, { display: 'block', opacity: 1 })
      return false;
    }

    setStyle(this, {
      display: 'block',
      opacity: 0,
      'will-change' : 'opacity, transform',
      transition: 'none'
    })

    const pxVisibleToAnimate = this.getAttribute('px-visible') || 100
    const threshold = this._calculateThreshold(pxVisibleToAnimate)
        
    const observer = new IntersectionObserver(entries => {
      const entry = entries[0]
      const target = entry.target
      const delay = target.getAttribute('delay') || 100
      const speed = target.getAttribute('speed') || 1000
      
      if(entry.isIntersecting === true && !target.getAttribute('animated')){
        target.setAttribute('animated', true)
        
        switch(target.getAttribute('animation')){
          case 'up': 
            setTimeout(() => setStyle(target, {transform: 'translate3d(0, 100px, 0)' }), 0)
            break
          case 'down': 
            setTimeout(() => setStyle(target, {transform: 'translate3d(0, -100px, 0)' }), 0)
            break
          case 'left': 
            setTimeout(() => setStyle(target, {transform: 'translate3d(-100px, 0, 0)' }), 0)
            break
          case 'right': 
            setTimeout(() => setStyle(target, {transform: 'translate3d(100px, 0, 0)' }), 0)
            break
          case 'scale': 
            setTimeout(() => setStyle(target, {transform: 'scale(0.65)' }), 0)
            break
        }
        
        setTimeout(() => setStyle(target, { transform: '', opacity: 1, transition: `opacity ${speed}ms, transform ${speed}ms` }), delay)
      }
    }, { threshold: [ threshold ] })

    setTimeout(() => {
      observer.observe(this)
    })
  }

  _calculateThreshold(pxVisible){
    return Math.min(pxVisible / this.offsetHeight, 1)
  }
}

export default animateOnScroll