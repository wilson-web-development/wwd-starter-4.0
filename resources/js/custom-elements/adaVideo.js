import { wrap, setStyle } from '../plugins/helperFunctions'

class animateOnScroll extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    const source = this.getAttribute('source')
    const poster = this.getAttribute('poster')
    const alt = this.getAttribute('alt')
    const title = this.getAttribute('title')

    if(!source){
      console.warn('ada video must have a source')
      return
    }
    if(!title){
      console.warn('ada video must have a title')
      return
    }
    if(poster && !alt){
      console.warn('ada video with poster must have an alt')
      return
    }

    const iframe = document.createElement('iframe')
    iframe.setAttribute('src', source)
    iframe.setAttribute('title', title)
    setStyle(iframe, { width: '100%', height: '100%' })

    if(poster){
      const imageContainer = document.createElement('div')
      const closeButton = document.createElement('button')
      const image = document.createElement('img')

      image.setAttribute('loading', 'lazy')
      image.setAttribute('alt', alt)
      image.setAttribute('src', poster)

      closeButton.setAttribute('alt', 'Close poster and play video')
      closeButton.innerText = 'Play'

      setStyle(imageContainer, { position: 'relative', width: '100%', height: '100%' })      
      setStyle(image, { width: '100%', height: '100%' })      
      setStyle(closeButton, { position: 'absolute', top: '50%', left: '50%', transform: 'translate(-50%, -50%)', zIndex: 1 })   

      this.append(imageContainer)
      imageContainer.append(image)
      imageContainer.append(closeButton)

      closeButton.addEventListener('click', e => {
        iframe.setAttribute('src', iframe.getAttribute('src') + '?autoplay=1')
        iframe.setAttribute('allow', 'autoplay')
        iframe.setAttribute('allowFullScreen', 'true')
        this.append(iframe)
        imageContainer.remove()
      })
    } else {
      if(iframe.src.indexOf('autoplay=1') != false){
        iframe.setAttribute('allow', 'autoplay')
      }
      this.append(iframe)
    }

    setStyle(this, { display: 'block' })
  }
}

export default animateOnScroll