import { setStyle, wrap } from '../plugins/helperFunctions'

class adaModal extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
    console.log("toast")
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    if(!this.id){
      console.warn('ADA modal needs an ID', this)
      this.remove()
      return;
    }
    
    if(!this.querySelectorAll('h1, h2, h3, h4, h5, h6').length){
      console.warn('#' + this.id + ' should have a heading')
    }
    
    //hide it
    this.style.display = 'none'
    this.setAttribute('aria-hidden', true)
    this.setAttribute('aria-modal', true)
    this.setAttribute('role', 'dialog')
    
    //add aria tags to the button(s)
    const togglers = document.querySelectorAll(`[modal-id="${this.id}"]`)
    if(togglers){
      togglers.forEach(toggler => {
        if(toggler.tagName != 'BUTTON'){
          console.warn('ADA modal trigger should be a button', toggler)
        }
        toggler.setAttribute('aria-expanded', false)
        toggler.setAttribute('aria-haspopup', 'dialog')
      })
    }
    
    //listen for a button click to open
    document.addEventListener('click', e => {
      let target
      let toggler
      
      if(e.target.hasAttribute('modal-id')){
        target = e.target.getAttribute('modal-id')
        toggler = e.target
      } else {
        target = e.target.closest('[modal-id]')
        if(target){
          target = target.getAttribute('modal-id')
        }
        toggler = e.target.closest('[modal-id]')
      }
      
      if(target && target == this.id){
        toggler.setAttribute('aria-expanded', true)
        this._openModal()
      }
    })
    
    //listen for escape
    if(this.getAttribute('esc-to-close') !== 'false'){
      document.addEventListener('keydown', e => {
        if(e.key == 'Escape' && this.style.display != 'none'){
          this._closeModal()
        }
      })
    }
  }
  
  _openModal(){
    this.dispatchEvent(new Event('open'))    
    this.setAttribute('aria-hidden', false)
    
    //wrap in an overlay and style
    const wrapper = wrap(this)
    setStyle(wrapper, {
      position: 'fixed',
      top: 0,
      left: 0,
      width: '100vw',
      height: '100vh',
      background: 'rgba(0,0,0,0.65)',
      transition: '450ms',      
      opacity: 0,
      overflow: 'auto',
      'backdrop-filter': 'blur(3px)',
      'z-index': 999999,
      margin: 0
    })

    //disables scroll (when menu is open)
    document.body.style.top = `-${window.scrollY}px`
    document.body.style.position = 'fixed'
    document.body.style.width = '100%'

    //animate in
    setTimeout(() => { setStyle(wrapper, { opacity: 1 }) }, 0)
    setTimeout(() => { setStyle(this, { display: 'block', 'margin-left': 'auto', 'margin-right': 'auto', opacity: 0, transform: 'translateY(-100px)', transition: '450ms' }) }, 1)
    setTimeout(() => { 
      setStyle(this, { opacity: 1, transform: 'translateY(0)' }) 
      this._trapFocus()
    }, 100)
    
    
    //listen for close events
    if(this.getAttribute('click-backdrop-to-close') != 'false'){
      wrapper.addEventListener('click', e => {
        if(e.target == wrapper){
          this._closeModal()
        }
      })
    }

    const closers = this.querySelectorAll('[ada-modal-closer]')
    
    if(closers.length){
      closers.forEach(closer => {
        if(!closer.dataset.listenerAdded){
          if(closer.tagName != 'BUTTON'){
            console.warn('ADA modal closer should be a button', closer)
          }
          closer.dataset.listenerAdded = true
          closer.addEventListener('click', e => this._closeModal())
        }
      })
    } else {
      console.warn('#' + this.id + ' should have a close button')
    }
  }
  
  _closeModal(){
    const wrapper = this.parentNode

    //enables scrolling again
    const scrollY = document.body.style.top
    document.body.style.top = ''
    document.body.style.position = ''
    document.body.style.width = ''    
    window.scrollTo(0, parseInt(scrollY || '0') * -1)
    
    setStyle(this, { opacity: 0, transform: 'translateY(-100px)' })
    setStyle(wrapper, { opacity: 0 })
    setTimeout(() => {
      this.setAttribute('aria-hidden', true)
      this.style.display = 'none' 
      //unwrap
      wrapper.parentNode.insertBefore(this, wrapper)
      wrapper.remove()
      this.dispatchEvent(new Event('close'))
    }, 500)
  }
  
  _trapFocus(){
    // add all the elements inside modal which you want to make focusable
    const focusableElements = this.querySelectorAll('button, [href], input, select, textarea, [tabindex]:not([tabindex="-1"])');
    const firstFocusableElement = focusableElements.length ? focusableElements[0] : this; // get first element to be focused inside modal
    const lastFocusableElement = focusableElements.length ? focusableElements[focusableElements.length - 1] : this; // get last element to be focused inside modal

    document.addEventListener('keydown', function(e) {
      let isTabPressed = e.key === 'Tab' || e.keyCode === 9;

      if (!isTabPressed) {
        return;
      }

      if (e.shiftKey) { // if shift key pressed for shift + tab combination
        if (document.activeElement === firstFocusableElement) {
          lastFocusableElement.focus(); // add focus for the last focusable element
          e.preventDefault();
        }
      } else { // if tab key is pressed
        if (document.activeElement === lastFocusableElement) { // if focused has reached to last focusable element then focus first focusable element after pressing tab
          firstFocusableElement.focus(); // add focus for the first focusable element
          e.preventDefault();
        }
      }
    });

    this.setAttribute('tabindex', -1)
    this.focus();       
  }
}

export default adaModal