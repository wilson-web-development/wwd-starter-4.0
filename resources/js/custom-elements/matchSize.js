class matchSize extends HTMLElement {
  constructor(){
    super()
    this.loaded = false
  }

  connectedCallback() {
    if(this.loaded){
      return false
    } else {
      this.loaded = true
    }

    this.style.display = 'inline-block'

    if(this.getAttribute('height')){
      this._matchHeight()
      window.addEventListener('resize', () => this._matchHeight())
    }

    if(this.getAttribute('width')){
      this._matchWidth()
      window.addEventListener('resize', () => this._matchWidth())
    }
  }

  _matchHeight(){
    const group = document.querySelectorAll(`[group="${this.getAttribute('group')}"]`)

    let maxHeight = 0
    if(group){
      group.forEach(el => {
        el.style.height = ''

        if(el.offsetHeight > maxHeight){
          maxHeight = el.offsetHeight
        }
      })

      group.forEach(el => {
        el.style.height = `${maxHeight}px`
      })
    }
  }

  _matchWidth(){
    const group = document.querySelectorAll(`[group="${this.getAttribute('group')}"]`)
    let maxWidth = 0
    if(group){
      group.forEach(el => {
        el.style.width = ''

        if(el.offsetWidth > maxWidth){
          maxWidth = el.offsetWidth
        }
      })

      group.forEach(el => {
        el.style.width = `${maxWidth}px`
      })
    }
  }
}

export default matchSize