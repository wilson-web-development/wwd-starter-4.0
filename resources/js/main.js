/**
 * This is the main js file, any global JS can go into here, otherwise it should go in a block
 */
import lazySizes from 'lazysizes'
import touchDetector from './plugins/touchDetector'
import smoothScroller from './plugins/smoothScroller'
import initCustomElements from './custom-elements/_all'
import gravityformHelpers from './plugins/gravityformHelpers'

 //lazy images
lazySizes.cfg.lazyClass = 'lazy'
setTimeout(lazySizes.init, 250)

//aria hide any <br> because they are bad for screen readers
jQuery('br').attr('aria-hidden', 'true')

//add touch detection
touchDetector()

//add smooth scroll to #jump-links
//true|false to account for a sticky/fixed header or not
smoothScroller(false)

//add custom HTML elements
initCustomElements()

//adds helpers for gravityforms
gravityformHelpers()