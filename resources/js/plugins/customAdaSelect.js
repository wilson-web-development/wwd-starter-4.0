/** 
 * Create custom selects that are ADA friendly
 * This structure is staright ripped from https://pattern-library.dequelabs.com/components/selects 2020-01-06
 *
 * // html
 * <div class="custom-select-block">
 *   <label for="animal">Choose a favorite animal</label>
 *   <select id="animal" required>
 *     <option value="" disabled selected>Choose a favorite animal</option>
 *     <option>Dog</option>
 *     <option value="no-one-will-ever-pick-this">Fish</option>
 *     <option>Horse</option>
 *     <option>Bird</option>
 *     <option>Ferret</option>
 *     <option disabled>Not cat</option>
 *   </select>
 * </div>
 *
 * // js
 * new customAdaSelect('.custom-select-block', {
 *   //callback on select
 *   onSelect(value, e){
 *     console.log(value, e)
 *   }
 * })
 */
const $ = jQuery

//binds events on a list of "options"
function bindList(list){
  list.addEventListener('keydown', onKeydown)
  list.addEventListener('click', onSelect)
  list.addEventListener('blur', onBlur)
  document.documentElement.setAttribute('tabindex', '-1')
  document.addEventListener('focus', onFocus, true)
  Array.from(list.children).forEach(child => child.addEventListener('focus', onChildFocus, true))
}

//unbinds events on a list of "options"
function unbindList(list){
  list.removeEventListener('keydown', onKeydown)
  list.removeEventListener('click', onSelect)
  list.removeEventListener('blur', onBlur)
  document.documentElement.removeAttribute('tabindex')
  document.removeEventListener('focus', onFocus, true)
  Array.from(list.children).forEach(child => child.removeEventListener('focus', onChildFocus, true))  
}

//hides a list of "options"
function hideList(list){
  const trigger = list.previousElementSibling
  unbindList(list)
  trigger.setAttribute('aria-expanded', 'false')
  document.body.classList.remove('ada-select--is-visible')

  if(list.classList.contains('ada-select--options')){
    // list.style.display = 'none'
    $(list).slideUp(150)
  }

  setTimeout(() => {
    trigger.focus()
  }, 100)

  //update the active descendant
  setTimeout(() => {
    list.setAttribute('aria-activedescendant', list.querySelector('[aria-selected="true"]').id)
  }, 101)  
};

//show a list of "options"
function showList(list){
  const trigger = list.previousElementSibling

  if(list.classList.contains('ada-select--options')){
    trigger.setAttribute('aria-expanded', 'true')
    // list.style.display = ''  
    $(list).slideDown(150)
    list.focus()
    document.body.classList.add('ada-select--is-visible')
    bindList(list)
  }
}

//updates a selected item
function updateSelected(item, isSelected){
  // item.setAttribute('aria-label', `${item.textContent}${(isSelected) ? ", selected" : ""}`)
  item.setAttribute('aria-selected', isSelected)
}

function updateTrigger(trigger, content, label){
  trigger.textContent = content
  // trigger.setAttribute('aria-label', (label) ? label : content)
}

function findMatch(needle, haystack){
  return haystack.textContent.toLowerCase().startsWith(needle.toLowerCase())
}

function generateID(){
  return `_${Math.random().toString(36).substr(2, 9)}`
}


/**
 * Events
 */

//on focusing a child element in a list, update the aria-activedescendant and aria-selected
function onChildFocus(e){
  e.target.parentElement.setAttribute('aria-activedescendant', e.target.id)
}

function onSelect(event){
  const target = event.target
  const list = target.parentElement
  const isMultiple = list.getAttribute('aria-multiselectable') === 'true'
  const trigger = list.previousElementSibling
  const current = list.querySelector('[aria-selected="true"]')
  const native = list.nextElementSibling
  const items = Array.from(list.querySelectorAll('[role="option"]'))

  if(target.classList.contains('ada-select--optgroup-title') || target.classList.contains('ada-select--option--disabled') || target.classList.contains('ada-select--options')){
    return false
  }

  if (!isMultiple && current !== target) {
    if (current) {
      updateSelected(current, false)
    }

    updateSelected(target, true)
    updateTrigger(trigger, target.textContent, `${target.textContent}, ${trigger.previousElementSibling.textContent}`)

    // Keep native select in sync
    // native.selectedIndex = items.indexOf(target)
    native.value = items[items.indexOf(target)].dataset.value
  }

  if (isMultiple) {

    updateSelected(target, target.getAttribute('aria-selected') !== 'true')

    /* Update trigger label */

    // Build the trigger label from selected items
    const selected =
      Array
        .from(list.querySelectorAll('[aria-selected="true"]'))
        .map(item => item.textContent)
        .join(', ')

    if (selected.length) {
      updateTrigger(trigger, selected, `${selected}, ${trigger.previousElementSibling.textContent}`)
    } else {
      updateTrigger(trigger, trigger.getAttribute('data-placeholder'))
    }

    // Keep native select in sync
    // native[items.indexOf(target)].selected = (current !== target)
    // native.value = 
    // console.log(selected)
  }

  if (!isMultiple) {
    hideList(list)
  }

  //run custom callback but ignore if all options are the target (because it was just open/closed on keyboard)
  if(!event.target.classList.contains('ada-select--options')){
    customADASelectInstances.forEach(instance => {
      instance.els.forEach(parentElement => {
        if(parentElement.contains(event.target)){
          instance.options.onSelect(native.value, event)
        }
      })
    })
  }
}

/* Focus handler on body, to hide listbox. Mostly for iOS */
function onFocus(event) {
  const list = document.querySelector('.ada-select--trigger[aria-expanded="true"]').nextElementSibling
  if (!list.parentElement.contains(event.target)) {
    hideList(list)
  }
}

/* Blur handler on list, to hide listbox */
function onBlur(event) {
  const list = this
  if (!list.contains(event.relatedTarget) && list.previousElementSibling !== event.relatedTarget) {
    hideList(list)
  }
}

/* Keydown handler for events on the list */
function onKeydown(event) {
  const target = event.target
  const key = event.key.replace('Arrow', '')
  const list = this
  const options = Array.from(list.querySelectorAll('[role="option"]'))
  let index = options.indexOf(target)

  switch (key) {
    case 'Up':
      event.preventDefault()
      if (index > 0) {
        options[index -= 1].focus()
      }
      break
    case 'Down':
      event.preventDefault()
      if (index !== options.length - 1) {
        options[index += 1].focus()
      }
      break
    case ' ':
    case 'Spacebar':
    case 'Enter':
      /* Selection made */
      if (!target.hasAttribute('aria-disabled')) {
        event.preventDefault()
        onSelect(event)
      }
      break
    case 'Home':
      event.preventDefault()
      options[0].focus()
      break
    case 'End':
      event.preventDefault()
      options[options.length - 1].focus()
      break
    case 'Esc':
    case 'Escape':
    case 'Tab':

      /* Hide list */
      event.preventDefault()
      hideList(list)
      break
    default:

      /* Type ahead */

      // Do any of the items start with the character? Easy out
      if (options.some(option => findMatch(key, option))) {
        // Find out if an item is already focused
        let focused = options.indexOf(document.activeElement)
        let next

         // Nothing focused, start from the top
        if (focused === -1) {
          next = options.findIndex(option => findMatch(key, option))
        } else {
          const start = focused += 1
          const items = [].concat(options.slice(start), options.slice(0, start))

          next = options.indexOf(items.find(item => findMatch(key, item)))
        }

        // Found something
        if (next !== -1) {
          options[next].focus()
        }
      }
      break
  }
}

/* Event handler for combobox trigger */
function onTrigger(event) {
  const target = event.target

  if ((event.type === 'keydown' && event.key.match(/Up|Down|Spacebar|\s/u)) || event.type === 'click') {
    const isExpanded = target.getAttribute('aria-expanded') === 'true'
    const list = target.nextElementSibling

    event.preventDefault()

    if (isExpanded) {
      hideList(list)
    } else {
      showList(list)
    }
  }
}





let customADASelectInstances = [] //{}

class customAdaSelect {
  constructor(selector, options){
    this._id = Math.floor(Math.random() * Date.now())
    this.els = document.querySelectorAll(selector)
    this.options = $.extend({
      onSelect: () => {}
    }, options)

    //set up a global func for onSelect...kinda janky, fix later if you get a chance!
    // customADASelectInstances.onSelect = this.options.onSelect
    customADASelectInstances.push(this)

    this._init()
  }


  //start everything up
  _init(){
    this.els.forEach(el => {
      el.querySelectorAll('select').forEach(select => {
        //create options HTML
        Array.from(select.children).forEach(option => option.id = 'native-clone-' + option.textContent.toLowerCase().replace(' ', '-').replace(/\W/g, ''))

        const options = this._createOptions(select)        
        const label = el.querySelector(`label[for="${select.id}"]`)
        const labelText = label.textContent
        const selectedOption = this._getSelectedOption(select)
        const firstOptionText = selectedOption.textContent
        const firstOptionID = selectedOption.id.replace('native-clone-', '')
        const isMultiple = select.hasAttribute('multiple')
        const nativeClone = select.cloneNode(true)

        //wrap the native select/label and the new widget
        const wrapper = document.createElement('div');
        wrapper.classList.add('ada-select')
        wrapper.insertAdjacentHTML('afterBegin',
          `
            <span class="ada-select--label" id="${select.id}-label">${labelText}</span>
            <button class="ada-select--trigger" aria-haspopup="listbox" ara-labelledby="${select.id}-label" aria-expanded="false">${firstOptionText}</button>
            <ul class="ada-select--options" role="listbox" id="${select.id}-list" tabindex="-1" aria-activedescendant="${firstOptionID}" style="display:none">${options}</ul>
          `.trim()
        )

        // wrap everything this widget uses then remove the original select and label
        select.parentElement.insertBefore(wrapper, select)
        select.parentElement.removeChild(select)
        label.parentElement.removeChild(label)

        // Set up main handlers
        const trigger = wrapper.querySelector('.ada-select--trigger')
        trigger.addEventListener('click', onTrigger)
        // Since this is not native interactive element, we have to listen to keydown in addition to click
        trigger.addEventListener('keydown', onTrigger)

        //trigger a click if something was preselected
        for(let i = 0; i < nativeClone.children.length; i++){
          if(nativeClone.children[i].selected){
            updateTrigger(trigger, nativeClone.children[i].textContent)
            break;
          }
        }

        // Native select clone should be hidden from all user interaction
        wrapper.appendChild(nativeClone)    
        this._changeInputType(nativeClone, 'hidden')    
      })
    })
  }

  //helper functio to change the type of an input (select to hidden)
  _changeInputType(oldObject, oType) {
    var newObject = document.createElement('input');
    newObject.type = oType;
    if(oldObject.size) newObject.size = oldObject.size;
    if(oldObject.value) newObject.value = oldObject.value;
    if(oldObject.name) newObject.name = oldObject.name;
    if(oldObject.id) newObject.id = oldObject.id;
    if(oldObject.className) newObject.className = oldObject.className;
    oldObject.parentNode.replaceChild(newObject,oldObject);
    return newObject;
  }


  //creates an li of options
  _createOptions(select){
    let options = ''
    Array.from(select.children).forEach(child => {
      if (child.nodeName === 'OPTION') {
        let classes = 'ada-select--option'
        const optionID = 'option-' + child.textContent.toLowerCase().replace(' ', '-').replace(/\W/g, '') + generateID()

        if(child.disabled){
          classes += ' ada-select--option--disabled'
        }

        // Most common type
        options += `<li data-value="${child.value}" tabindex="-1" class="${classes}" id="${optionID}" role="option" ${child.disabled ? 'aria-disabled="true"' : ''} ${child.selected ? 'aria-selected="true"' : ''}>${child.textContent}</li>`;
      } else if (child.nodeName === 'OPTGROUP') {
        /* Grouped options come as an Array, need to treat differently */
        // Generate a random ID real quick */
        const groupID = generateID();
        /* Build the group heading from the optgroup label */
        options += `<li class="ada-select--optgroup-title" id="${groupID}" role="presentation" aria-hidden="true">${child.label}</li>`;
        options += Array.from(child.children).map(item => {
          let classes = 'ada-select--option ada-select--optgroup-option '
          const optionID = 'option-' + child.textContent.toLowerCase().replace(' ', '-').replace(/\W/g, '') + generateID()

          if(item.disabled){
            classes += ' ada-select--option--disabled'
          }

          return `<li data-value="${item.value}" tabindex="-1" class="${classes}" id="${optionID}" role="option" aria-describedby="${groupID}" ${item.disabled ? 'aria-disabled="true"' : ''}  ${item.selected ? 'aria-selected="true"' : ''}>${item.textContent}</li>`
        }).join('');
      }
    })
    return options
  }

  //gets a selected option from a select
  _getSelectedOption(select){
    let selected = select[0]
    for (var i = select.children.length - 1; i >= 0; i--) {
      if(select[i].selected){
        selected = select[i]
        break
      }
    }
    return selected
  }
}

export default customAdaSelect