function gravityformHelpers(){
  function init(){
    //Add aria-hidden to gravity forms required asterisk
    jQuery('.gfield_required').attr('aria-hidden', 'true');
    //Add required attribute
    jQuery('input[aria-required="true"]').each(function(){
      jQuery(this).attr("required", "required");
    })
    jQuery('textarea[aria-required="true"]').each(function(){
      jQuery(this).attr("required", "required");
    })
    //Prevents form from being submitted before validation is complete
    jQuery('input[type="submit"').removeAttr('onkeypress');
    //basic regex for email field validation
    // jQuery('input[type="email"]').attr('pattern', "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,}jQuery")
    //basic regex for tel field validation, specifically for gravity forms' format (123) 456-7890
    jQuery('input[type="tel"]').attr('pattern', "\\([0-9]{3}\\)\\s[0-9]{3}\-[0-9]{4}")

    //Scroll to first invalid input
    function scrollToInvalid(form) {
      var navbar = jQuery('.main-header');

      // listen for `invalid` events on all form inputs
      form.find(':input').on('invalid', function(event) {
        var input = jQuery(this)

        // the first invalid element in the form
        var first = form.find(':invalid').first()

        // only handle if this is the first invalid input
        if (input[0] === first[0]) {
          // height of the nav bar plus some padding
          var navbarHeight = navbar.height() + 50

          // the position to scroll to (accounting for the navbar)
          var elementOffset = input.offset().top - navbarHeight

          // the current scroll position (accounting for the navbar)
          var pageOffset = window.pageYOffset - navbarHeight

          // don't scroll if the element is already in view
          if (elementOffset > pageOffset && elementOffset < pageOffset + window.innerHeight) {
              return true
          }

          // note: avoid using animate, as it prevents the validation message displaying correctly
          jQuery('html,body').scrollTop(elementOffset)
        }
      });
    }
    var form = jQuery('form')
    scrollToInvalid(form)

    //Focus on first invalid input after ajax submit
    jQuery(document).on('gform_post_render', function(){ 
      jQuery('input[aria-invalid="true"]').first().focus() 
    });

    //Add alert to gf confirmation
    jQuery(document).on('gform_confirmation_loaded', function(event, formId){
      jQuery('.gform_confirmation_message').attr('role', 'alert')
    });
  }
  jQuery(document).ready(init)
}

export default gravityformHelpers