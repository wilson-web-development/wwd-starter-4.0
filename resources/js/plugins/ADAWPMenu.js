/**
 * Example usage: 
 *
  new ADAWPMenu('.main-menu .menu', {
    menuTogglerContainer: '', //defaults to the `el` this is being mounted on, but if you have a set up like this: <div class="thing"><div><!-- stuff --></div><ul><!-- wp menu --></ul></div> and you want to trigger "thing" add that here
    menuDisplayWhenOpen: 'block', //the display property of the menu when open
    slideDuration: 450, //the speed for open/close menus
    subMenuDisplayWhenOpen: 'block', //the display property of sub menus
    menuToggler: '', //a class name/ID to show/hide the entire menu
    menuFillsScreenHeightOnToggle: true, //should the menu fill the screen when the menuToggler is clicked?
    menuFillAccountsForHeaderHeight: true, //should the menu fill account for the main header? 
    mobileSwitch: 768, //when does the menu switch to mobile mode? 
    setHiddenWhenMobileMenuIsToggled: [], //an array of classes or IDs to aria-hide when the mobile menu is open    
    mobileSubMenusAutomaticallyClose: false, //i guess they aren't supposed to auto close for ADA purposes? But that is annoying so you can also turn it off    
  })
 */

class ADAWPMenu {
  constructor(el, options){
    this.el = document.querySelector(el)
    this.options = jQuery.extend({
      menuTogglerContainer: '', //defaults to the `el` this is being mounted on, but if you have a set up like this: <div class="thing"><div><!-- stuff --></div><ul><!-- wp menu --></ul></div> and you want to trigger "thing" add that here
      menuDisplayWhenOpen: 'block', //the display property of the menu when open
      slideDuration: 450, //the speed for open/close menus
      subMenuDisplayWhenOpen: 'block', //the display property of sub menus
      menuToggler: '', //a class name/ID to show/hide the entire menu
      menuFillsScreenHeightOnToggle: true, //should the menu fill the screen when the menuToggler is clicked?
      menuFillAccountsForHeaderHeight: true, //should the menu fill account for the main header? 
      mobileSwitch: 768, //when does the menu switch to mobile mode? 
      setHiddenWhenMobileMenuIsToggled: [], //an array of classes or IDs to aria-hide when the mobile menu is open     
      mobileSubMenusAutomaticallyClose: false, //i guess they aren't supposed to auto close for ADA purposes? But that is annoying so you can also turn it off
    }, options)
    this.timeoutAnimationInstances = []

    this._createFirstSubMenuFromParent()

    this._addAria()

    this._enableKeyboardNavigation()

    this._enableDesktopHoverSubmenus()
    this._enableDesktopHoverSubSubmenus()

    this._enableMobileMenuButtonToggle()
    this._enableMobileMenuSubmenuClicks()
    

    //watch for class changes to submenus (that way this runs whether the submenu is opened via keyboard or mouse)
    const observer = new MutationObserver((mutation) => this._handleMutation(mutation))
    observer.observe(this.el, { childList: true, attributes: true, subtree: true })
  }

  //adds aria tags to required elements
  _addAria(){
    const parent = this.el.parentNode
    if(parent.tagName !== 'NAV'){
      console.warn('An ADA Menu should be wrapped in a <nav>')
    }

    if(!parent.hasAttribute('aria-label')){
      console.warn('An ADA Menu\'s parent should have an aria-label')
    }

    //copy the parent's aria-label to this element
    this.el.setAttribute('aria-label', parent.getAttribute('aria-label'))
    this.el.setAttribute('role', 'menubar')
    this._addChildAria(this.el)
  }

  //find children and add aria stuff to them
  //el here is going to be a UL containing LI.menu-item(s)
  _addChildAria(el){
    if(el.children){
      Array.from(el.children).forEach(menuItem => {

        menuItem.setAttribute('role', 'none')

        const link = menuItem.querySelector('a')
        if(link){
          link.setAttribute('role', 'menuitem')
        }

        if(menuItem.classList.contains('menu-item-has-children')){
          link.setAttribute('aria-haspopup', true)
          link.setAttribute('aria-expanded', false)

          const submenu = menuItem.querySelector('.sub-menu')
          submenu.setAttribute('role', 'menu')
          submenu.setAttribute('aria-label', menuItem.querySelector('a').innerText)
          this._addChildAria(submenu)
        }
      })
    }
  }

  //enables submenu actions (click on mobile|hover on desktop)
  _enableDesktopHoverSubSubmenus(){
    const childrenWithSubmenus = this.el.querySelectorAll('.sub-menu .menu-item-has-children')

    childrenWithSubmenus.forEach(el => {
      el.addEventListener('mouseenter', e => {
        el.parentNode.querySelector('.sub-menu').classList.toggle('open')
      })

      el.addEventListener('mouseleave', e => {
        el.parentNode.querySelector('.sub-menu').classList.toggle('open')
      })
    })
  }

  //enables the open/close menu toggler, generally on mobile only
  _enableMobileMenuButtonToggle(){
    if(this.options.menuToggler){
      const toggler = document.querySelector(this.options.menuToggler)

      if(toggler.tagName !== 'BUTTON'){
        console.warn('An ADA Menu\'s toggler should be a button')
      }

      toggler.setAttribute('aria-expanded', false)
      toggler.setAttribute('aria-label', 'Open Menu')

      toggler.addEventListener('keydown', e => {
        if(window.innerWidth <= this.options.mobileSwitch && e.shiftKey && e.key == 'Tab'){
          const lastTabbable = this._getTabbables('last')
          lastTabbable.focus()
          e.preventDefault()
        }
      })

      toggler.addEventListener('click', e => {
        const el = this.options.menuTogglerContainer ? document.querySelector(this.options.menuTogglerContainer) : this.el

        setTimeout(() => {
          document.querySelector('html').classList.toggle('ada-wp-menu-open')
          el.classList.toggle('open')
          toggler.classList.toggle('open')
        }, 0)

        if(this._elIsVisible(el)){
          this._enableScroll()
          this._slideUp(el)
          this._setHiddenWhenMobileMenuIsToggled(false)
          toggler.setAttribute('aria-expanded', false)
          toggler.setAttribute('aria-label', 'Open Menu')          
        } else {
          this._disableScroll()
          this._slideDown(el)
          this._setHiddenWhenMobileMenuIsToggled(true)
          toggler.setAttribute('aria-expanded', true)
          toggler.setAttribute('aria-label', 'Close Menu')
          const firstTabbable = this._getTabbables('first')
          firstTabbable.focus()
        }
      })
    }
  }

  _setHiddenWhenMobileMenuIsToggled(hidden){
    if(this.options.setHiddenWhenMobileMenuIsToggled.length){
      this.options.setHiddenWhenMobileMenuIsToggled.forEach(selector => {
        var el = document.querySelector(selector)
        if(el){
          el.setAttribute('aria-hidden', hidden)
        }
      })
    }
  }
  
  //enables scrolling again
  _enableScroll(){
    const scrollY = document.body.style.top
    document.body.style.top = ''
    document.body.style.position = ''
    document.body.style.width = ''    
    window.scrollTo(0, parseInt(scrollY || '0') * -1)
  }

  //disables scroll (when menu is open)
  _disableScroll(){
    document.body.style.top = `-${window.scrollY}px`
    document.body.style.position = 'fixed'
    document.body.style.width = '100%'
  }

  //enables either click or hover mouse actions on sub menus
  _enableDesktopHoverSubmenus(){
    if(window.innerWidth <= this.options.mobileSwitch){
      return false
    }

    const topLevelSubmenuTogglers = this.el.querySelectorAll(':scope > .menu-item-has-children')

    topLevelSubmenuTogglers.forEach(el => {
      el.addEventListener('mouseenter', e => {
        const menuShouldBeClosed = el.querySelector('.sub-menu').classList.contains('open')

        this._closeAllTopLevelSubMenus()

        if(!menuShouldBeClosed){
          el.querySelector('.sub-menu').classList.toggle('open')
        }
      })
    })

    topLevelSubmenuTogglers.forEach(el => {
      el.addEventListener('mouseleave', e => {
        this._closeAllTopLevelSubMenus()
      })
    })

    const allLinks = this.el.querySelectorAll(':scope > .menu-item > a')
    allLinks.forEach(link => {
      link.addEventListener('focus', e => {
        this._closeAllTopLevelSubMenus()
      })
    })
  }

  //handles keyboard navigation events
  _enableKeyboardNavigation(){

    const el = window.innerWidth <= this.options.mobileSwitch && this.options.menuTogglerContainer ? document.querySelector(this.options.menuTogglerContainer) : this.el
    
    if(this.options.menuToggler){
      const toggler = document.querySelector(this.options.menuToggler)

      toggler.addEventListener('keydown', e => {
        if(e.key == 'Tab'){
          const tabbables = this._getTabbables()

          if(!tabbables.length){
            return
          }

          if(e.shiftKey){
            tabbables[tabbables.length - 1].focus()
            e.preventDefault()
          } else {
            tabbables[1].focus() //1 because toggler is 0
            e.preventDefault()
          }
        }
      })
    }
    el.addEventListener('keydown', e => {

      // SPACEBAR
      if (e.code === 'Space' || e.which === 32) {
        e.preventDefault()
        this._handleArrowDown()
      }       

      // ALPHA KEY
      else if( (e.keyCode >= 48 && e.keyCode <= 57) || (e.keyCode >= 65 && e.keyCode <= 90) || (e.keyCode >= 97 && e.keyCode <= 122) ){
        // maybe there is a better way to check for alpha key
        
        let letter = e.key.toLowerCase()
        
        const focusableAnchors = Array.from(this.el.querySelectorAll('.menu-item a'))

        const firstLetters = focusableAnchors.map(el => el.innerText[0].toLowerCase())

        // focus anything in the menu from this point forwards that starts with $letter
        var startingPosition = 0
        for (var i = focusableAnchors.length - 1; i >= 0; i--) {
          if(focusableAnchors[i] == document.activeElement){
            startingPosition = i + 1
          }
        }

        let targetIndex = firstLetters.indexOf(letter, startingPosition)

        if (targetIndex !== -1 )  {
          focusableAnchors[targetIndex].focus()
        }
        
      } else {
        let isSubmenu = document.activeElement.closest('.sub-menu')

        switch(e.key){
          case 'Enter':
            if (e.target.closest('.menu-item').classList.contains('menu-item-has-children')) {
              e.preventDefault()

              const submenu = e.target.closest('.menu-item').querySelector('.sub-menu')

              if(submenu.classList.contains('open') && window.innerWidth <= this.options.mobileSwitch){
                submenu.classList.remove('open')
              } else {
                this._handleArrowDown()
              }
            }
            break
          case 'Home':
            e.preventDefault()
            this._focusTabbable('first')
          break
          case 'End':
            e.preventDefault()
            this._focusTabbable('last')
          break
          case 'ArrowUp':
            e.preventDefault()

            //circle back to the last item of that submenu
            let isFirstItemInSubmenu = false
            if(isSubmenu){
              let firstChild = isSubmenu.querySelector(':scope > .menu-item:first-child a')
              isFirstItemInSubmenu = firstChild == document.activeElement

              let prevSubmenu = isSubmenu.parentNode.closest('.sub-menu')
              if(prevSubmenu && isFirstItemInSubmenu){
                //we are on a sub level submenu... so focus the `a` that opened this one
                this._slideUp(document.activeElement.closest('.sub-menu'))
                var link = document.activeElement.closest('.sub-menu').parentNode.querySelector('a')
                link.focus()
                link.setAttribute('aria-expanded', false)
                return false;
              }
              // this._slideUp(isSubmenu)
            }
    
            if(isFirstItemInSubmenu){
              // i guess we aren't looping... just go to the above sub menu
              //if we are currently focusing the first menu item of a submenu
              // this._focusTabbable('sub-menu-last')
              isSubmenu.closest('.menu-item').querySelector('a').focus()
            } else {
              this._focusTabbable('prev')  
            }
            break
          case 'ArrowRight':
            e.preventDefault()
            if(isSubmenu){
              let hasSubmenu = document.activeElement.parentNode.classList.contains('menu-item-has-children')
              if(hasSubmenu){
                //if we are in a submenu but it has its own submenu, drop into its submenu
                document.activeElement.parentNode.querySelector('.sub-menu').classList.add('open')
                setTimeout(() => this._focusTabbable('next'), 0)
              } else{
                //if we are currently in a submenu, focus the top level item to the right
                let nextTopLevelMenuItem = this.getNextTopLevelMenuItem(document.activeElement)
                nextTopLevelMenuItem.querySelector('a').focus()
              }
            } else {
              //we are top level menu item.... just go to the next one! 
              this._focusTabbable('next')
            }
            break
          case 'ArrowDown':
            e.preventDefault()
            //if we are currently focusing the last menu item of a submenu
            //circle back to the first item of that submenu
            let isLastItemInSubmenu = false
            if(isSubmenu){
              let lastChild = isSubmenu.querySelector(':scope > .menu-item:last-child a')
              isLastItemInSubmenu = lastChild == document.activeElement
            }

            if(isLastItemInSubmenu){
              //i guess we don't want to loop? just stop respecting the down arrow
              // this._focusTabbable('sub-menu-first')
            } else {
              this._handleArrowDown()
            }
            break
          case 'ArrowLeft':
            e.preventDefault()

            //if we are in a submenu handle this differently
            if(isSubmenu){
              let prevSubmenu = isSubmenu.parentNode.closest('.sub-menu')
              if(prevSubmenu){
                //we are on a sub level submenu... so focus the `a` that opened this one
                this._slideUp(document.activeElement.closest('.sub-menu'))
                var link = document.activeElement.closest('.sub-menu').parentNode.querySelector('a')
                link.focus()
                link.setAttribute('aria-expanded', false)
              } else {
                //we are on a first level submenu... so focus the previous top level menu item
                let getPrevTopLevelMenuItem = this.getPrevTopLevelMenuItem(document.activeElement)
                getPrevTopLevelMenuItem.querySelector('a').focus()
              }
            } else {
              //we are on a top level menu item... so just focus the next item
              this._focusTabbable('prev')
            }
            break
          case 'Tab':     
            if(window.innerWidth > this.options.mobileSwitch){
              break
            }

            let focusedIndex = 0
            const tabbables = this._getTabbables()
            tabbables.forEach((tabbable, index) => {
              if(document.activeElement == tabbable){
                focusedIndex = index
              }
            })

            if(e.shiftKey && focusedIndex == 1){
              tabbables[0].focus()
              e.preventDefault()
            }

            if(!e.shiftKey && focusedIndex == tabbables.length - 1){
              tabbables[0].focus()
              e.preventDefault()
            }
            break
        }
      }
    })

    // this.el.querySelectorAll(':scope > li > a').forEach(link => {
    //   link.addEventListener('focus', e => {
    //     this._closeAllTopLevelSubMenus()
    //   })
    // }) 
  }

  _closeAllTopLevelSubMenus(){
    this.el.querySelectorAll('.open').forEach(open => open.classList.remove('open'))
  }

  //returns first tabbable element, last, or all
  //@param type | choice: first, last, null (for all)
  _getTabbables(type){
    const el = window.innerWidth <= this.options.mobileSwitch && this.options.menuTogglerContainer ? document.querySelector(this.options.menuTogglerContainer) : this.el    
    let tabbables = Array.from(el.querySelectorAll('a, button, input, [tabindex]:not([tabindex="-1"]'))

    //add the toggler
    if(el.classList.contains('open')){
      const toggler = document.querySelector(this.options.menuToggler)
      tabbables.unshift(toggler)
    }

    //remove anything that can't be seen
    tabbables = tabbables.filter(el => this._elIsVisible(el) && !el.hasAttribute('disabled'))

    return type == 'first' ? tabbables[0] : type == 'last' ? tabbables[tabbables.length - 1] : tabbables
  }

  //focus a the next or previous tabbable element
  _focusTabbable(type){
    const tabbables = Array.from(this.el.querySelectorAll('a, button, input, [tabindex]:not([tabindex="-1"]')).filter(el => this._elIsVisible(el) && !el.hasAttribute('disabled'))
    for (var i = tabbables.length - 1; i >= 0; i--) {
      if(tabbables[i] == document.activeElement){
        if(type == 'next'){
          if(typeof tabbables[i + 1] != 'undefined'){
            tabbables[i + 1].focus()
          } else {
            tabbables[0].focus()
          }
        } else if (type == 'sub-menu-first') {
          let subMenuItems = tabbables[i].closest('.menu-item-has-children').querySelectorAll('ul.sub-menu li.menu-item')
          if (subMenuItems) {
            let lastItem = subMenuItems[0]
            lastItem.querySelector('a').focus()
          }
        } else if (type == 'sub-menu-last') {
          let subMenuItems = tabbables[i].closest('.menu-item-has-children').querySelectorAll('ul.sub-menu li.menu-item')
          if (subMenuItems) {
            let lastItem = subMenuItems[subMenuItems.length - 1]
            lastItem.querySelector('a').focus()
          }
        } else if(type == 'last'){
          tabbables[tabbables.length - 1].focus()
        } else if(type == 'first'){
          tabbables[0].focus()
        } else {
          if(typeof tabbables[i - 1] != 'undefined'){
            tabbables[i - 1].focus()
          } else {
            tabbables[tabbables.length - 1].focus()
          }
        }
        break
      }
    }
  }

  //handle an arrow down event
  _handleArrowDown(){
    let activeParent = document.activeElement.parentNode
    let isTopLevelMenuItem = Array.from(this.el.querySelectorAll(':scope > .menu-item')).filter(e => e == activeParent).length
    //we only want to open the submenu if we are on a top level menu item
    if(isTopLevelMenuItem && activeParent.classList.contains('menu-item-has-children')){
      activeParent.querySelector('.sub-menu').classList.add('open')
    }
    setTimeout(() => this._focusTabbable('next'), 0)
  }

  _enableMobileMenuSubmenuClicks() {
    if (window.innerWidth > this.options.mobileSwitch) {
      return
    }
    
    let mobileLinks = this.el.querySelectorAll('.menu-item-has-children > a')
    if (mobileLinks) {
      mobileLinks.forEach(link => {
        link.addEventListener('click', e => {
          const submenu = e.target.closest('.menu-item-has-children').querySelector('.sub-menu')

          if(submenu.classList.contains('open')){
            submenu.classList.remove('open')
          } else {
            if(this.options.mobileSubMenusAutomaticallyClose){
              this._closeAllTopLevelSubMenus()
            }
            setTimeout(() => submenu.classList.add('open'))
          }
        })
      })
    }
  }

  /**
   * This duplicates a parent as a submenu item if that parent is a link
   * If the parent is a #, this is ignored
   */
  _createFirstSubMenuFromParent() {
    let parentMenuItems = document.querySelectorAll('.menu-item-has-children')
    if (parentMenuItems) {
      parentMenuItems.forEach(menu => {
        
        // check if parent has a link or a #
        let href = menu.querySelector('a').getAttribute('href')
        let subMenu = menu.querySelector('ul.sub-menu')

        // if link, add as first sub menu 
        if (href && href[0] !== '#') {

          // create the li
          var firstLi = document.createElement('li')
          firstLi.setAttribute('class', 'menu-item')
          firstLi.setAttribute('role', 'none')
          
          // create the anchor tag
          var firstLiAnchor = document.createElement('a')
          firstLiAnchor.setAttribute('role', 'menuitem')
          firstLiAnchor.setAttribute('href', href)
          firstLiAnchor.innerHTML = menu.querySelector('a').innerHTML

          // append as needed 
          firstLi.appendChild(firstLiAnchor)
          subMenu.insertBefore(firstLi, subMenu.firstChild)

          // clear out the initial href from the parent
          menu.querySelector('a').setAttribute('href', '#')

        }
      })
    }
  }

  //checks if an element is visible or not
  _elIsVisible(el){
    return el.offsetWidth > 0 || el.offsetHeight > 0 || el.getClientRects().length > 0
  }

  //handle mutations on children of the main element
  _handleMutation(mutation){
    const data = mutation[0]
    //whenever the sub menu class has changed do this
    if(data.attributeName == 'class' && data.target.classList.contains('sub-menu')){
      if(data.target.classList.contains('open')){
        data.target.parentNode.querySelector('a').setAttribute('aria-expanded', true)
        this._slideDown(data.target)
      } else {
        data.target.parentNode.querySelector('a').setAttribute('aria-expanded', false)
        this._slideUp(data.target)
        //close any open submenus
        var allSubSubmenus = this.el.querySelectorAll('.sub-menu .sub-menu')
        if(allSubSubmenus){
          this.el.querySelectorAll('.sub-menu a[aria-expanded="true"]').forEach(el => el.setAttribute('aria-expanded', false))
          allSubSubmenus.forEach(el => el.style.display = 'none')
        }
      }
    }
  }

  //slides a target up to hide it
  _slideUp(target){
    if(target.dataset.timeoutInstance){
      //clear the timeout and remove it from instances
      clearTimeout(this.timeoutAnimationInstances.filter(x => x == target.dataset.timeoutInstance)[0])
      this.timeoutAnimationInstances = this.timeoutAnimationInstances.filter(x => x != target.dataset.timeoutInstance)
    }
    target.style.transitionProperty = 'height, margin, padding'
    target.style.transitionDuration = this.options.slideDuration + 'ms'
    target.style.boxSizing = 'border-box'
    target.style.height = target.offsetHeight + 'px'
    target.offsetHeight
    target.style.overflow = 'hidden'
    target.style.height = 0
    target.style.paddingTop = 0
    target.style.paddingBottom = 0
    target.style.marginTop = 0
    target.style.marginBottom = 0
    
    const timeout = window.setTimeout( () => {
      target.style.display = 'none'
      target.style.removeProperty('height')
      target.style.removeProperty('padding-top')
      target.style.removeProperty('padding-bottom')
      target.style.removeProperty('margin-top')
      target.style.removeProperty('margin-bottom')
      target.style.removeProperty('overflow')
      target.style.removeProperty('transition-duration')
      target.style.removeProperty('transition-property')

      this.timeoutAnimationInstances = this.timeoutAnimationInstances.filter(x => x != target.dataset.timeoutInstance)      
      target.dataset.timeoutInstance = 0
    }, this.options.slideDuration)

    this.timeoutAnimationInstances.push(timeout)
    target.dataset.timeoutInstance = timeout
  }

  //slides a target down to display it
  _slideDown(target){
    if(target.dataset.timeoutInstance){
      //clear the timeout and remove it from instances
      clearTimeout(this.timeoutAnimationInstances.filter(x => x == target.dataset.timeoutInstance)[0])
      this.timeoutAnimationInstances = this.timeoutAnimationInstances.filter(x => x != target.dataset.timeoutInstance)
    }

    target.style.removeProperty('display')
    let display = window.getComputedStyle(target).display

    if (display === 'none'){
      display = target == this.el ? this.options.menuDisplayWhenOpen : this.options.subMenuDisplayWhenOpen
    }

    target.style.display = display
    let height = target.offsetHeight

    const parentEl = this.options.menuTogglerContainer ? document.querySelector(this.options.menuTogglerContainer) : this.el

    if(target == parentEl && this.options.menuFillsScreenHeightOnToggle){

      if(this.options.menuFillAccountsForHeaderHeight){
        const header = this.el.closest('header')
        height = window.innerHeight - header.offsetHeight
      } else {
        height = window.innerHeight
      }
    }

    target.style.overflow = 'hidden'
    target.style.height = 0
    target.style.paddingTop = 0
    target.style.paddingBottom = 0
    target.style.marginTop = 0
    target.style.marginBottom = 0
    target.offsetHeight
    target.style.boxSizing = 'border-box'
    target.style.transitionProperty = "height, margin, padding"
    target.style.transitionDuration = this.options.slideDuration + 'ms'
    target.style.height = height + 'px'
    target.style.removeProperty('padding-top')
    target.style.removeProperty('padding-bottom')
    target.style.removeProperty('margin-top')
    target.style.removeProperty('margin-bottom')

    //add a push-left helper class if needed
    const viewportOffset = target.getBoundingClientRect()
    if(target.offsetWidth + viewportOffset.left > window.innerWidth){
      target.classList.add('push-left')
    }

    const timeout = window.setTimeout( () => {
      if(target != parentEl || !this.options.menuFillsScreenHeightOnToggle){
        target.style.removeProperty('height')
      }
      target.style.removeProperty('overflow')
      target.style.removeProperty('transition-duration')
      target.style.removeProperty('transition-property')

      this.timeoutAnimationInstances = this.timeoutAnimationInstances.filter(x => x != target.dataset.timeoutInstance)      
      target.dataset.timeoutInstance = 0      
    }, this.options.slideDuration)

    this.timeoutAnimationInstances.push(timeout)
    target.dataset.timeoutInstance = timeout    
  }

  getNextTopLevelMenuItem(currentFocusedElement){
    let menuItem = null
    const allTopLevelLinks = this.el.querySelectorAll(':scope > .menu-item')
    if(allTopLevelLinks){
      for(let i = 0; i < allTopLevelLinks.length; i++){
        if(allTopLevelLinks[i].contains(currentFocusedElement)){
          if(i == allTopLevelLinks.length - 1){ //if we are on the last item, grab the first one instead
            menuItem = allTopLevelLinks[0]
          } else {
            menuItem = allTopLevelLinks[i + 1]
          }
        }
      }
    }
    return menuItem
  }

  getPrevTopLevelMenuItem(currentFocusedElement){
    let menuItem = null
    const allTopLevelLinks = this.el.querySelectorAll(':scope > .menu-item')
    if(allTopLevelLinks){
      for(let i = 0; i < allTopLevelLinks.length; i++){
        if(allTopLevelLinks[i].contains(currentFocusedElement)){
          if(i == 0){ //if we are on the first item, grab the last one instead
            menuItem = allTopLevelLinks[allTopLevelLinks.length - 1]
          } else {
            menuItem = allTopLevelLinks[i - 1]
          }
        }
      }
    }
    console.log()
    return menuItem
  }
}

export default ADAWPMenu