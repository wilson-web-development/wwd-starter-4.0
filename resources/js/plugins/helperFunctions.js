/**
 * Sets an element's style
 * @param el | DOM element
 * @param style | Object
 */ 
function setStyle(el, style){
  Object.keys(style).forEach(key => {
    el.style[key] = style[key]
  })
}

/**
 * Wraps an element
 * @param el | DOM element
 */
function wrap(el, wrapperTag) {
  const wrapper = document.createElement( wrapperTag ? wrapperTag : 'div')  
  el.parentNode.insertBefore(wrapper, el)
  wrapper.appendChild(el)
  return wrapper
}


function slideUp(target, duration = 500){
  target.style.transitionProperty = 'height, margin, padding';
  target.style.transitionDuration = duration + 'ms';
  target.style.boxSizing = 'border-box';
  target.style.height = target.offsetHeight + 'px';
  target.offsetHeight;
  target.style.overflow = 'hidden';
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  window.setTimeout( () => {
    target.style.display = 'none';
    target.style.removeProperty('height');
    target.style.removeProperty('padding-top');
    target.style.removeProperty('padding-bottom');
    target.style.removeProperty('margin-top');
    target.style.removeProperty('margin-bottom');
    target.style.removeProperty('overflow');
    target.style.removeProperty('transition-duration');
    target.style.removeProperty('transition-property');
  }, duration);
}

function slideDown(target, duration = 500){
  target.style.removeProperty('display');
  let display = window.getComputedStyle(target).display;
  if (display === 'none') display = 'block';
  target.style.display = display;
  let height = target.offsetHeight;
  target.style.overflow = 'hidden';
  target.style.height = 0;
  target.style.paddingTop = 0;
  target.style.paddingBottom = 0;
  target.style.marginTop = 0;
  target.style.marginBottom = 0;
  target.offsetHeight;
  target.style.boxSizing = 'border-box';
  target.style.transitionProperty = "height, margin, padding";
  target.style.transitionDuration = duration + 'ms';
  target.style.height = height + 'px';
  target.style.removeProperty('padding-top');
  target.style.removeProperty('padding-bottom');
  target.style.removeProperty('margin-top');
  target.style.removeProperty('margin-bottom');
  window.setTimeout( () => {
    target.style.removeProperty('height');
    target.style.removeProperty('overflow');
    target.style.removeProperty('transition-duration');
    target.style.removeProperty('transition-property');
  }, duration);
}

/**
 * Generates an ID
 */
function generateID(){
  return `_${Math.random().toString(36).substr(2, 9)}`
}

//checks if a variable is /pretty much/ empty
function empty(data) {
  if(typeof(data) == 'number' || typeof(data) == 'boolean') { 
    return false 
  }
  
  if(typeof(data) == 'undefined' || data === null) {
    return true 
  }
  
  if(typeof(data.length) != 'undefined'){
    return data.length == 0
  }

  var count = 0
  
  for(var i in data) {
    if(data.hasOwnProperty(i)){
      count ++
    }
  }

  return count == 0
}

//sets a cookie
function setCookie(cname, cvalue, exdays) {
  var d = new Date()
  d.setTime(d.getTime() + (exdays*24*60*60*1000))
  var expires = "expires="+ d.toUTCString()
  document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/"
}

//gets a cookie
function getCookie(cname) {
  var name = cname + "="
  var decodedCookie = decodeURIComponent(document.cookie)
  var ca = decodedCookie.split(';')
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i]
    while (c.charAt(0) == ' ') {
      c = c.substring(1)
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length)
    }
  }
  return ""
}

//gets a GET string from the URL
function getURLParam(key){
  var params = {}
  var parser = document.createElement('a')
  parser.href = window.location.href
  var query = parser.search.substring(1)
  var vars = query.split('&')
  for (var i = 0; i < vars.length; i++) {
    var pair = vars[i].split('=')
    params[pair[0]] = decodeURIComponent(pair[1])
  }

  return typeof params[key] !== undefined ? params[key] : null
}

function isVisible(el){
  return el.offsetParent !== null 
  //not doing this anymore??
  var rect = el.getBoundingClientRect();
  var viewHeight = Math.max(document.documentElement.clientHeight, window.innerHeight);
  return !(rect.bottom < 0 || rect.top - viewHeight >= 0);
}

/**
 * Get all parts of a date
 * Example usage: 
 * const parts = getDateParts(new Date())
 * console.log(parts)
 **/
function getDateParts(d){
  function isValidDate(date) {
    return date && Object.prototype.toString.call(date) === "[object Date]" && !isNaN(date);
  }

  function zerofill(number, length) {
    // Setup
    var result = number.toString();
    var pad = length - result.length;

    while(pad > 0) {
      result = '0' + result;
      pad--;
    }

    return result;
  }

  if(!isValidDate(d)){
    console.error("Invalid date passed to getDateParts")
    return {}
  }

  let months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];
  let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
  let day = d.getDay();
  let hour = d.getHours();
  let minute = d.getMinutes();
  if (minute < 10) {
      minute = "0" + minute;
  }
  let second = zerofill(d.getSeconds(), 2)
  let ampm = "am";
  if( hour >= 12 ) {
      hour -= 12;
      if(hour == 0){
        hour = 12
      }
      ampm = "pm";
  }
  let date = d.getDate();
  let month = d.getMonth();
  let year = d.getFullYear();

  let dayName = days[day]
  let monthName = months[month]

  return {
    day, hour, minute, second, ampm, date, month, year, dayName, monthName
  }
}

export {
  setStyle,
  wrap,
  generateID,
  empty,
  slideDown,
  slideUp,
  setCookie,
  getCookie,
  getURLParam,
  isVisible,
  getDateParts
}