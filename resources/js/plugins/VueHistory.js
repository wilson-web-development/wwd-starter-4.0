/**
 * This Vue plugin sets data from the URL and pushes history to it allowing a user to refresh and hit the back button but keeps the current state of the application
 * Example Usage:
 *
 * //initial setup in a Vue component:
 * import VueHistory from '../../resources/js/plugins/VueHistory'
 * import Vue from '../../resources/node_modules/vue/dist/vue.esm.browser.min.js'
 *
 * Vue.use(VueHistory, {
 *   url_params: [ 'pg', 'search' ],
 *   data_keys:  [ 'page', 'search' ]
 * })
 *
 * //you can then use these methods like so: 
 * mounted(){
 *  this.$setFromUrl()
 * },
 * methods: {
 *   someMethod(){
 *     this.$pushToUrl()
 *   }
 * }
 */
const VueHistory = {}

import { getURLParam } from './helperFunctions'

VueHistory.install = function (Vue, globalOptions) {
  Vue.prototype.$setFromUrl = function () {
    if(globalOptions?.url_params){
      for(let i = 0; i < globalOptions.url_params.length; i++){
        try {
          const url_value = getURLParam(globalOptions.url_params[i])
          if(url_value){
            const val = decodeURIComponent(url_value)
            if(val == parseFloat(val)){
              this[globalOptions.data_keys[i]] = parseFloat(val)
            } else if(val == parseInt(val)){
              this[globalOptions.data_keys[i]] = parseInt(val)
            } else {
              this[globalOptions.data_keys[i]] = val
            }
            
          }
        } catch (e) {
          //nada
        }
      }
    }
  }

  Vue.prototype.$pushToUrl = function () {
    if(globalOptions?.url_params){
      const url = new URL(`${window.location.origin}${window.location.pathname}`)

      for(let i = 0; i < globalOptions.url_params.length; i++){
        try {
          const data_value = this[globalOptions.data_keys[i]]
          if(data_value){
            url.searchParams.append(globalOptions.url_params[i], encodeURIComponent(data_value))
          }
        } catch (e) {
          //nada
        }
      }

      window.history.pushState({}, document.title, url.href)
    }
  }
}

export default VueHistory