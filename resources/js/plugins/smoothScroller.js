function handleSmoothScrollClicks(accountForHeader){
  jQuery(document).on('click', 'a', e => {
    if(typeof e.target.hash == 'string' && e.target.hash.length){
      const current = new URL(window.location.href)
      const target = new URL(e.target.href)
      const isSamePage = current.pathname == target.pathname
      const targetElement = document.querySelector(target.hash)

      if(isSamePage){
        if(targetElement){
          let top = jQuery(target.hash).offset().top
          e.preventDefault()

          if(accountForHeader){
            top -= document.querySelector('.main-header').offsetHeight
          }

          jQuery('html, body').animate({ scrollTop: top }, 650)
        } else {
          //target element should be on this page but is not found... just ignore the click
        }
      } else {
        //this is supposed to go to a different page... just let the browser handle it
      }
    }
  })
}

function handleHashUrlLoad(accountForHeader){
  if(accountForHeader){
    window.addEventListener('DOMContentLoaded', e => {
      setTimeout(() => {
        let top = window.scrollY - document.querySelector('.main-header').offsetHeight
        jQuery('html, body').animate({ scrollTop: top }, 0)
      }, 100)
    })
  }
}

function smoothScroller(accountForHeader){
  ~(() => {
    //handle smooth scroll clicks
    handleSmoothScrollClicks(accountForHeader)
    //handle hash loading (move the scroll if we are accounting for fixed header)
    handleHashUrlLoad(accountForHeader)
  })()
}

export default smoothScroller