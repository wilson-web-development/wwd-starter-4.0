/**
 * This is a helpful scrips for lazy loading google maps because they charge you for everytime its loaded
 * Generally this will make it load less
 *
 * Options:
 * - key - google maps API key
 * - onload - callback to run after the script loads
 * - scrollTarget - if you want to listen for when an element gets near the view port, pass that element here
 *
 * Usage (easy):
 * new gmapLoader({ 
 *   key : 'abc123',
 *   scrollTarget :  document.querySelector('.map-container'),
 *   onload : () => {
 *     console.log('gmaps loaded, initialize the map with `new google.maps.Map(options)` or whatever')
 *   } 
 * })
 *
 * Usage (advanced?):
 * const loader = new gmapLoader({ key : 'abc123' })
 * document.querySelector('.load-maps').addEventListener('click', e => {
 *   loader.load(() => {
 *     console.log('gmaps loaded, initialize the map with `new google.maps.Map(options)` or whatever')
 *   })
 * })
 */
class gmapLoader {
  constructor(options){
    this.key = options.key ? options.key : null
    this.onload = options.onload ? options.onload : () => {}
    this.scrollTarget = options.scrollTarget ? options.scrollTarget : null 
    this.isLoaded = false

    if(this.scrollTarget){
      this._startScrollListener()
    }
  }

  _startScrollListener(){
    window.addEventListener('scroll', e => {
      if(window.pageYOffset > this.scrollTarget.offsetTop - window.innerHeight / 2){
        this.load(this.onload)
      }
    })
  }

  load(callback){
    //only load once
    if(this.isLoaded){
      return
    }
    this.isLoaded = true

    //add the script to the head, then run a callback
    const gmaps = document.createElement('script')
    gmaps.src = `https://maps.googleapis.com/maps/api/js?key=${this.key}`
    gmaps.onload = () => {
      callback()
    }
    const head = document.querySelector('head')
    head.insertBefore(gmaps, head.lastChild)
  }
}

export default gmapLoader