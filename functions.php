<?php

//require extra theme files
foreach (glob(__DIR__ . '/includes/*.php') as $auto_load) {
  require_once $auto_load;
}

//global theme options
theme_configuration([
  'remove_comments'                       => true, //should we remove WP comments?
  'acf_whitelist_ids'                     => [ 1 ], //which users should be able to edit ACF settings?
  'acf_options_pages'                     => [ 'Theme Options' ], //create ACF option pages
  'menus'                                 => [ 'Main Menu', 'Site Map' ], //create WP menus
  'enable_select2_for_gform_multiselects' => true, //enable select2 for gform multiselects
  'convert_images_to_webp'                => true, //convert any jpg/pngs to webp on upload
  'webp_quality'                          => 80, //webp quality when converting
  'admin_background_color'                => '#0065ad', //used to control various buttons/colors in the admin 
  'admin_text_color'                      => '#ffffff', //used to control various buttons/colors in the admin
  'wysiwyg_colors'                        => [  //used for WYSIWYG and ACF color options
    'White' => '#ffffff', 
    'Black' => '#000000',
    'Blue'  => '#0065ad'
  ],
  'allow_mime_uploads'          => [ 
    [ 'svg' => 'image/svg' ] 
  ],
  // 'post_types'                  => [
  //   [
  //     'plural'       => 'Example Custom',
  //     'singular'     => 'Example Custom',
  //     'slug'         => 'example_custom',
  //     'rewrite_slug' => 'example-custom-post-type',
  //     'supports'     => [ 'title', 'editor', 'thumbnail', 'excerpt' ],
  //     'icon'         => 'dashicons-arrow-right',
  //     'taxonomies'   => [],
  //     'public'       => true
  //   ]
  // ],
  // 'taxonomies'                  => [
  //   [
  //     'slug'         => 'example_custom_categories',
  //     'singular'     => 'Example Custom Category',
  //     'plural'       => 'Example Custom Categories',
  //     'post_types'   => [ 'example_custom' ]
  //     'rewrite_slug' => 'example-custom-categories'
  //   ]
  // ]
]);