<?php

/**
 * These are helpful functions to use within the theme
 * They will be used all the time, so get to know them all
 */

/**
 * Returns an string of html5 picture using next gen images when available
 * @param $name - name of image located in public/images/ WITHOUT the extention
 * @param $args - alt/title/etc to add to the image
 */
function theme_image($name, $args = []){
  if(!function_exists('get_image_source')){
    function get_image_source($name, $ext){
      return file_exists(get_template_directory() . '/public/images/' . $name . '.' . $ext) ? 
                get_template_directory_uri() . '/public/images/' . $name . '.' . $ext :
                false;
    }
  }

  $found_images = array_filter([
    'webp' => get_image_source($name, 'webp'),
    'jpeg' => get_image_source($name, 'jpeg'),
    'jpg'  => get_image_source($name, 'jpg'),
    'png'  => get_image_source($name, 'png'),
    'gif'  => get_image_source($name, 'gif'),
    'svg'  => get_image_source($name, 'svg'),
  ]);

  //first check if we should be lazy or not
  $should_be_lazy = true;
  if(gettype($args) == 'string'){
    if(strpos($args, 'not-lazy') !== false){
      $should_be_lazy = false;
    }
  } else {
    foreach($args as $key => $value){
      if($key == 'class' && strpos($value, 'not-lazy') !== false){
        $should_be_lazy = false;
      }
    }
  }

  $extra_tags = '';
  if(gettype($args) == 'string'){
    $args .= $should_be_lazy ? ' lazy' : '';
    $extra_tags .= " class='$args'";
  } else {
    foreach($args as $key => $value){
      $value .= $key == 'class' && $should_be_lazy ? ' lazy' : '';
      $extra_tags .= " $key='$value'";
    }
  }

  if(empty($extra_tags)){
    $extra_tags .= " class='lazy'";
  }
  
  $html = '';

  if(!empty($found_images['webp'])){
    if($should_be_lazy){
      $html = "<img data-src='{$found_images['webp']}' $extra_tags>";
    } else {
      $html = "<img src='{$found_images['webp']}' $extra_tags>";
    }
  } else {
    $sources = array_filter($found_images);
    $src = array_shift($sources);
    if(!empty($src)){
      if($should_be_lazy){
        $html = "<img data-src='{$src}' $extra_tags>";
      } else {
        $html = "<img src='{$src}' $extra_tags>";
      }
    }
  }

  return $html;
}

/**
 * This is just a for wp_get_attachment_image that makes it html5 and next gen images
 */
function wp_image($image_id, $size = 'large', $args = []){
  if(gettype($args) == 'string'){
    $args = [ 'class' => $args ];
  }
  return wp_get_attachment_image($image_id, $size, '', $args);
}

/**
 * Converts a youtube or vimeo video to work with iframes
 * Vimeo example: https://player.vimeo.com/video/ID
 * Youtube example: https://www.youtube.com/embed/ID
 * @param $url 
 * @param $options | these are things like mute, background, loop, etc.
 */
function convert_video_link_for_iframe($url, $options = []){
  if(strpos($url, 'vimeo') !== false ){
    $url = 'https://player.vimeo.com/video/' . preg_replace('/[^0-9]/', '', $url);
  }

  if(strpos($url, 'youtube.com') !== false || strpos($url, 'youtu.be') !== false){
    preg_match("#(?<=v=)[a-zA-Z0-9-]+(?=&)|(?<=v\/)[^&\n]+(?=\?)|(?<=v=)[^&\n]+|(?<=youtu.be/)[^&\n]+#", $url, $id);
    $id = !empty($id[0]) ? $id[0] : null;
    $url = 'https://www.youtube.com/embed/' . $id;
  }

  if(!empty($options)){
    $url .= '?';
    foreach($options as $key => $value){
      if($key == 'loop' && $value == 1 & !empty($id)){
        $url .= 'playlist=' . $id . '&';
      }

      $url .= $key . '=' . $value . (count($options) - 1 !== $key ? '&' : '');
    }
  }

  return trim($url);
}

/**
 * This returns a single category, it prioritizes yoast's primary categories, if one isn't set, it just grabs the first one.
 * @param $id - post ID
 * @param $term - term_slug (if using this for a custom taxonomy)
 */
function get_primary_category($id, $term = 'category'){
  $primary_cat = '';

  $category = get_the_terms($id, $term);
  // If post has a category assigned.
  if ($category){
    if ( class_exists('WPSEO_Primary_Term') ) {
      // Show the post's 'Primary' category, if this Yoast feature is available, & one is set
      $wpseo_primary_term = new WPSEO_Primary_Term( $term, $id );
      $wpseo_primary_term = $wpseo_primary_term->get_primary_term();
      $term = get_term( $wpseo_primary_term );

      if (is_wp_error($term)) {
        // Default to first category (not Yoast) if an error is returned
        $primary_cat = $category[0];
      } else {
        // Yoast Primary category
        $primary_cat = $term;
      }
    } else {
      // Default, display the first category in WP's list of assigned categories
      $primary_cat = $category[0];
    }
  }

  return $primary_cat;
}

// converts a phone number to a tel: link
function phone_to_link($num, $class = "link"){
  $number_only = preg_replace('/[^0-9]/', '', $num);
  return "<a href='tel:{$number_only}' class='{$class}' >{$num}</a>";
}

// converts email address to a mailto: link
function email_to_link($email, $class = 'link'){
  $emailHtml = esc_html( antispambot( $email ) );
  return "<a class='$class' href='mailto:$emailHtml'>$emailHtml</a>";
}

/**
  * converts ACF link array to pretty 
  * @param ACF link array 
*/
function acf_link($link, $classes='') {
  if(empty($link['target'])){
    $target = '_self';
  }
  else{
    $target = $link['target'];
  }
  if(empty($link['title'])){
    $title = "Learn More";
  }
  else{
    $title = $link['title'];
  }
  return '<a class="'.$classes.'" target="'.$target.'" href="'.$link['url'].'">'.$title.'</a>';
}

/**
 * get an arbritrary numbers of words from some text
 * @param $text - string to get some words from
 * @param $count - int | # of words you want
 * @return String of text
 */
function get_words($text, $count){
  return implode(' ', array_slice(explode(' ', $text), 0, $count - 1));
}

/**
 * Wraps a PHP variable to pass to vue via a prop
 * @param $var
 * @return escaped JSON var
 */
function vue_data($var){
  return htmlspecialchars(json_encode($var), ENT_QUOTES, 'UTF-8', true);
}


/**
  * returns a WP page obj by template
  * @param $template (string) - which template are we looking for
  * @param $id_only (bool)    - should we return the WP obj or the ID?
  * @param $return_all (bool) - should we return all or just the first?
  */  
function get_page_by_template($template, $id_only = false, $return_all = false){
  $pages = get_posts([
    'post_type' => 'page',
    'meta_key' => '_wp_page_template',
    'meta_value' => $template,
    'numberposts' => -1
  ]);
  
  if(empty($pages)){
    return false;
  } else {
    if(!$id_only && $return_all){
      return $pages;
    } else if($id_only && $return_all){
      $page_ids = [];
      foreach($pages as $page){
        $page_ids[] = $page->ID;
      }
      return $page_ids;
    } else if($id_only && !$return_all){
      return $pages[0]->ID;
    } else {
      return $pages[0];
    }
  }
}

/**
 * Checks if we are on devbucket... thats it!
 */
function is_devbucket(){
  return !empty($_SERVER['SERVER_NAME']) && strpos($_SERVER['SERVER_NAME'], 'devbucket.net') !== false;
}

/**
 * Returns an array of ints with some number of ints on either side of the current
 * @param $current_page - int - the current page 
 * @param $total_pages - int - the total number of pages
 * @param $limit - int -  how many pages should show on either side of the current page
 * @return array - where null represents some number of pages being removed. example: [1,null,6,7,8,9,10,null,32]
 */
function get_limited_pages($current_page, $total_pages, $limit = 2){
  $page_numbers = [];

  for($i = 1; $i <= $total_pages; $i++){
    if($i == 1 || $i == $total_pages){
      //always add in the first and last pages
      $page_numbers[] = $i;
    } else if($i > $current_page + $limit || $i < $current_page - $limit){
      //dont add a page if is isn't within the limit
      $page_numbers[] = null;
    } else {
      //this page is within the limits
      $page_numbers[] = $i;
    }
  }

  //remote any consecutive null values in the array
  $last = null;
  $page_numbers = array_filter($page_numbers, function($page){
    global $last;
    $should_remove = $last != $page;
    $last = $page;
    return $should_remove;
  });

  return $page_numbers;
}

/**
 * Adds arbitrary metafields to admin post listing tables
 * @param $slug - string - the metafield key
 * @param $title - string - the metafield key pretty name
 * @param $allow_filtering - boolean - if true the user can click on this to filter posts by it
 * @param $post_type - string - which post type listing should this be added to
 */
function add_metafield_to_post_listing($slug, $title, $allow_filtering = true, $post_type = 'post'){
  if(!empty($_GET['post_type']) && $_GET['post_type'] !== $post_type){
    return;
  }

  //add the column to the table head
  add_filter('manage_posts_columns',  function ($columns) use ($slug, $title) {
    return array_merge ( $columns, [ $slug => $title ]);
  });

  //display each post's column
  add_action('manage_posts_custom_column', function($column, $post_id) use ($slug, $allow_filtering) {
    if($column == $slug){
      $meta = get_post_meta($post_id, $slug, true);
      if($allow_filtering){
        echo '<a href="'.site_url().'/wp-admin/edit.php?'.$slug.'='.$meta.'">'.$meta.'</a>';    
      } else {
        echo $meta;    
      }
    }
  }, 10, 2);

  //add the filter to the query
  if($allow_filtering){
    add_action('pre_get_posts', function($query) use ($slug) {
      if(is_admin() && !empty($_GET[$slug])){
        $query->set('meta_key', $slug);
        $query->set('meta_value', $_GET[$slug]);
      }
      return $query;
    });
  }
}

/**
 * Adda taxonomy to admin post listing
 * @param $tax - string - tax slug
 * @param $title - string - name for column
 * @param $post_type - string - WP post type to display column on
 */
function add_taxonomy_to_post_listing($tax, $title, $post_type){
  if(!empty($_GET['post_type']) && $_GET['post_type'] !== $post_type){
    return;
  }

  //add the column to the table head
  add_filter('manage_posts_columns',  function ($columns) use ($tax, $title) {
    return array_merge ( $columns, [ $tax => $title ]);
  });

  add_action('manage_posts_custom_column', function($column, $post_id) use ($tax) {
    if($column == $tax){
      $tax = get_the_terms($post_id, $tax);

      if(empty($tax)){
        echo 'None';
      } else {
        echo implode(', ', wp_list_pluck($tax, 'name'));
      }
    }
  }, 10, 2);
}

/**
 * Makes a request and returns the body
 * @param $url - string - URL to go to
 * @param $args - array - See WP documentation noted below
 * @return uknown
 */
function http_request($url, $args = []){
  try {
    if(!empty($args['body'])){
      //https://developer.wordpress.org/reference/functions/wp_remote_post/
      $response = wp_remote_post($url, $args);
    } else {
      //https://developer.wordpress.org/reference/functions/wp_remote_get/
      $response = wp_remote_get($url, $args);
    }
    if(is_wp_error($response)){
      throw new Exception($response->get_error_message());
    } else {
      $body = wp_remote_retrieve_body($response);
      return $body;
    }
  } catch(Exception $e){
    die($e->getMessage());
  }
}

/**
 * Creates a WP cron event... but easy!
 * @param $callback - function to run
 * @param $recurrenace - how often it should run? See wp_get_schedules() for accepted values.
 */
function izi_cron($callback, $recurrance, $timestamp = ''){
  $action_name = $callback . '_cron_action';

  if(empty($timestamp)){
    $timestamp = time();
  }

  if(!wp_next_scheduled($action_name)){
    wp_schedule_event($timestamp, $recurrance, $action_name);
  }
  add_action($action_name, $callback);
}

/**
 * Loads a CSV into a key/value array for easy processing
 * @param $path | string | file path to CSV
 * @param $parse_column_names | boolean | whether we should return a key/value array or just the values
 */
function izi_load_csv($path, $parse_column_names = true){
  $data = [];
  $file = fopen($path,"r");
  $column_names_processed = false;
  $column_names = [];

  while(!feof($file)){
    if($parse_column_names){
      //if we are parsing column names and are on the first row, we need to store the names to use later
      if($column_names_processed == false){
        foreach(fgetcsv($file) as $column_name){
          $column_names[] = str_replace('-', '_', sanitize_title($column_name));
        }
        $column_names_processed = true;
      } else {
        //get the row and set it to its appropriate column name
        $row = fgetcsv($file);
        $clean_row = [];
        if(!empty($row)){
          foreach($row as $key => $value){
            $clean_row[$column_names[$key]] = $value;
          }
        }
        $data[] = $clean_row;
      }
    } else {
      //just send back each row
      $data[] = fgetcsv($file);
    }
  }
  fclose($file);
  return $data;
}

/**
 * Uploads a remote image to the media library and attaches it to a post
 * @param $file_url | string | remote file URL 
 * @param $parent_post_id | int | WP post ID
 */
function upload_remote_image_to_media_library($file_url, $parent_post_id){
  $response = http_request($file_url);
  $filename = basename($file_url);
  $upload_file = wp_upload_bits($filename, null, $response);  
  if(!$upload_file['error']) {
    $wp_filetype = wp_check_filetype($filename, null );
    $attachment = [
      'post_mime_type' => $wp_filetype['type'],
      'post_parent' => $parent_post_id,
      'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
      'post_content' => '',
      'post_status' => 'inherit'
    ];

    $attachment_id = wp_insert_attachment( $attachment, $upload_file['file'], $parent_post_id );
    if(!is_wp_error($attachment_id)) {
      require_once(ABSPATH . "wp-admin" . '/includes/image.php');
      $attachment_data = wp_generate_attachment_metadata( $attachment_id, $upload_file['file'] );
      wp_update_attachment_metadata( $attachment_id,  $attachment_data );
      return $attachment_id;
    } 
  }
}

/**
 * @param $file_data | binary file data
 * @param $post_id | which post this file should be asociated with
 * @return $attachment_id | the post_id of the new file
 */
function handle_file_upload($file_data, $post_id, $title = ''){
  if (!function_exists('wp_handle_upload')) {
    require_once(ABSPATH . 'wp-admin/includes/file.php');
  }
  if(!function_exists('wp_generate_attachment_metadata')){
    require_once( ABSPATH . 'wp-admin/includes/image.php' );
  }

  $uploaded_file = wp_handle_upload($file_data, [ 'test_form' => false ]);
  $file_path = $uploaded_file['file'];

  // Get the path to the upload directory.
  $wp_upload_dir = wp_upload_dir();
   
  // Prepare an array of post data for the attachment.
  $attachment = [
    // 'guid'           => $wp_upload_dir['url'] . '/' . sanitize_title_with_dashes(basename( $file_path )) . '-' . uniqid(), 
    'post_mime_type' => $uploaded_file['type'],
    'post_title'     => $title ?: sanitize_title_with_dashes(basename( $file_path ) ),
    'post_content'   => '',
    'post_status'    => 'inherit'
  ];
 
  // Insert the attachment.
  $attach_id = wp_insert_attachment( $attachment, $file_path, $post_id );
  // Generate the metadata for the attachment, and update the database record.
  $attach_data = wp_generate_attachment_metadata( $attach_id, $file_path );
  wp_update_attachment_metadata( $attach_id, $attach_data );
  return $attach_id;
}

/**
 * If content exists, display an element for it
 * Rememer to put an @ at the beginning of this function to surpress any errors for empty variables
 * @param $tag | string | html tag h1, h2, p, etc
 * @param $content | string | content to display if !empty
 * @param $attr | array or string | key => value pairs to add to the tag, if string, class is assumed
 * @example: <?= @el_if('h2', $title, 'contact-callouts--title text-center' ) ?> or <?= @el_if('h2', $title, ['aria-label' => 'this-thing', 'class' => 'contact-callouts--title text-center' ]) ?>
 * @return string html
 */
function el_if($tag, $content = '', $attr = []){
  $html = '';
  $extra_tags = '';  

  if(!empty($content)){
    if(gettype($attr) == 'string'){
      $extra_tags .= "class='$attr'";
    } else {
      foreach($attr as $key => $value){
        $extra_tags .= " $key='$value'";
      }
    }
    $html .= "<$tag $extra_tags>$content</$tag>";
  }

  return $html;
}

function create_module_attributes($class, $classes, $padding, $background_color, $text_color, $id=''){
  $new_classes = [ 'module' ];
  $styles = [];

  $new_classes[] = $class; 
  $new_classes[] = $classes; 
  $new_id[] = $classes; 
  
  $padding_sizes = get_field('module_sizes', 'options');
  $padding_sizes['none'] = 0;

  if(!empty($padding['desktop_top'])){
    $styles[] = "--desktop-padding-top:" . $padding_sizes[$padding['desktop_top']] . "px;";
  }
  if(!empty($padding['desktop_bottom'])){
    $styles[] = "--desktop-padding-bottom:" . $padding_sizes[$padding['desktop_bottom']] . "px;";
  }
  if(!empty($padding['mobile_top'])){
    $styles[] = "--mobile-padding-top:" . $padding_sizes[$padding['mobile_top']] . "px;";
  }
  if(!empty($padding['mobile_bottom'])){
    $styles[] = "--mobile-padding-bottom:" . $padding_sizes[$padding['mobile_bottom']] . "px;";
  }

  if(!empty($background_color)){
    $styles[] = "background-color:$background_color;";
  }

  if(!empty($text_color)){
    $styles[] = "color:$text_color;";
  }

  $attribute_string = 'class="'.implode(' ', $new_classes).'" style="'.implode(' ', $styles).'"' . '';

  if(!empty($id)){
    $attribute_string .= " id= '$id' ";
  }

  return $attribute_string;
  
}

/**
 * Find the address/latitude/longitude based on a string
 * @param $string | expected input: "45.23423,-83.123123" or "123 fake st, royal oak, MI USA"
 * 
 * Example usage: 
 * AddressParser::init("45.23423,-83.123123", "SomeGoogleAPIKey123"); 
 * //or AddressParser::init("123 fake st, royal oak, MI USA", "SomeGoogleAPIKey123");
 * echo AddressParser::$street_address;
 * echo AddressParser::$city;
 * echo AddressParser::$state;
 * echo AddressParser::$latitude;
 * echo AddressParser::$longitude;
 */  
class AddressParser {
  public static $google_api_key;
  public static $string_address;
  public static $address;
  public static $street_address;
  public static $city;
  public static $state;
  public static $latitude;
  public static $longitude;

  /**
   * Find the address/latitude/longitude based on a string
   * @param $string | expected input: "45.23423,-83.123123" or "123 fake st, royal oak, MI USA"
   */  
  public static function init($string, $google_api_key){
    self::$string_address = $string;
    self::$google_api_key = $google_api_key;

    if(self::string_is_lat_long()){
      self::$latitude = self::extract_lat_long_from_string('lat');
      self::$longitude = self::extract_lat_long_from_string('long');
      self::$address = self::get_address_by_lat_long();
    } else {
      self::$address = self::$string_address;
      self::set_lat_long_by_address();
    }
  }

  /**
   * Checks if a given string is probably a lat/long
   */
  public static function string_is_lat_long(){
    $exploded = explode(',', self::$string_address);
    return count($exploded) == 2 && self::is_string_float($exploded[0]) && self::is_string_float($exploded[1]);
  }

  /**
   * Checks if a given string is a float (it is probably a lat/long then!)
   * @param $string | some string
   */
  public static function is_string_float($string){
    $float_value = (float) $string;
    return strval($float_value) == $string;
  }

  /**
   * Returns either the lat or long from a string that is (probably) a lat long
   */
  public static function extract_lat_long_from_string($type){
    $exploded = explode(',', self::$string_address);
    return $type == 'lat' ? $exploded[0] : $exploded[1];
  }

  /**
   * Returns an address based on the lat/long
   */
  public static function get_address_by_lat_long(){
    $url = "https://maps.googleapis.com/maps/api/geocode/json?latlng=".self::$latitude.",".self::$longitude."&key=".self::$google_api_key;
    $response = wp_remote_get($url);
    $body = json_decode(wp_remote_retrieve_body($response));
    return self::get_address_from_google_response($body);
  }

  public static function set_lat_long_by_address(){
    $url = "https://maps.googleapis.com/maps/api/geocode/json?address=".self::$string_address."&key=".self::$google_api_key;
    $response = wp_remote_get($url);
    $body = json_decode(wp_remote_retrieve_body($response));
    if(!empty($body->results[0]->geometry->location->lat)){
      self::$latitude = $body->results[0]->geometry->location->lat;
      self::$longitude = $body->results[0]->geometry->location->lng;
    }
  }

  /**
   * Builds out a string address from a google geocode API call
   */
  public static function get_address_from_google_response($body){
    $address = "";

    if(!empty($body->results[0]->address_components)){
      $components = $body->results[0]->address_components;
      $address = self::get_address_part("street_number", $components) . " " . 
                 self::get_address_part("route", $components) . ", " . 
                 self::get_address_part("locality", $components) . ", " . 
                 self::get_address_part("administrative_area_level_1", $components) . " " . 
                 self::get_address_part("postal_code", $components);

      self::$street_address = self::get_address_part("street_number", $components) . " " . self::get_address_part("route", $components);
      self::$city = self::get_address_part("locality", $components);
      self::$state = self::get_address_part("administrative_area_level_1", $components);
    }

    return $address;
  }

  /**
   * Gets an address part
   */
  public static function get_address_part($needle, $parts){
    $found = '';

    foreach($parts as $part){
      if(in_array($needle, $part->types)){
        $found = $part->short_name;
        break;
      }
    }

    return $found;
  }
}