<?php

/**
 * Any AJAX functions can go here
 */

/**
 * Find posts based on search and category
 */
function fetch_posts(){
  $per_page = get_option('posts_per_page');

  $args = [
    'post_type'      => 'post',
    'post_status'    => 'publish',
    'posts_per_page' => $per_page,
    'offset'         => ($_REQUEST['page'] - 1) * $per_page
  ];

  if(!empty($_REQUEST['search'])){
    $args['s'] = $_REQUEST['search'];
  }

  if(!empty($_REQUEST['category'])){
    $args['tax_query'] = [[
      'taxonomy' => 'category',
      'terms'    => $_REQUEST['category'],
      'field'    => 'term_id',
    ]];
  }

  if(!empty($_REQUEST['tag'])){
    $args['tag_id'] = $_REQUEST['tag'];
  }

  $query = new WP_Query($args);

  //add any meta we need
  array_map(function($post){
    $post->permalink = get_the_permalink($post);
    $post->featured_image_src = get_the_post_thumbnail_url($post);
    $category = get_primary_category($post);
    $post->category_name = $category->name;
    $post->formatted_date = get_the_date('F j, Y', $post);
  }, $query->posts);

  wp_send_json_success([ 
    'posts'      => $query->posts, 
    'maxPages'   => $query->max_num_pages,
    'postsFound' => $query->found_posts
  ]);
}
add_action( 'wp_ajax_fetch_posts', 'fetch_posts');
add_action( 'wp_ajax_nopriv_fetch_posts', 'fetch_posts');