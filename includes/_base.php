<?php

/**
 * These are functions that power theme_configuration and blocks
 * There are also some functions that need to be run all the time
 * You probably don't need to be in here
 */
 $theme_configuration;

/**
 * Pretty debugging
 * @param $var - one or many variables to dump
 */
function pr($args){
  echo '<pre style="background:black;color:white;padding:1em;font-family:Courier;white-space:pre-wrap;">';
    echo '<div style="color:lime;">Debug:</div>';
    foreach(func_get_args() as $arg){
      echo '<div>' . print_r($arg, true) . '</div>';
    }
  echo '</pre>';
}

function dd($args){
  pr($args);
  die;
}


if(!is_admin() && !function_exists('get_field')){
  function get_field(){
    return 'Please install ACF to continue!';
  }
}

/**
 * Include block functionality 
 * @param $name 
 * @param $args
 */
function block($name, $args = [], $echo = true){


  if(is_acf_block_preview()){
    echo generate_acf_block_preview($name);
    return;
  }

  $html = '';

  //set each key as a variable
  foreach($args as $key => $value){
    $$key = $value;
  }

  if($echo == false){
    ob_start();
  }
  
  //include the template to have access to variables
  $template_location = 'blocks/' . $name . '/' . $name . '.php';
  if(locate_template($template_location)){
    require(get_template_directory() . '/' . $template_location);
  } else {
    pr($name . ' block not found.');
  }

  if($echo == false){
    $html = ob_get_contents();
    ob_end_clean();
  }

  //unset any variables we've created
  foreach($args as $key => $value){
    unset($$key);
  }

  return $html;
}

/**
 * Disable certain blocks from ever being used...
 */
function izi_disable_blocks($blocks){
  add_action('admin_footer', function() use ($blocks){
    echo '<script>if(typeof wp != "undefined"){wp.domReady(() => {';
      foreach($blocks as $block){
        echo "wp.blocks.unregisterBlockType('$block');";
      }
    echo '})}</script>';
  });
}

/**
 * Handle izi ACF gutenberg integrations
 */
function is_acf_block_preview(){
  return is_admin() && !empty($_POST['query']['preview']);
}

function izi_acf_gute_blocks($blocks){
  usort($blocks, function($a, $b) {
    return $a['name'] > $b['name'];
  });

  foreach($blocks as $block){
    $slug = sanitize_title_with_dashes($block['name']);
    $keywords = array_merge([ 'custom' ], explode('-', $slug));
    $clean_options = [
      'name'            => $slug,
      'title'           => $block['name'],
      'description'     => $block['description'],
      'category'        => !empty($block['category']) ? $block['category'] : sanitize_title_with_dashes(get_bloginfo('name')),
      'icon'            => [
        // 'background' => '#000', 
        // 'foreground' => '#FFF', 
        'src' => !empty($block['icon']) ? $block['icon'] : 'editor-kitchensink' //dashicon (without "dashicon-") or svg content
      ],
      'keywords'        => !empty($block['keywords']) ? $block['keywords'] : $keywords,
      'post_types'      => !empty($block['post_types']) ? $block['post_types'] : [ ], //restrict if needed
      'mode'            => 'edit',
      'example'         => [ 'attributes' => [ 'mode' => 'preview', 'data' => [] ]],
      'supports'        => !empty($_POST['query']['preview']) ? [] : [ 'mode' => false ],
      'render_callback' => function() use ($slug) {
        block($slug, get_fields());
      }
    ];

    acf_register_block_type($clean_options);
  }
}

add_action( 'block_categories_all', function($categories){
  return array_merge([
    [
      'slug'  => sanitize_title_with_dashes(get_bloginfo('name')),
      'title' => get_bloginfo('name') . " - Custom"
    ]
  ], $categories);
}, 10, 2 );

function get_block_preview_image_src($name){
  if(file_exists(get_template_directory() . "/blocks/$name/$name.png")){
    return get_template_directory_uri() . "/blocks/$name/$name.png";
  }
}

function generate_acf_block_preview($name){
  if(!empty($_POST['block'])){
    $block = json_decode(stripslashes($_POST['block']), true);
    if(!empty($block['data'])){ 
      //this means they are previewing their content
      $file_name = ucfirst(str_replace('-', ' ', $name));
      return "<div style='background:black;color:white;padding:1rem;font-family:courier'>\"$file_name\" preview unavailable. Please use edit mode.</div>";
    } else {
      //this means they are previewing to select a block
      $src = get_block_preview_image_src($name);
      if($src){
        return "<img src='$src'>";
      } else {
        return "No preview available.";
      }
    }
  }
}

/**
 * Registers wp menus
 * @param $menus - An array of titles
 */
function register_menus($menus){
  foreach($menus as $menu){
    register_nav_menu(sanitize_title_with_dashes($menu), $menu);
  }
}

/**
 * Include CSS/JS
 */
//remove woocommerce default styles... we will serve our own
add_filter( 'woocommerce_enqueue_styles', '__return_false' );

//remove jquery migrate
add_action( 'wp_default_scripts', function($scripts){
  if (!is_admin() && isset($scripts->registered['jquery'])) {
    $script = $scripts->registered['jquery'];
    if($script->deps){ 
      $script->deps = array_diff($script->deps, [ 'jquery-migrate' ]);
    }
  }
});

add_action('wp_enqueue_scripts', function(){
  // Main CSS
  if(file_exists(get_template_directory() . '/public/css/style.css')){
    wp_enqueue_style('izi-main', get_template_directory_uri().'/public/css/style.css', [], filemtime(get_template_directory() . '/public/css/style.css'));
  }

  //add woocommerce styles if we are on a page that needs it
  if(file_exists(get_template_directory() . '/public/css/woocommerce.css') && (is_cart() || is_checkout())){
    wp_enqueue_style('izi-woocommerce', get_template_directory_uri().'/public/css/woocommerce.css', [], filemtime(get_template_directory() . '/public/css/woocommerce.css'));
  }

  //wp block library
  wp_dequeue_style( 'wp-block-library' ); 
  wp_dequeue_style( 'wp-block-library-theme' );
  // Remove WooCommerce block CSS
  wp_dequeue_style( 'wc-blocks-style' );

  // Main JS
  wp_enqueue_script('jquery');
  if(file_exists(get_template_directory() . '/public/js/main.js')){
    wp_enqueue_script('izi-main', get_template_directory_uri().'/public/js/main.js', [ 'jquery' ], filemtime(get_template_directory() . '/public/js/main.js'), true);
    wp_localize_script('izi-main', 'izi', [
      'ajaxURL' => admin_url('admin-ajax.php')
    ]);
  }
});

//wysiwyg styles for admin
add_action('admin_init', function(){
  //editor style for admin
  add_editor_style('public/css/editor-style.css');
  if(file_exists(get_template_directory() . '/public/css/admin.css')){
    wp_enqueue_style('izi-admin', get_template_directory_uri() . '/public/css/admin.css', [], filemtime(get_template_directory() . '/public/css/admin.css') ) ;
  }
  if(file_exists(get_template_directory() . '/public/js/admin.js')){
    wp_enqueue_script('izi-admin', get_template_directory_uri(). '/public/js/admin.js', [ 'jquery' ], filemtime(get_template_directory() . '/public/js/admin.js'), true);
    wp_localize_script('izi-admin', 'adminLocal', ['directory' => get_stylesheet_directory_uri() ]);
  }
});

add_action('admin_enqueue_scripts', function(){
  global $theme_configuration;
  echo '<style>:root{';
  if(!empty($theme_configuration['admin_background_color'])){
      echo "--admin-background-color:{$theme_configuration['admin_background_color']};";
    }
    if(!empty($theme_configuration['admin_text_color'])){
      echo "--admin-text-color:{$theme_configuration['admin_text_color']};";
    }
  echo '}</style>';
});

//calls a bunch of functions to help the theme out
function theme_configuration($options){
  global $theme_configuration;
  $theme_configuration = $options;
  //boolean
  empty($options['remove_comments']) ?: remove_comments_from_wp();
  //pass args
  empty($options['acf_options_pages']) ?: register_acf_options_pages($options['acf_options_pages']);
  empty($options['menus']) ?: register_menus($options['menus']);
  empty($options['acf_whitelist_ids']) ?: whitelist_acf_admin($options['acf_whitelist_ids']);
  empty($options['wysiwyg_colors']) ?: add_wysiwyg_colors($options['wysiwyg_colors']);
  empty($options['allow_mime_uploads']) ?: allow_mime_uploads($options['allow_mime_uploads']);
  empty($options['post_types']) ?: create_post_types($options['post_types']);
  empty($options['taxonomies']) ?: create_taxonomies($options['taxonomies']);
}

/**
 * registers ACF options pages
 */
function register_acf_options_pages($pages){
  if( function_exists('acf_add_options_page') ) {
    foreach($pages as $page){
      acf_add_options_page(array(
        'page_title'  => $page,
        'menu_title'  => $page,
        'menu_slug'   => sanitize_title_with_dashes($page),
        'capability'  => 'edit_posts'
      )); 
    }
  }
}

// Removes all comment support from WP
function remove_comments_from_wp(){
  add_action( 'admin_menu', 'my_remove_admin_menus' );
  function my_remove_admin_menus() {
      remove_menu_page( 'edit-comments.php' );
  }
  // Removes from post and pages
  add_action('init', 'remove_comment_support', 100);

  function remove_comment_support() {
      remove_post_type_support( 'post', 'comments' );
      remove_post_type_support( 'page', 'comments' );
  }
  // Removes from admin bar
  function mytheme_admin_bar_render() {
      global $wp_admin_bar;
      $wp_admin_bar->remove_menu('comments');
  }
  add_action( 'wp_before_admin_bar_render', 'mytheme_admin_bar_render' );
}

function whitelist_acf_admin($ids){
  $ids = is_array($ids) ? $ids : [ $ids ];
  if(!in_array(get_current_user_id(), $ids)) {
    add_filter('acf/settings/show_admin', '__return_false');
  }
}

/**
 * Add wysiwyg colors to tiny MCE
 */
function add_wysiwyg_colors($colors){
  add_filter('tiny_mce_before_init', function($init) use ($colors){
    $colors_concat = '';
    
    foreach($colors as $name => $color){
      $colors_concat .= '"'.str_replace('#', '', $color).'","'.$name.'",';
    }

    // build colour grid default+custom colors
    $init['textcolor_map'] = '['.$colors_concat.']';

    return $init;
  });

  add_action('acf/input/admin_footer', function() use ($colors) {
    $colors = json_encode(array_values($colors));
    echo "<script>
      acf.add_filter('color_picker_args', function( args ){
        args.palettes = $colors
        return args;
      });
    </script>";
  });
}

/**
 * Allow mime uploads
 */
function allow_mime_uploads($types){
  add_filter('upload_mimes', function($mimes) use ($types) {
    foreach($types as $type){
      foreach($type as $key => $value){
        $mimes[$key] = $value;
      }
    }
    return $mimes;
  });
}

/**
 * Create post types
 */
function create_post_types($post_types){
  add_action('init', function() use ($post_types) {
    foreach($post_types as $type){
      register_post_type($type['slug'], [
        'labels'              => [ 
          'name'          => $type['plural'], 
          'singular_name' => $type['singular'] 
        ],
        'supports'            => !empty($type['supports']) ? $type['supports'] : [],
        'hierarchical'        => isset($type['hierarchical']) ? $type['hierarchical'] : false,
        'public'              => isset($type['public']) ? $type['public'] : true,
        'show_ui'             => true,
        'show_in_menu'        => true,
        'show_in_nav_menus'   => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'can_export'          => true,
        'has_archive'         => isset($type['has_archive']) ? $type['has_archive'] : true,
        'exclude_from_search' => isset($type['exclude_from_search']) ? $type['exclude_from_search'] : true,
        'publicly_queryable'  => isset($type['publicly_queryable']) ? $type['publicly_queryable'] : true,
        'capability_type'     => 'page',
        'menu_icon'           => !empty($type['icon']) ? $type['icon'] : [],
        'rewrite'             => !empty($type['rewrite_slug']) ? [ 'with_front' => false, 'slug' => $type['rewrite_slug'] ] : [ 'with_front' => false ],
        'taxonomies'          => !empty($type['taxonomies']) ? $type['taxonomies'] : [],
      ]);
    }
  });
}

/**
 * Create taxonomies
 */
function create_taxonomies($taxonomies){
  add_action('init', function() use ($taxonomies) {
    foreach($taxonomies as $tax){
      register_taxonomy($tax['slug'], $tax['post_types'], [
        'labels' => [
          'name'          => $tax['plural'],
          'singular_name' => $tax['singular']
        ],
        'hierarchical' => true,
        'show_admin_column' => isset($tax['show_admin_column']) ? $tax['show_admin_column'] : false,
        'rewrite' => [
          'slug' => isset($tax['rewrite_slug']) ? $tax['rewrite_slug'] : false
        ]
      ]);
    }
  });
}

/**
 * =-=-=-=-=-=--====-=-=-=-=-====-=-=-=-===-=-
 * =====-=====-=-=-=-=-=-=-=-=-=-=-=-=-
 * This stuff should always be done
 * =====-=====-=-=-=-=-=-=-=-=-=-=-=-=-
 * =-=-=-=-=-=--====-=-=-=-=-====-=-=-=-===-=-
 */

add_theme_support('post-thumbnails');
add_theme_support('post');
add_theme_support('html5');
add_theme_support('menus');
add_theme_support('responsive-embeds');

//remove gforms terrible AJAX spinner (we add a new one with CSS)
add_filter( 'gform_ajax_spinner_url', function(){ return 'data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///yH5BAEAAAAALAAAAAABAAEAAAIBRAA7'; }, 10, 2);

//stop WP from automatically cropping and reducing the quality of large image uploads
add_filter( 'big_image_size_threshold', '__return_false' );

// save ACF JSON files
add_filter('acf/settings/save_json', function ( $path ) {
  return get_stylesheet_directory() . '/acf-json';
});
 
/**
 * Disable the emojis
 */
function disable_emojis() {
  remove_action( 'wp_head', 'print_emoji_detection_script', 7 );
  remove_action( 'admin_print_scripts', 'print_emoji_detection_script' );
  remove_action( 'wp_print_styles', 'print_emoji_styles' );
  remove_action( 'admin_print_styles', 'print_emoji_styles' ); 
  remove_filter( 'the_content_feed', 'wp_staticize_emoji' );
  remove_filter( 'comment_text_rss', 'wp_staticize_emoji' ); 
  remove_filter( 'wp_mail', 'wp_staticize_emoji_for_email' );
  add_filter( 'tiny_mce_plugins', function($plugins){
    if(is_array($plugins)) {
      return array_diff($plugins, [ 'wpemoji']);
    } else {
      return [];
    }
  });
  add_filter( 'wp_resource_hints', function( $urls, $relation_type ) {
    if ('dns-prefetch' == $relation_type) {
      /** This filter is documented in wp-includes/formatting.php */
      $emoji_svg_url = apply_filters( 'emoji_svg_url', 'https://s.w.org/images/core/emoji/2/svg/' );

      $urls = array_diff( $urls, array( $emoji_svg_url ) );
    }

    return $urls;
  }, 10, 2 );
}
add_action( 'init', 'disable_emojis' );

/**
 * Removes WP events from dashboard
 */
add_action('wp_dashboard_setup', function(){
  remove_meta_box('dashboard_primary','dashboard','side'); //WordPress.com Blog
});

/**
 * Adds the site logo to the login page
 */
add_action( 'login_enqueue_scripts', function() {
  $image_url = wp_get_attachment_image_url(get_field('site_logo', 'options'), 'large');

  global $theme_configuration;
  $background = !empty($theme_configuration['admin_background_color']) ? $theme_configuration['admin_background_color'] : "#f0f0f1";

  $style = "
  <style type='text/css'>
    body {
      background-color: $background !important;
    }

    #nav,
    #backtoblog {
      background: #fff;
      margin: 0 !important;
      padding: 3px 24px !important;
    }

    #login h1 a, .login h1 a {
      background-image: url($image_url);
      width:320px;
      background-size: contain;
      background-repeat: no-repeat;
    }
  </style>
  ";

  echo $style;
});

/**
 * change the WP link on the login page
 */
add_filter( 'login_headerurl', function(){
  return home_url();
});

/**
 * Automatically convert all JPG/PNG uploads to WEBP before storing them on the server
 */
add_filter( 'wp_handle_upload', function($array, $var){
  global $theme_configuration;

  //cut off early if this is false
  if(empty($theme_configuration['convert_images_to_webp'])){
    return $array;
  }

  if($var == 'upload'){
    $webp_array = [];

    if($array['type'] == 'image/jpeg'){
      $image = imagecreatefromjpeg( $array['file'] );
      $filename = str_replace([ '.jpeg', '.jpg' ], '.webp', $array['file']);
      imagewebp( $image, $filename, $theme_configuration['webp_quality'] );
      imagedestroy( $image );
      unlink($array['file']);

      foreach($array as $key => $value){
        $webp_array[$key] = str_replace([ '.jpg', '.jpeg' ], '.webp', $value);
        $webp_array[$key] = str_replace('image/jpeg', 'image/webp', $webp_array[$key]);
      }
    }

    if($array['type'] == 'image/png'){
      $image = imagecreatefrompng( $array['file']  );
      $filename = str_replace('.png', '.webp', $array['file']);
      imagepalettetotruecolor( $image );
      imagealphablending( $image, true );
      imagesavealpha( $image, true );
      imagewebp( $image, $filename, $theme_configuration['webp_quality'] );
      imagedestroy( $image );
      unlink($array['file']);

      foreach($array as $key => $value){
        $webp_array[$key] = str_replace('.png', '.webp', $value);
        $webp_array[$key] = str_replace('image/png', 'image/webp', $webp_array[$key]);
      }
    }

    return !empty($webp_array) ? $webp_array : $array;
  }

  return $array;
}, 10, 2 ); 

// Changes media link to default to none
update_option('image_default_link_type','none');
remove_action('wp_head', 'wp_generator');

// Filter Yoast Meta Priority
add_filter( 'wpseo_metabox_prio', function() { return 'low'; } );

//remove empty p tags by wpautop and the_content
add_filter('the_content', function($content){
  $content = preg_replace( '#<p>\s*+(<br\s*/*>)?\s*</p>#i', '', $content );
  $content = preg_replace( '~\s?<p>(\s)+</p>\s?~', '', $content );
  return $content;
}, 20, 1);

/**
 * Tell woocommerce that our theme can support it
 */
add_action( 'after_setup_theme', function(){
  add_theme_support( 'woocommerce' );
});

// setup mail settings to get trapped in mailhog
add_action ( 'phpmailer_init', function ($phpmailer) {
  if (!empty($_ENV['MAIL_DRIVER']) && $_ENV['MAIL_DRIVER'] == "smtp") {
    $phpmailer->From = $_ENV ['MAIL_FROM'];
    $phpmailer->Timeout = 20;
    if (!empty($_ENV['MAIL_ENCRYPTION']) && $_ENV['MAIL_ENCRYPTION'] != 'null') {
      $phpmailer->SMTPSecure = true;

      $phpmailer->SMTPAutoTLS = false;
      $phpmailer->SMTPOptions ['ssl'] ['verify_peer'] = false;
      $phpmailer->SMTPOptions ['ssl'] ['verify_peer_name'] = false;
      $phpmailer->SMTPOptions ['ssl'] ['allow_self_signed'] = true;
      $phpmailer->SMTPOptions ['tls'] ['verify_peer'] = false;
      $phpmailer->SMTPOptions ['tls'] ['verify_peer_name'] = false;
      $phpmailer->SMTPOptions ['tls'] ['allow_self_signed'] = true;
    } else {
      $phpmailer->SMTPSecure = false;
    }
    $phpmailer->isSMTP();
    $phpmailer->Host = $_ENV ['MAIL_HOST'];
    $phpmailer->Port = $_ENV ['MAIL_PORT'];
    if(!empty($_ENV['MAIL_AUTH']) &&  $_ENV ['MAIL_AUTH'] != 'false') {
      $phpmailer->SMTPAuth = true;
      $phpmailer->Username = $_ENV ['MAIL_USERNAME'];
      $phpmailer->Password = $_ENV ['MAIL_PASSWORD'];
    } else {
      $phpmailer->SMTPAuth = false;
    }
  }
}, 3, 1 );

/**
 * Strip out # hrefs from menu items
 */
// add_filter('wp_nav_menu_items', function($items){
//   $items = preg_replace('/<a href="#">(.*)<\/a>/', '<span>$1</span>', $items);
//   return $items;
// });

/**
 * Fix height/width attributes when serving SVGs via wp_uploads otherwise they just end up as 1/1
 */
add_filter( 'wp_get_attachment_image_src',  function($image, $attachment_id, $size, $icon) {
  if (is_array($image) && preg_match('/\.svg$/i', $image[0]) && $image[1] <= 1) {
    $auth_file = !empty($_ENV['HTUSER']) ? str_replace('://', "://{$_ENV['HTUSER']}:{$_ENV['HTPASS']}@", $image[0]) : $image[0];

    if(is_array($size)) {
      $image[1] = $size[0];
      $image[2] = $size[1];
    } elseif(($xml = simplexml_load_file($auth_file )) !== false) {
      $attr = $xml->attributes();
      $viewbox = explode(' ', $attr->viewBox);
      $image[1] = isset($attr->width) && preg_match('/\d+/', $attr->width, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[2] : null);
      $image[2] = isset($attr->height) && preg_match('/\d+/', $attr->height, $value) ? (int) $value[0] : (count($viewbox) == 4 ? (int) $viewbox[3] : null);
    } else {
      $image[1] = $image[2] = null;
    }
  }
  return $image;
} , 10, 4 );

// Customize mce editor font sizes, fonts, & styles
if ( ! function_exists( 'wpex_mce_text_sizes' ) ) {
  function wpex_mce_text_sizes( $initArray ){
    //Font sizes
    $initArray['fontsize_formats'] = "12px 13px 14px 16px 18px 20px 22px 24px 26px 28px 30px 32px 34px 36px 38px 40px 60px";

    //Fonts
    //$initArray['font_formats'] = 'Andale Mono=andale mono,times;Arial=arial,helvetica,sans-serif;Arial Black=arial black,avant garde;Book Antiqua=book antiqua,palatino;Comic Sans MS=comic sans ms,sans-serif;Courier New=courier new,courier;Georgia=georgia,palatino;Helvetica=helvetica;Impact=impact,chicago;Symbol=symbol;Tahoma=tahoma,arial,helvetica,sans-serif;Terminal=terminal,monaco;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva;Webdings=webdings;Wingdings=wingdings,zapf dingbats';

    //Styles (add classes)
    /*$style_formats = array(
        // These are the custom styles
        [
          'title' => 'Line Height 1',
          'block' => 'span',
          'classes' => 'line-height1',
          'wrapper' => true,
        ],
    );
    // Insert the array, JSON ENCODED, into 'style_formats'
    $initArray['style_formats'] = json_encode( $style_formats );*/

    return $initArray;
  }
}
add_filter( 'tiny_mce_before_init', 'wpex_mce_text_sizes' );

//Add buttons to TinyMCE
function add_style_select_button($buttons) {
    //array_unshift($buttons, 'styleselect');    
    array_unshift( $buttons, 'fontsizeselect' ); 
    //array_unshift( $buttons, 'fontselect' );
    return $buttons;
}
add_filter('mce_buttons_2', 'add_style_select_button');

/* Automatically set the image Title, Alt-Text, Caption & Description upon upload
--------------------------------------------------------------------------------------*/
add_action( 'add_attachment', function ( $post_ID ) {
    // Check if uploaded file is an image
    if ( wp_attachment_is_image( $post_ID ) ) {
        $my_image_title = get_post( $post_ID )->post_title;

        // Sanitize the title:  remove hyphens, underscores & extra spaces:
        $my_image_title = preg_replace( '%\s*[-_\s]+\s*%', ' ',  $my_image_title );

        // Sanitize the title:  capitalize first letter of every word (other letters lower case):
        $my_image_title = ucwords( strtolower( $my_image_title ) );

        // Create an array with the image meta (Title, Caption, Description) to be updated
        $my_image_meta = [
            'ID' => $post_ID,            // Specify the image (ID) to be updated
            'post_title' => $my_image_title,     // Set image Title to sanitized title
        ];

        // Set the image Alt-Text
        update_post_meta( $post_ID, '_wp_attachment_image_alt', $my_image_title );

        // Set the image meta (e.g. Title, Excerpt, Content)
        wp_update_post( $my_image_meta );
    }
});

//This will prepend your WordPress RSS feed content with the featured image
add_filter('the_content', 'smartwp_featured_image_in_rss_feed');
function smartwp_featured_image_in_rss_feed( $content ) {
  global $post;
  if( is_feed() ) {
    if ( has_post_thumbnail( $post->ID ) ){
      $prepend = '<div>' . get_the_post_thumbnail( $post->ID, 'medium', array( 'style' => 'margin-bottom: 10px;' ) ) . '</div>';
      $content = $prepend . $content;
    }
  }
  return $content;
}