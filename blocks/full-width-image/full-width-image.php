<?php 
  /**
   * Style options:
   * @param $padding[] | array
   * @param $padding[][desktop_top] | choice: none, small, medium, large
   * @param $padding[][mobile_top] | choice: none, small, medium, large
   * @param $padding[][desktop_bottom] | choice: none, small, medium, large
   * @param $padding[][mobile_bottom] | choice: none, small, medium, large
   * @param $animation | choice: up, down, left, right, fade, scale
   * @param $background_image | WP ID: image
   * @param $background_color | string text
   * @param $text_color | string text* 
   * @param $classes | string text
   * @param $id | string text
   *
   * Content options:
   * @param $image | WP ID: image
   */
  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';

  if(empty($image)){
    return false;
  }
?>
<section <?= create_module_attributes('full-width-image break-wrapper', $classes, $padding, $background_color, $text_color, $id) ?>>
  <?= !empty($background_image) ? wp_image($background_image, 'full', 'module--background') : '' ?>
  <animate-on-scroll animation="<?= $animation ?>">
    <?= wp_image($image, 'full') ?>
  </animate-on-scroll>
</section>