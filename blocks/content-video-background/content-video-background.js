{
  function fixSize(el){
    const parentWidth = el.parentNode.offsetWidth
    const parentHeight = el.parentNode.offsetHeight
    const shape = parentWidth > parentHeight ? 'wide' : 'tall'
    let multiple = 1

    if(el.offsetWidth < parentWidth){
      multiple = parentWidth / el.offsetWidth
    } else if(el.offsetHeight < parentHeight){
      multiple = parentHeight / el.offsetHeight
    }

    el.style.height = el.offsetHeight * multiple + 'px'
    el.style.width = el.offsetWidth * multiple + 'px'
  }

  function init(){
    //always make sure the background video is full sized 
    const videos = document.querySelectorAll('.content-video-background--video-container')

    if(videos){
      window.addEventListener('resize', e => {
        videos.forEach(video => fixSize(video))
      })
      setTimeout(() =>{
        videos.forEach(video => fixSize(video))
      }, 10)
    }
  }

  init()
}