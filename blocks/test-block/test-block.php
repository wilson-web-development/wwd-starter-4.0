<div class="p-t-60 p-b-60 wrapper editor-content">

  <h1 class="text-center m-b-100">Custom Element Examples</h1>

  <h2>Parallax</h2>
  <!-- kind of a hero -->
  <div style="position:relative;padding:100px 0;text-align:center;overflow:hidden;">
    <para-lax style="position:absolute; z-index:1; height:calc(100% + 200px); top: -100px; width: 100%" max-distance="100">
      <img src="https://wordpress.devbucket.net/wp-content/uploads/2021/03/island-1024x576.jpg" alt="A pretty island" style="width:100%">
    </para-lax>
    <p style="position:relative;z-index:2;background:white;padding:1em;width:300px;margin:0 auto">I'm static... but my background will move</p>
  </div>

  <div>
    <div class="m-t-100 m-b-100" style="border:2px solid black;display:flex;">
      <?php for($i = 1; $i < 11; $i++): ?>
        <para-lax speed="<?= $i * 0.25 ?>" max-distance="50" style="flex:1">
          <div style="background:lightcoral;display:flex;align-items:center;justify-content:center;height:50px;border:2px solid coral">Speed: <?= $i * 0.25 ?></div>
        </para-lax>
      <?php endfor ?>
    </div>
  </div>

  <h2>Animate on Scroll</h2>
  <div class="row">
    <div class="col-xs-4">
      <animate-on-scroll animation="up"><h3 class="p-t-100 p-b-100 text-center" style="background:red">Up</h3></animate-on-scroll>
    </div>
    <div class="col-xs-4">
      <animate-on-scroll animation="down"><h3 class="p-t-100 p-b-100 text-center" style="background:orange">Down</h3></animate-on-scroll>
    </div>
    <div class="col-xs-4">
      <animate-on-scroll animation="left"><h3 class="p-t-100 p-b-100 text-center" style="background:yellow">Left</h3></animate-on-scroll>
    </div>
    <div class="col-xs-4">
      <animate-on-scroll animation="right"><h3 class="p-t-100 p-b-100 text-center" style="background:green">Right</h3></animate-on-scroll>
    </div>
    <div class="col-xs-4">
      <animate-on-scroll animation="scale"><h3 class="p-t-100 p-b-100 text-center" style="background:blue">Scale</h3></animate-on-scroll>
    </div>
    <div class="col-xs-4">
      <animate-on-scroll animation="fade"><h3 class="p-t-100 p-b-100 text-center" style="background:purple">Fade</h3></animate-on-scroll>
    </div>
  </div>

  <h2>ADA Modal</h2>
  <button modal-id="example-modal">Open Modal</button>
  <button modal-id="another-example-modal">Open Another Modal</button>
  <ada-modal id="example-modal" click-backdrop-to-close="false" esc-to-close="false" style="background:white;margin:10vh auto;width:400px">
    <h3>Example Modal</h3>
    <p>You can't close me by clicking the background or pressing esc.</p>
    <button ada-modal-closer>Ok</button>
  </ada-modal>
  <ada-modal id="another-example-modal" style="background:white;margin:10vh auto;width:400px">
    <button ada-modal-closer>X</button>
    <h3>Another Example Modal</h3>
    <p>Thanks, that felt good.</p>
  </ada-modal>
  <script>
    document.querySelector('#example-modal').addEventListener('open', e => console.log(e))
    document.querySelector('#example-modal').addEventListener('close', e => console.log(e))
    document.querySelector('#another-example-modal').addEventListener('open', e => console.log(e))
    document.querySelector('#another-example-modal').addEventListener('close', e => console.log(e))
  </script>

  <h2>ADA Slider</h2>
  <ada-slider 
    id="example-ada-slider" 
    slides-per-page="1" 
    slides-per-page-large="1"
    slides-per-page-medium="1"
    slides-per-page-small="1"
    active-page="0" 
    speed="250"
    transition-type="fade" 
    pagination-style="single"
  >
    <ada-slide><div class="p-20" style="background:mediumpurple">0</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightblue">1</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightgreen">2</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightyellow">3</div></ada-slide>
    <ada-slide><div class="p-20" style="background:coral">4</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightcoral">5</div></ada-slide>
  </ada-slider>
  <div class="flex">
    <ada-slider-prev for="example-ada-slider">Previous</ada-slider-prev>
    <ul class="flex" style="padding:0;list-style:none">
      <ada-slider-page for="example-ada-slider" index="0">0</ada-slider-page>
      <ada-slider-page for="example-ada-slider" index="1">1</ada-slider-page>
      <ada-slider-page for="example-ada-slider" index="2">2</ada-slider-page>
      <ada-slider-page for="example-ada-slider" index="3">3</ada-slider-page>
      <ada-slider-page for="example-ada-slider" index="4">4</ada-slider-page>
      <ada-slider-page for="example-ada-slider" index="5">5</ada-slider-page>
    </ul>
    <ada-slider-next for="example-ada-slider">Next</ada-slider-next>  
  </div> 

  <ada-slider 
    id="example-ada-slider-2" 
    slides-per-page="2" 
    slides-per-page-large="2" <?php /* defaults to slides-per-page if not present */ ?>
    slides-per-page-medium="2" <?php /* defaults to slides-per-page-large if not present */ ?>
    slides-per-page-small="1" <?php /* defaults to slides-per-page-medium if not present */ ?>
    active-page="0" 
    speed="250" 
    transition-type="slide" 
    pagination-style="single"
    autoplay="true" <?php /* this is not ADA perfect at all right now... probably dont use it */ ?>
    autoplay-speed="6000"
  >
    <ada-slide><div class="p-20" style="background:mediumpurple">0</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightblue">1</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightgreen">2</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightyellow">3</div></ada-slide>
    <ada-slide><div class="p-20" style="background:coral">4</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightcoral">5</div></ada-slide>
  </ada-slider>
  <div class="flex">
    <ada-slider-prev for="example-ada-slider-2">Previous</ada-slider-prev>
    <ul class="flex" style="padding:0;list-style:none">
      <ada-slider-page for="example-ada-slider-2" index="0">0</ada-slider-page>
      <ada-slider-page for="example-ada-slider-2" index="1">1</ada-slider-page>
      <ada-slider-page for="example-ada-slider-2" index="2">2</ada-slider-page>
      <ada-slider-page for="example-ada-slider-2" index="3">3</ada-slider-page>
      <ada-slider-page for="example-ada-slider-2" index="4">4</ada-slider-page>
      <ada-slider-page for="example-ada-slider-2" index="5">5</ada-slider-page>
    </ul>
    <ada-slider-next for="example-ada-slider-2">Next</ada-slider-next>  
  </div>

  <ada-slider 
    id="example-ada-slider-3" 
    slides-per-page="3" 
    slides-per-page-large="3" <?php /* defaults to slides-per-page if not present */ ?>
    slides-per-page-medium="2" <?php /* defaults to slides-per-page-large if not present */ ?>
    slides-per-page-small="1" <?php /* defaults to slides-per-page-medium if not present */ ?>
    active-page="0" 
    speed="250" 
    transition-type="slide" 
    pagination-style="multi"
  >
    <ada-slide><div class="p-20" style="background:mediumpurple">0</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightblue">1</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightgreen">2</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightyellow">3</div></ada-slide>
    <ada-slide><div class="p-20" style="background:coral">4</div></ada-slide>
    <ada-slide><div class="p-20" style="background:lightcoral">5</div></ada-slide>
  </ada-slider>
  <div class="flex">
    <ada-slider-prev for="example-ada-slider-3">Previous</ada-slider-prev>
    <ul class="flex" style="padding:0;list-style:none">
      <ada-slider-page for="example-ada-slider-3" index="0">0</ada-slider-page>
      <ada-slider-page for="example-ada-slider-3" index="1">1</ada-slider-page>
      <ada-slider-page for="example-ada-slider-3" index="2">2</ada-slider-page>
      <ada-slider-page for="example-ada-slider-3" index="3">3</ada-slider-page>
      <ada-slider-page for="example-ada-slider-3" index="4">4</ada-slider-page>
      <ada-slider-page for="example-ada-slider-3" index="5">5</ada-slider-page>
    </ul>
    <ada-slider-next for="example-ada-slider-3">Next</ada-slider-next>  
  </div> 

  <!--
      THESE WILL ACTUALLY BE COMING FROM A PLUGIN INSTEAD -- DON'T USE THEM RIGHT NOW
  <h2>ADA Forms</h2>
  <div class="row">
    <div class="col col-sm-6 col-xs-12 editor-content">
      <p>Available input types:</p>
      <ul>
        <li>text</li>
        <li>email</li>
        <li>number</li>
        <li>checkbox</li>
        <li>radio</li>
        <li>textarea</li>
        <li>password</li>
        <li>file</li>
        <li>date</li>
        <li>select</li>
      </ul>
    </div>
    <div class="col col-sm-6 col-xs-12 editor-content">
      <p>Available validation rules:</p>
      <ul>
        <li>required</li>
        <li>email</li>
        <li>min:1</li>
        <li>max:100</li>
        <li>minLength:1</li>
        <li>maxLength:100</li>
        <li>sameAs:inputName</li>
        <li>mime:image/*</li>
        <li>dateBefore:1970-01-01</li>
        <li>dateAfter:1970-01-01</li>
      </ul>
    </div>
  </div>
  <div class="row">
    <div class="col col-sm-6 col-xs-12">
      <ada-form id="simple-form">
        <h3>Simple Example</h3>
        <br>
        <ada-input label="Full Name" type="text" name="fullName" rules="required" placeholder="John Smith"></ada-input>
        <ada-input type="submit" value="Submit"></ada-input>
      </ada-form>
      <script>document.querySelector('#simple-form').addEventListener('submit', e => alert(`Hello ${e.detail.fullName}`))</script>
    </div>
    <div class="col col-sm-6 col-xs-12">
      <ada-form id="complex-form">
        <h3>Complex Example</h3>
        <br>
        <p><button  class="button" id="throw-error">Programatically Add Error</button></p>
        <br>
        <ada-input label="Full Name" type="text" name="fullName" rules="required" placeholder="John Smith"></ada-input>
        <ada-input label="Your Email" type="email" name="email" rules="required|email" placeholder="email@mail.com"></ada-input>
        <ada-input label="Choose a number greater than 3 but less than 7" type="number" name="number" rules="required|min:3|max:7" placeholder="5"></ada-input>
        <ada-input label="Choose something" type="select" name="thing" rules="required">
          <ada-option value="" label="--"></ada-option>
          <ada-option value="something" label="Something"></ada-option>
          <ada-option value="something-else" label="Something Else"></ada-option>
          <ada-option value="another-thing" label="Another Thing"></ada-option>
        </ada-input>
        <ada-input label="Choose 2 items" type="checkbox" name="items" rules="required|min:2|max:2">
          <ada-option value="something" label="Something"></ada-option>
          <ada-option value="something-else" label="Something Else"></ada-option>
          <ada-option value="another-thing" label="Another Thing"></ada-option>
        </ada-input>
        <ada-input label="Choose 1 item" type="radio" name="items2" rules="required">
          <ada-option value="something" label="Something"></ada-option>
          <ada-option value="something-else" label="Something Else"></ada-option>
          <ada-option value="another-thing" label="Another Thing"></ada-option>
        </ada-input>
        <ada-input label="Message" type="textarea" name="message" rules="required|minLength:10|maxLength:100" placeholder="Leave me a nice message"></ada-input>
        <ada-input label="Password" type="password" name="password" rules="required|minLength:5" placeholder="Enter your password"></ada-input>
        <ada-input label="Confirm Password" type="password" name="confirmPassword" rules="required|sameAs:password" placeholder="Confirm your password"></ada-input>
        <ada-input label="Upload an Image" type="file" name="image" rules="required|mime:image/"></ada-input>
        <ada-input label="Date" type="date" name="date" rules="required|dateAfter:2021-04-01|dateBefore:2021-04-30"></ada-input>
        <ada-input type="submit" value="Wow you made it all the way!"></ada-input>
      </ada-form>
      <script>
        document.querySelector('#throw-error').addEventListener('click', e => {
          e.preventDefault()
          const emailInput = document.querySelector('ada-input[name="email"]')
          emailInput._setErrors([ 'Uh oh, that email doesn\'t work for some reason' ])
          emailInput.querySelector('input').focus()
        })
        document.querySelector('#complex-form').addEventListener('submit', e => console.log(e.detail))
      </script>
    </div>
  </div>-->

  <h2>ADA Accordion/Toggler</h2>
  <ada-toggler 
    <?php /* these are required */ ?>
    group="faqs"              <?php /* what group this tab belongs to (opening this will close others in the same group) */ ?>
    id="faq-question-1"       <?php /* Required for ADA/aria labels */ ?>
    for="faq-answer-1"        <?php /* ID of the toggler's target */ ?>
    <?php /* things below here are optional */ ?>
    initially-active="false"  <?php /* true|false, a group should only have 1 ada-toggler set to true */ ?>
    can-close-all="true"      <?php /* true|false, should the user be able to close all tabs in this group or should 1 always stay open? */ ?>
    slide="true"              <?php /* true|false, should the target slide open or just appear? */ ?>
  >
    <h3>Do you know who I am?</h3>
  </ada-toggler>
  <div id="faq-answer-1">
    <p>Yes we do, your name is Mr. Bojangles.</p>
  </div>

  <ada-toggler group="faqs" id="faq-question-2" for="faq-answer-2">
    <h3>Do you know who I work for?</h3>
  </ada-toggler>
  <div id="faq-answer-2">
    <p>Of course, your chicken shack is known across the United States.</p>
  </div>

  <ada-toggler group="faqs" id="faq-question-3" for="faq-answer-3">
    <h3>Why did you say that?</h3>
  </ada-toggler>
  <div id="faq-answer-3">
    <p>Because I love you and your delicious chicken.</p>
  </div>

  <hr>

  <ada-toggler group="tabs" id="tab-title-1" for="tab-content-1" initially-active="true" can-close-all="false" slide="false">
    <h3>Tab 1</h3>
  </ada-toggler>
  <ada-toggler group="tabs" id="tab-title-2" for="tab-content-2" can-close-all="false" slide="false">
    <h3>Tab 2</h3>
  </ada-toggler>
  <ada-toggler group="tabs" id="tab-title-3" for="tab-content-3" can-close-all="false" slide="false">
    <h3>Tab 3</h3>
  </ada-toggler>
  <div id="tab-content-1">
    <p>Tab content 1</p>
  </div>
  <div id="tab-content-2">
    <p>Tab content 2</p>
  </div>
  <div id="tab-content-3">
    <p>Tab content 3</p>
  </div>

  <hr>

  <h2>Countdown Timer</h2>
  <countdown-timer target="<?= strtotime("1 hour, 15 seconds") ?>" now="<?= strtotime("now") ?>">
    <p><countdown-timer-days></countdown-timer-days> Days</p>
    <p><countdown-timer-hours></countdown-timer-hours> Hours</p>
    <p><countdown-timer-minutes></countdown-timer-minutes> Minutes</p>
    <p><countdown-timer-seconds></countdown-timer-seconds> Seconds</p>
  </countdown-timer>


  <h2>Force Aspect Ratios</h2>
  <div class="row">
    <div class="col-xs-4">
      <force-aspect-ratio aspect-ratio="16:9">
        <div style="background:lightblue" class="flex justify-center align-center">16:9</div>
      </force-aspect-ratio>
    </div>  
    <div class="col-xs-4">
      <force-aspect-ratio aspect-ratio="4:3">
        <div style="background:lightblue" class="flex justify-center align-center">4:3</div>
      </force-aspect-ratio>
    </div>  
    <div class="col-xs-4">
      <force-aspect-ratio aspect-ratio="1:1">
        <div style="background:lightblue" class="flex justify-center align-center">1:1</div>
      </force-aspect-ratio>
    </div>  
  </div>

  <h2>Count on Scroll</h2>
  <div class="row">
    <div class="col-xs-4">
      $<count-on-scroll to="40"></count-on-scroll> million
    </div>
    <div class="col-xs-4">
      <count-on-scroll to="1450" speed="2500"></count-on-scroll>% increase
    </div>
    <div class="col-xs-4">
      <count-on-scroll to="345.69" speed="5000"></count-on-scroll>/days a year
    </div>
  </div>

  <h2>ADA Video</h2>
  <force-aspect-ratio aspect-ratio="16:9">
    <ada-video 
      source="<?= convert_video_link_for_iframe('https://vimeo.com/519433816') ?>" 
      poster="https://wordpress.devbucket.net/wp-content/uploads/2021/03/island-1024x576.jpg"
      title="The videos title"
      alt="A guy with a microphone sings loudly to the crowd"
    ></ada-video>
  </force-aspect-ratio>

  <h2>Match Size</h2>
  <div>
    <match-size width="true" group="match-size-group-1" style="vertical-align:middle;background:yellow">
      <div style="width:200px;height:200px">Everything in this group will match my width because I'm the biggest</div>
    </match-size>
    <match-size width="true" group="match-size-group-1" style="vertical-align:middle;background:orange">
      <div>Hello</div>
    </match-size>
    <match-size width="true" group="match-size-group-1" style="vertical-align:middle;background:red">
      <div>More content</div>
    </match-size>
  </div>

  <div>
    <match-size height="true" group="match-size-group-2" style="vertical-align:middle;background:yellow">
      <div style="width:200px;height:200px">Everything in this group will match my height because I'm the biggest</div>
    </match-size>
    <match-size height="true" group="match-size-group-2" style="vertical-align:middle;background:orange">
      <div>Hello</div>
    </match-size>
    <match-size height="true" group="match-size-group-2" style="vertical-align:middle;background:red">
      <div>More content</div>
    </match-size>
  </div>

  <div>
    <match-size height="true" width="true" group="match-size-group-3" style="vertical-align:middle;background:yellow">
      <div style="width:200px;height:200px">Everything in this group will match my height and width because I'm the biggest</div>
    </match-size>
    <match-size height="true" width="true" group="match-size-group-3" style="vertical-align:middle;background:orange">
      <div>Hello</div>
    </match-size>
    <match-size height="true" width="true" group="match-size-group-3" style="vertical-align:middle;background:red">
      <div>More content</div>
    </match-size>
  </div>

  <h2>Sticky Div</h2>
  <div style="border:1px solid black; height:500px; position: relative">
    <sticky-div>
      <div style="width: 200px; height: 200px; border: 2px solid red; margin: 20px"></div>
    </sticky-div>
  </div>

  <h2>Scroll Progress</h2>
  <div style="border:1px solid black; height:300px; position: relative" id="scroll-progress">
    <scroll-progress style="height: 20px; width: 200px" background-color="#000" foreground-color="orange" target="#scroll-progress" type="horizontal"></scroll-progress>
    <scroll-progress style="height: 200px; width: 20px" background-color="#efefef" foreground-color="lightblue" target="#scroll-progress" type="vertical"></scroll-progress>
  </div>

  <h2>Compare Images</h2>
  <image-compare label-1="Before" label-2="After">
    <?= wp_image(336) ?>
    <?= wp_image(335) ?>
  </image-compare>
</div>