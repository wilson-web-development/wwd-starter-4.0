<?php
  global $wp_query;
  $found_posts = $wp_query->posts;
  $current_page = !empty($wp_query->query['paged']) ? $wp_query->query['paged'] : 1;
  $max_pages = $wp_query->max_num_pages;

  //add any extra data to posts
  array_map(function($single){
    $single->highlighted_title = preg_replace('/('.implode('|', explode(" ", get_search_query(false))) .')/iu', '<span class="search-content--highlight">\0</span>', $single->post_title);
    $single->highlighted_content = preg_replace('/('.implode('|', explode(" ", get_search_query(false))) .')/iu', '<span class="search-content--highlight">\0</span>', get_words(strip_tags(strip_shortcodes($single->post_content)), 20));
  }, $found_posts);
  
?>
<section class="search-content m-t-100 m-b-100">
  <div class="wrapper">
    <h1 class="m-b-50">Search results for "<?= get_search_query() ?>":</h1> 

    <?php if(!empty($found_posts)): ?>

      <?php foreach($found_posts as $single): ?>
        <div class="m-b-50 content-editor">
          <h2><?= $single->highlighted_title; ?></h2>
          <p><?= $single->highlighted_content; ?>...</p>
          <p><a class="button" href="<?= get_the_permalink(); ?>">Read More</a></p>
        </div>
      <?php endforeach; ?>

      <?php if($max_pages > 1): ?>
        <nav class="text-center" aria-label="Pagination" aria-controls="search-content">
          <?php foreach(get_limited_pages($current_page, $max_pages, 2) as $i): ?>
            <?php if(!empty($i)): ?>
              <a class="<?= $current_page == $i ? 'current' : '' ?>" href="<?= home_url() . '/page/' . $i . '?s=' . get_search_query(false) ?>"><?= $i ?></a>
            <?php else: ?>
              <span>...</span>
            <?php endif; ?>
          <?php endforeach; ?>
        </nav>
      <?php endif; ?>

    <?php else: ?>
      <div class="block-section content-editor"><h2>No results found</h2></div>
    <?php endif; ?>
  </div>
</section>