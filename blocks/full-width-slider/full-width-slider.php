<?php 
  /**
   * Style options:
   * @param $padding[] | array
   * @param $padding[][desktop_top] | choice: none, small, medium, large
   * @param $padding[][mobile_top] | choice: none, small, medium, large
   * @param $padding[][desktop_bottom] | choice: none, small, medium, large
   * @param $padding[][mobile_bottom] | choice: none, small, medium, large
   * @param $animation | choice: up, down, left, right, fade, scale
   * @param $background_image | WP ID: image
   * @param $background_color | string text
   * @param $text_color | string text* 
   * @param $classes | string text
   * @param $id | string text
   *
   * Content options:
   * @param $slides[] | array
   * @param $slides[][background_image] | WP ID: image
   * @param $slides[][content] | string html
   */
  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';

  $slider_id = uniqid("full-width-slider--");
?>
<section <?= create_module_attributes('full-width-slider break-wrapper', $classes, $padding, $background_color, $text_color, $id) ?>>
  <?= !empty($background_image) ? wp_image($background_image, 'full', 'module--background') : '' ?>

  <ada-slider 
    id="<?= $slider_id ?>"
    slides-per-page="1" 
    slides-per-page-large="1"
    slides-per-page-medium="1"
    slides-per-page-small="1"
    active-page="0" 
    speed="250"
    transition-type="fade" 
    pagination-style="single"    
  >
    <?php foreach($slides as $slide): ?>
      <ada-slide>
        <div class="full-width-slider--slide">
          <?= !empty($slide['background_image']) ? wp_image($slide['background_image'], 'full', 'full-width-slider--slide-background') : '' ?>
          <div class="wrapper">
            <animate-on-scroll animation="<?= $animation ?>">
              <div class="editor-content">
                <?= $slide['content'] ?>
              </div>
            </animate-on-scroll>
          </div>
        </div>
      </ada-slide>
    <?php endforeach ?>
  </ada-slider>

  <div class="wrapper flex justify-center align-center m-t-10">
    <ada-slider-prev for="<?= $slider_id ?>">Previous</ada-slider-prev>
    <ul class="flex flex-wrap" style="padding:0;list-style:none">
      <?php foreach($slides as $index => $slide): ?>
        <ada-slider-page for="<?= $slider_id ?>" index="<?= $index ?>"><?= $index ?></ada-slider-page>
      <?php endforeach ?>
    </ul>
    <ada-slider-next for="<?= $slider_id ?>">Next</ada-slider-next>  
  </div> 

</section>