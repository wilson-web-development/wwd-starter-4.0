<nav class="main-menu" aria-label="Main Navigation">
  <?php
    wp_nav_menu([
      'theme_location'  => 'main-menu',
      'container'       => false,
   ]);
  ?>
</nav>