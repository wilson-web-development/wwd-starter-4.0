import ADAWPMenu from '../../resources/js/plugins/ADAWPMenu'

{
  new ADAWPMenu('.main-menu .menu', {
    menuTogglerContainer: '', //defaults to the `el` this is being mounted on, but if you have a set up like this: <div class="thing"><div><!-- stuff --></div><ul><!-- wp menu --></ul></div> and you want to trigger "thing" add that here
    menuDisplayWhenOpen: 'block', //the display property of the menu when open
    slideDuration: 450, //the speed for open/close menus
    subMenuDisplayWhenOpen: 'block', //the display property of sub menus
    menuToggler: '.mobile-menu-button', //a class name/ID to show/hide the entire menu
    menuFillsScreenHeightOnToggle: true, //should the menu fill the screen when the menuToggler is clicked?
    menuFillAccountsForHeaderHeight: true, //should the menu fill account for the main header? set to false if the menu is position:fixed/overlaps the header 
    mobileSwitch: 768, //when does the menu switch to mobile mode? 
      setHiddenWhenMobileMenuIsToggled: [ '.skip-to-content', '.main-header--search', '.main-header--site-logo', 'main', '.main-footer' ], //an array of classes or IDs to aria-hide when the mobile menu is open
    mobileSubMenusAutomaticallyClose: false, //i guess they aren't supposed to auto close for ADA purposes? But that is annoying so you can also turn it off
  })
}
