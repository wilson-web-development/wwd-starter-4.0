<?php 
  /**
   * Style options:
   * @param $padding[] | array
   * @param $padding[][desktop_top] | choice: none, small, medium, large
   * @param $padding[][mobile_top] | choice: none, small, medium, large
   * @param $padding[][desktop_bottom] | choice: none, small, medium, large
   * @param $padding[][mobile_bottom] | choice: none, small, medium, large
   * @param $animation | choice: up, down, left, right, fade, scale
   * @param $background_image | WP ID: image
   * @param $background_color | string text
   * @param $text_color | string text* 
   * @param $classes | string text
   * @param $id | string text
   *
   * Content options:
   * @param $content | string html
   * @param $posts[] | array of WP ID's, if empty, most recent posts will be used
   */
  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';

  if(empty($posts)){
    $posts = wp_list_pluck(get_posts([ 'posts_per_page' => 3 ]), 'ID');
  }

?>
<section <?= create_module_attributes('content-block break-wrapper', $classes, $padding, $background_color, $text_color, $id) ?>>
  <?= !empty($background_image) ? wp_image($background_image, 'full', 'module--background') : '' ?>
  <div class="wrapper">
    <animate-on-scroll animation="<?= $animation ?>">
      <div class="editor-content m-b-40">
        <?= $content ?>
      </div>
    </animate-on-scroll>
    <?php if(!empty($posts)): ?>
      <div class="row">
        <?php foreach($posts as $index => $id): ?>
          <div class="col col-md-4 col-sm-6 col-xs-12">
            <animate-on-scroll animation="<?= $animation ?>" delay="<?= ($index % 3) * 100 //3 columns per row ?>">
              <?php block('post-thumbnail', [ 'post_id' => $id ]) ?>
            </animate-on-scroll>
          </div>
        <?php endforeach ?>
      </div>
    <?php endif ?>
  </div>
</section>