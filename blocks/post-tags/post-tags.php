<div class="wrapper  m-t-60 m-b-60">
  <div>Categories: <?php the_category(', '); ?></div>
  <div>Tags: <?php echo get_the_tag_list('', ', ') ?: 'No tags'; ?></div>
</div>