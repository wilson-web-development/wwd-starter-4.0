<?php
  /**
   * @param $title | string text
   * @param $link | string URL
   */
  $link = !empty($link) ? $link : get_the_permalink();
  $title = !empty($title) ? $title : 'Share';
?>
<div class="share wrapper m-t-100 m-b-100 flex align-center">
  <p class="m-r-15">SHARE</p>
  <ul class="share--links flex align-center">
    <li>
      <a class="m-r-15" target="_blank" href="http://www.facebook.com/sharer.php?u=<?php echo $link; ?>">
        <?php echo render_svg('facebook'); ?>
      </a>
    </li>
    <li>
      <a class="m-r-15" target="_blank" href="http://twitter.com/share?url=<?php echo $link; ?>"><?php echo render_svg('twitter'); ?></a>
    </li>
    <li>
      <a class="m-r-15" target="_blank" href="https://www.linkedin.com/shareArticle?mini=true&url=<?php echo $link; ?>"><?php echo render_svg('linkedin'); ?></a>
    </li>
    <li>
      <a class="m-r-15" target="_blank" href="mailto:?subject=<?php echo $title; ?>&body=<?php echo $link; ?>"><?php echo render_svg('email'); ?></a>
    </li>
  </ul>
</div>