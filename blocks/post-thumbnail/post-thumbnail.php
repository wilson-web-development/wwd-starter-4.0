<?php
  /**
   * @param $post_id
   */

  $single = get_post($post_id);
  $featured_image = get_post_thumbnail_id($post_id);
  $category = get_primary_category($post_id);
  $date = get_the_date('F j, Y', $single);
  $permalink = get_the_permalink($single);
?>
<div class="post-thumbnail editor-content">
  <a class="no-underline" href="<?= $permalink ?>">
    <?= !empty($featured_image) ? wp_image($featured_image, 'large', 'post-thumbnail--image m-b-15') : '' ?>
    <h3><?= $single->post_title ?></h3>
  </a>
  <p>
    <small><?= $category->name ?> | <?= $date ?></small>
  </p>
  <?= @el_if('p', $single->post_excerpt) ?>
  <p><a href="<?= $permalink ?>">Read More</a></p>
</div>