<?php 
  /**
   * Style options:
   * @param $padding[] | array
   * @param $padding[][desktop_top] | choice: none, small, medium, large
   * @param $padding[][mobile_top] | choice: none, small, medium, large
   * @param $padding[][desktop_bottom] | choice: none, small, medium, large
   * @param $padding[][mobile_bottom] | choice: none, small, medium, large
   * @param $animation | choice: up, down, left, right, fade, scale
   * @param $background_image | WP ID: image
   * @param $background_color | string text
   * @param $text_color | string text* 
   * @param $classes | string text
   *
   * Modified version of this: https://www.thewebtaylor.com/articles/wordpress-creating-breadcrumbs-without-a-plugin
   */

  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';

  global $post;

  $links = [];

  //first link is always the homepage
  $links[] = [
    'title' => 'Home',
    'link'  => home_url()
  ];

  //add a ton of checks to get more links
  if(is_archive() && !is_tax() && !is_category() && !is_tag()){
    $title = post_type_archive_title('', false); 

    //the above function fails on tribe_events because it doesn't have a filter for post_type_archive_title... so figure it out manually
    if(empty($title) && is_post_type_archive()){
      $post_type = get_query_var( 'post_type' );
      if(is_array($post_type)){
        $post_type = reset($post_type);
      }
      $post_type_obj = get_post_type_object( $post_type );
      $title = $post_type_obj->labels->name;
    }

    //archive
    if (!empty($title)) {
      $links[] = [ 'title' => $title ];
    }
  } else if (is_archive() && is_tax() && !is_category() && !is_tag()){
    // If post is a custom post type
    $post_type = get_post_type();
    // If it is a custom post type display name and link
    if($post_type != 'post') {
      $post_type_object = get_post_type_object($post_type);
      $post_type_archive = get_post_type_archive_link($post_type);
      $links[] = [
        'title' => $post_type_object->labels->name,
        'link'  => $post_type_archive
      ];
    }
              
    $custom_tax_name = get_queried_object()->name;
    $links[] = [ 'title' => $custom_tax_name ];
  } else if ( is_single() ) {
    // If post is a custom post type
    $post_type = get_post_type();
              
    // If it is a custom post type display name and link
    if($post_type != 'post') {
      $post_type_object = get_post_type_object($post_type);
      $post_type_archive = get_post_type_archive_link($post_type);
      $links[] = [
        'title' => $post_type_object->labels->name,
        'link'  => $post_type_archive
      ];
    }
              
    // Get post category info
    $primary_category = get_primary_category(get_the_ID());
     
    if(!empty($primary_category)) {
      $links[] = [
        'title' => $primary_category->name,
        'link'  => get_term_link($primary_category, 'category')
      ];
    }
  
    //We should probably add something here to handle custom taxonomies?

    $links[] = [ 'title' => get_the_title() ];
  } else if ( is_category() ) {
    // Category page
    $links[] = [ 'title' => single_cat_title('', false) ];
  } else if ( is_page() ) {
    // Standard page
    if( $post->post_parent ){
      // If child page, get parents 
      $anc = get_post_ancestors( $post->ID );
           
      // Get parents in the right order
      $anc = array_reverse($anc);
  
      if($anc){
        foreach ( $anc as $ancestor ) {
          $links[] = [
            'title' => get_the_title($ancestor),
            'link'  => get_permalink($ancestor)
          ];
        }
      }
         
      // Current page
      $links[] = [ 'title' => get_the_title(), 'link' => get_permalink($post->ID) ];
    } else {
      // Just display current page if not parents
      $links[] = [ 'title' => get_the_title(), 'link' => get_permalink($post->ID) ];
    }
               
  } else if ( is_tag() ) {
      // Tag page
      // Get tag information
      $term_id        = get_query_var('tag_id');
      $taxonomy       = 'post_tag';
      $args           = 'include=' . $term_id;
      $terms          = get_terms( $taxonomy, $args );
      $get_term_id    = $terms[0]->term_id;
      $get_term_slug  = $terms[0]->slug;
      $get_term_name  = $terms[0]->name;
         
      // Display the tag name
      $links[] = [ 'title' => $get_term_name ];
  } elseif ( is_day() ) {
    // Day archive
    // Year link
    $links[] = [ 
      'title' => get_the_time('Y') . ' Archives',
      'link'  => get_year_link(get_the_time('Y'))
    ]; 
    // Month link
    $links[] = [ 
      'title' => get_the_time('M') . ' Archives',
      'link'  => get_month_link(get_the_time('Y'), get_the_time('m'))
    ];
    //Day display
    $links[] = [ 
      'title' => get_the_time('jS') . ' ' . get_the_time('M') . ' Archives',
    ];
  } else if ( is_month() ) {         
    // Month Archive
    // Year link
    $links[] = [ 
      'title' => get_the_time('Y') . ' Archives',
      'link'  => get_year_link(get_the_time('Y'))
    ]; 
    // Month link
    $links[] = [ 
      'title' => get_the_time('M') . ' Archives',
    ];
  } else if ( is_year() ) {
    // Display year archive
    $links[] = [ 
      'title' => get_the_time('Y') . ' Archives',
    ]; 
  } else if ( is_author() ) {
    // Auhor archive
    global $author;
    $userdata = get_userdata( $author );
    $links[] = [ 'title' => 'Author: ' . $userdata->display_name ];    
  } else if ( get_query_var('paged') ) {
    $links[] = [ 'title' => 'Page ' . get_query_var('paged') ];
  } else if ( is_search() ) {
     $links[] = [ 'title' => 'Search results for: ' . get_search_query() ];
  } elseif ( is_404() ) {
    $links[] = [ 'title' => '404' ];
  }
?>

<section <?= create_module_attributes('breadcrumbs break-wrapper', $classes, $padding, $background_color, $text_color, $id) ?>>
  <div class="wrapper">
    <animate-on-scroll animation="<?= $animation ?>">
      <nav aria-label="Breadcrumbs">
        <ol class="flex flex-wrap">
          <?php foreach($links as $index => $link): ?>
            <li <?= $index == count($links) - 1 ? "class='breadcrumbs--current'" : "" ?>>
              <?php
                if(!empty($link['link'])){
                  $aria = $index == count($links) - 1 ? "aria-current='page'" : "";
                  echo "<a href='{$link['link']}' {$aria}>{$link['title']}</a>";
                } else {
                  echo "<span {$aria}>{$link['title']}</span>";
                }
                if($index !== count($links) - 1){
                  echo "<span class='breadcrumb--separator m-l-5 m-r-5' aria-hidden='true'>|</span>";
                }
              ?>
            </li>
          <?php endforeach ?>
        </ol>
      </nav>
    </animate-on-scroll>
  </div>
</section>