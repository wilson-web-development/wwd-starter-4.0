<?php

/**
 * Style options:
 * @param $padding[] | array
 * @param $padding[][desktop_top] | choice: none, small, medium, large
 * @param $padding[][mobile_top] | choice: none, small, medium, large
 * @param $padding[][desktop_bottom] | choice: none, small, medium, large
 * @param $padding[][mobile_bottom] | choice: none, small, medium, large
 * @param $animation | choice: up, down, left, right, fade, scale
 * @param $background_image | WP ID: image
 * @param $background_color | string text
 * @param $text_color | string text* 
 * @param $classes | string text
 * @param $id | string text
 *
 * @param $rows[] | array
 * @param $rows[][title] | string text
 * @param $rows[][content] | string html
 */
  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';

  $group_id = uniqid('accordion-'); //generate an ID for this module so things open/close as a group
  
  $rows = array_map(function ($section) {
    $section['id'] = uniqid('accordion--row-'); //generate an ID for each row to match the toggler and the content
    return $section;
  }, $rows);
?>
<section <?= create_module_attributes('accordion break-wrapper', $classes, $padding, $background_color, $text_color, $id) ?>>
  <?= !empty($background_image) ? wp_image($background_image, 'full', 'module--background') : '' ?>
  <div class="wrapper">
    <?php foreach ($rows as $row) : ?>
      <div class="accordion--slide">
        <animate-on-scroll animation="<?= $animation ?>">
          <ada-toggler id="toggle-<?= $row['id'] ?>" for="content-<?= $row['id'] ?>" group="<?= $group_id ?>">
            <h3><?= $row['title'] ?></h3>
          </ada-toggler>
          <div id="content-<?= $row['id'] ?>" class="editor-content"><?php echo $row['content']; ?></div>
        </animate-on-scroll>
      </div>
    <?php endforeach; ?>
  </div>
</section>