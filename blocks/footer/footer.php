    </main>
    <footer class="main-footer">
      <?php block('back-to-top') ?>
      <div class="wrapper">
        <div class="editor-content">
          &copy; <?php echo date('Y'); ?> <?php echo bloginfo('blogname'); ?>
        </div>
        <?php block('social-media-list'); ?>
      </div>
      <?php wp_footer(); ?>
    </footer>
  </body>
</html>