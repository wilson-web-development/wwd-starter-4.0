<?php
  global $post;
  $product = new WC_product($post->ID);
  $images = array_merge([ $product->get_image_id() ], $product->get_gallery_image_ids());
?>

<div class="wrapper m-b-100">
  <?php do_action( 'woocommerce_before_single_product' ); ?>
  <div class="row">
    <div class="col col-sm-6 col-xs-12 single-product--slider text-center">
      <ada-slider 
        id="product-<?= $post->ID ?>-gallery" 
        slides-per-page="1" 
        slides-per-page-large="1"
        slides-per-page-medium="1"
        slides-per-page-small="1"
        active-page="0" 
        speed="250"
        transition-type="fade" 
        pagination-style="single"
      >
        <?php foreach($images as $image): ?>
          <ada-slide><?= wp_image($image) ?></ada-slide>
        <?php endforeach ?>
      </ada-slider>
      <div class="flex">
        <ada-slider-prev for="product-<?= $post->ID ?>-gallery">Previous</ada-slider-prev>
        <ul class="flex">
          <?php foreach($images as $index => $image): ?>
            <ada-slider-page for="product-<?= $post->ID ?>-gallery" index="<?= $index ?>"><?= wp_image($image, 'thumbnail') ?></ada-slider-page>
          <?php endforeach ?>
        </ul>
        <ada-slider-next for="product-<?= $post->ID ?>-gallery">Next</ada-slider-next>  
      </div>
    </div>
    <div class="col col-sm-6 col-xs-12">
      <div class="editor-content">
        <h1><?= get_the_title() ?></h1>
        <h2 class="single-product--price">
          <?= $product->get_price_html() ?>
        </h2>
        <?= apply_filters('the_content', $product->get_description()) ?>
        <?php woocommerce_template_single_add_to_cart() ?>
      </div>
    </div>
  </div>
</div>