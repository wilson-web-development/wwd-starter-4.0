{
  let button = document.querySelector('.back-to-top')
  if(button){
    jQuery(window).scroll(e => window.pageYOffset > (window.innerHeight / 2) ? button.classList.add('show') : button.classList.remove('show'))
  }
    jQuery('.back-to-top').click(e => {
    jQuery('html, body').animate({ scrollTop: 0 }, 650)
    setTimeout(() => {
      jQuery('#content').focus()
    }, 300)
  })
}