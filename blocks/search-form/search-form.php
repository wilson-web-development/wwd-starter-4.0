<form method="get" action="<?php echo home_url(); ?>" role="search">
  <label for="site-search" class="sr-only">Search</label>
  <input type="text" id="site-search" name="s" placeholder="Search">
  <input type="submit" value="Search">
</form>