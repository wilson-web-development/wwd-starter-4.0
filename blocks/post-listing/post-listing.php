<?php
  /**
   * Creates a post listing app, 
   * We really just use this to pass data into a component
   * @param $post_type - a WP post_type slug
   * @param $categories - an array of WP categories to filter by
   * @param $tags - an array of WP tags to filter by
   */

  // if we are on a category or tag page, handle that
  if(empty($post_type)){
    $queried_object = get_queried_object();
    if(!empty($queried_object)){
      if(!empty($queried_object->taxonomy)){
        if($queried_object->taxonomy == 'post_tag'){
          $categories = get_categories();
          $tags = get_tags();
          $queried_tag_id = $queried_object->term_id;
          $preset_tag = $queried_object;
        } else if($queried_object->taxonomy == 'category'){
          $categories = get_categories();
          $tags = get_tags();
          $queried_category_id = $queried_object->term_id;
          $preset_category = $queried_object;
        }
      } else {
        $categories = get_categories();
        $tags = get_tags();        
      }
    }
  }
?>
<div class="post-listing" id="post-app">
  <post-search 
    :categories='<?php echo !empty($categories) ? vue_data(array_values($categories)) : "[]"; ?>'
    :tags='<?php echo !empty($tags) ? vue_data(array_values($tags)) : "[]"; ?>'
    :preset-category='<?php echo !empty($preset_category) ? vue_data($preset_category) : "{}"; ?>'
    :preset-tag='<?php echo !empty($preset_tag) ? vue_data($preset_tag) : "{}"; ?>'
  ></post-search>
</div>