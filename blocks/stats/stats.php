<?php

/**
 * Style options:
 * @param $padding[] | array
 * @param $padding[][desktop_top] | choice: none, small, medium, large
 * @param $padding[][mobile_top] | choice: none, small, medium, large
 * @param $padding[][desktop_bottom] | choice: none, small, medium, large
 * @param $padding[][mobile_bottom] | choice: none, small, medium, large
 * @param $animation | choice: up, down, left, right, fade, scale
 * @param $background_image | WP ID: image
 * @param $background_color | string text
 * @param $text_color | string text* 
 * @param $classes | string text
 * @param $id | string text
 * 
 * Content Options:
 * @param $stats[] | array
 * @param $stats[][prefix] | string text
 * @param $stats[][number] | number
 * @param $stats[][suffix] | string text
 * @param $stats[][content] | string html
 */
  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';
?>
<section <?= create_module_attributes('stats break-wrapper', $classes, $padding, $background_color, $text_color, $id) ?>>
  <?= !empty($background_image) ? wp_image($background_image, 'full', 'module--background') : '' ?>
  <div class="wrapper">
    <div class="row">
      <?php foreach ($stats as $index => $stat) : ?>
        <div class="col col-sm-4 col-xs-12 text-center">
          <animate-on-scroll animation="<?= $animation ?>" delay="<?= ($index % 3) * 100 //3 columns per row ?>">
            <div class="stats--number flex justify-center">
              <?= !empty($stat['prefix']) ? $stat['prefix'] : '' ?>
              <count-on-scroll to="<?= $stat['number'] ?>" speed="2000"></count-on-scroll>
              <?= !empty($stat['suffix']) ? $stat['suffix'] : '' ?>
            </div>
            <?php if (!empty($stat['content'])) : ?>
              <div class="editor-content"><?= $stat['content'] ?></div>
            <?php endif ?>
          </animate-on-scroll>
        </div>
      <?php endforeach ?>
    </div>
  </div>
</section>