<?php
  global $wp_query;
  $found_posts = $wp_query->posts;
  $current_page = !empty($wp_query->query['paged']) ? $wp_query->query['paged'] : 1;
  $max_pages = $wp_query->max_num_pages;
?>
<div class="wrapper m-b-100">
  <h1 class="m-b-30">Shop</h1>
  <div class="row">
    <?php if(have_posts()): while(have_posts()): the_post(); ?>
      <div class="col col-md-4 col-sm-6 col-xs-12">
        <div class="editor-content">
          <?php $product = new WC_product(get_the_ID()); ?>
          <h3><?= get_the_title(); ?></h3>
          <?= !empty($product->get_image_id()) ? wp_image($product->get_image_id(), 'medium') : '' ?>
          <p><?= $product->get_price_html() ?></p>
          <p><a class="button" href="<?= get_the_permalink($product->get_id()) ?>">View Item</a></p>
        </div>
      </div>
    <?php endwhile; else: ?>
      <div class="col-xs-12"><h2>No products found.</h2></div>
    <?php endif ?>
  </div>

  <?php if($max_pages > 1): ?>
    <nav class="text-center" aria-label="Pagination" aria-controls="search-content">
      <?php foreach(get_limited_pages($current_page, $max_pages, 2) as $i): ?>
        <?php if(!empty($i)): ?>
          <a class="<?= $current_page == $i ? 'current' : '' ?>" href="<?= home_url() . '/shop/page/' . $i ?>"><?= $i ?></a>
        <?php else: ?>
          <span>...</span>
        <?php endif; ?>
      <?php endforeach; ?>
    </nav>
  <?php endif; ?>

</div>