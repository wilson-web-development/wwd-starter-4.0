<?php 
  /**
   * Style options:
   * @param $padding[] | array
   * @param $padding[][desktop_top] | choice: none, small, medium, large
   * @param $padding[][mobile_top] | choice: none, small, medium, large
   * @param $padding[][desktop_bottom] | choice: none, small, medium, large
   * @param $padding[][mobile_bottom] | choice: none, small, medium, large
   * @param $animation | choice: up, down, left, right, fade, scale
   * @param $background_image | WP ID: image
   * @param $background_color | string text
   * @param $text_color | string text* 
   * @param $classes | string text
   *
   * Content Options:
   * @param $columns_per_row | choice: 2,3,4,6
   * @param $columns_alignment | choice: top,center,end
   * @param $reverse_order_on_mobile | boolean
   * @param $columns[] | array
   * @param $columns[][content] | string html
   */
  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';

  switch($columns_per_row){
    case 2:
      $col_class = 'col col-sm-6 col-xs-12';
      break;
    case 3:
      $col_class = 'col col-md-4 col-sm-6 col-xs-12';
      break;
    case 4:
      $col_class = 'col col-md-3 col-sm-6 col-xs-12';
      break;
    case 6:
      $col_class = 'col col-md-2 col-sm-3 col-xs-6';
      break;
  }
?>

<section <?= create_module_attributes('columns break-wrapper', $classes, $padding, $background_color, $text_color, $id) ?>>
  <?= !empty($background_image) ? wp_image($background_image, 'full', 'module--background') : '' ?>
  <div class="wrapper">
    <div class="row align-<?= $columns_alignment ?> <?= !empty($reverse_order_on_mobile) ? 'mobile-reverse' : '' ?>">
      <?php foreach($columns as $index => $col): ?>
        <div class="<?= $col_class ?>">
          <animate-on-scroll animation="<?= $animation ?>" delay="<?= ($index % $columns_per_row) * 100 ?>">
            <div class="editor-content">
              <?php echo $col['content']; ?>
            </div>
          </animate-on-scroll>
        </div>
      <?php endforeach; ?>
    </div>
  </div>
</section>
