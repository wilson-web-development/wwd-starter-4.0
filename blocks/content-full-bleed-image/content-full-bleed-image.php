<?php

/**
 * Style options:
 * @param $padding[] | array
 * @param $padding[][desktop_top] | choice: none, small, medium, large
 * @param $padding[][mobile_top] | choice: none, small, medium, large
 * @param $padding[][desktop_bottom] | choice: none, small, medium, large
 * @param $padding[][mobile_bottom] | choice: none, small, medium, large
 * @param $animation | choice: up, down, left, right, fade, scale
 * @param $background_image | WP ID: image
 * @param $background_color | string text
 * @param $text_color | string text* 
 * @param $classes | string text
 * @param $id | string text
 *
 * Content options
 * @param $content | string html
 * @param $content_width | choice: 25, 50, 75
 * @param $image | WP ID: image
 * @param $image_side | choice: left, right
 */
  $padding['desktop_top'] = !empty($padding['desktop_top']) ? $padding['desktop_top'] : 'medium';
  $padding['desktop_bottom'] = !empty($padding['desktop_bottom']) ? $padding['desktop_bottom'] : 'medium';
  $padding['mobile_top'] = !empty($padding['mobile_top']) ? $padding['mobile_top'] : 'medium';
  $padding['mobile_bottom'] = !empty($padding['mobile_bottom']) ? $padding['mobile_bottom'] : 'medium';
  $classes = !empty($classes) ? $classes : '';
  $animation = !empty($animation) ? $animation : 'none';
  $background_color = !empty($background_color) ? $background_color : '';
  $text_color = !empty($text_color) ? $text_color : '';
  $id = !empty($id) ? $id : '';
  
  switch ($content_width) {
    case 25:
      $image_width = '68vw';
      $image_col = 'col-sm-8 col-xs-12';
      $content_col = 'col-sm-3 col-xs-12';
      break;
    case 50:
      $image_width = '50vw';
      $image_col = 'col-sm-6 col-xs-12';
      $content_col = 'col-sm-5 col-xs-12';
      break;
    case 75:
      $image_width = '42vw';
      $image_col = 'col-sm-4 col-xs-12';
      $content_col = 'col-sm-7 col-xs-12';
      break;
  }

  $classes .= ' image-' . $image_side;
?>
<section <?= create_module_attributes('content-full-bleed-image break-wrapper', $classes, null, $background_color, $text_color, $id) ?>>
  <?= !empty($background_image) ? wp_image($background_image, 'full', 'module--background') : '' ?>
  <div class="wrapper">
    <div class="row">
      <div class="content-column col <?= $content_col ?> relative">
        <animate-on-scroll animation="<?= $animation ?>">
          <div <?= create_module_attributes('editor-content', $classes, $padding, $background_color, $text_color) ?>>
            <?= $content ?>
          </div>
        </animate-on-scroll>
      </div>

      <div class="col spacer-column col-sm-1 col-xs-12"></div>

      <div class="col image-column <?= $image_col ?> relative">
        <div class="image-container" style="
          background-image:url(<?= wp_get_attachment_image_src($image, 'full')[0] ?>);
          width: <?= $image_width ?>;
        ">
          <?= wp_image($image, 'large', 'mobile-only') ?>
        </div>
      </div>

    </div>
  </div>
</section>