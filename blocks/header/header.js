{
  function createPlaceholder(header){
    const placeholder = document.createElement('div')
    header.parentNode.insertBefore(placeholder, header)
    placeholder.classList.add('main-header--placeholder')
    placeholder.style.height = header.offsetHeight + 'px'
  }

  function handleStickyHeader(){
    const sticky = document.querySelector('.main-header.sticky')
    if(sticky){
      createPlaceholder(sticky)
      window.addEventListener('scroll', e => {
        if(window.scrollY > sticky.offsetHeight){
          sticky.classList.add('compressed')
        } else {
          sticky.classList.remove('compressed')
        }
      })
    }
  }

  function handleRevealHeader(){
    const reveal = document.querySelector('.main-header.reveal')
    if(reveal){
      createPlaceholder(reveal)
      let lastScrollPos = 0
      window.addEventListener('scroll', e => {
        if(window.scrollY > reveal.offsetHeight && lastScrollPos < window.scrollY){
          reveal.classList.add('hide')
        } else{
          reveal.classList.remove('hide')
        }
        lastScrollPos = window.scrollY
      }) 
    }
  }

  setTimeout(handleStickyHeader, 10)
  setTimeout(handleRevealHeader, 10)
}