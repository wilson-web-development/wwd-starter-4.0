{
  jQuery(".video--image-overlay").click(e => {
    let frame = jQuery(e.target.parentNode).find("iframe")
    frame.attr("src", frame.attr("src") + "?autoplay=1&autopause=0")
    frame.attr("aria-hidden", false)
    frame.attr("tabindex", "")
    jQuery(e.target.parentNode).addClass('open')
  })
}