<?php
  /**
   *
   * @param $classes - string - extra classes to add
   * @param $video_thumbnail - int - WP image ID to overlay
   * @param $video - string
   * @param $video_title - string
   * @param $video_ratio | number
   */
  if(empty($video)){
    return false;
  }
  if(empty($classes)){
    $classes = '';
  }  
  if(empty($video_ratio)){
    $video_ratio = '16:9';
  }
  if(empty($video_title)){
    $video_title = 'Video';
  }
  $ratio_nums = explode(":", $video_ratio);
  $ratio = $ratio_nums[1]/$ratio_nums[0];
?>
<div class="video-section<?php echo $classes; ?>">
  <div class="video <?php echo $classes; ?>" style="padding-top: calc(<?php echo $ratio; ?> * 100%)">
    <?php if(!empty($video_thumbnail)): ?>
      <?php echo wp_get_attachment_image($video_thumbnail, 'large', '', ['class' => 'video--image-overlay']); ?>
    <?php endif; ?>
    <?php echo render_svg('play'); ?>
    <iframe <?= !empty($video_thumbnail) ? 'aria-hidden="true" tabindex="-1"' : ''; ?> src="<?php echo convert_video_link_for_iframe($video); ?>" allow="autoplay" title="<?php echo $video_title; ?>"></iframe>
  </div>
</div>