<?php
  block('header');
  block('standard-content', [ 'content' => get_field('404_content', 'options') ]);
  block('footer');
?>