<?php
  //this overrides the woocommerce default template for products archive
  block('header');
  block('breadcrumbs');
  block('woocommerce-shop');
  block('footer');
?>